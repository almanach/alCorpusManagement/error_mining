package Program;

require Exporter;
use strict;
use Config;

our @ISA = qw(Exporter);
our @EXPORT = qw(&addPrintFunction &finalize &catch_int &printMsgs &printErrors &printWarns &initErrors &configErrors &debug $debugLevel &addDelimitor &delimitor &setVersionContents &setHelpContents $versionContents $helpContents &addFinalizeFunction);
our @EXPORT_OK = qw();
our $VERSION = 1.00;

our $debugLevel = undef;
our $helpContents = "";
our $versionContents = "";

my $progName = $0;
my $progCmd = $0;

my @printFunctions;
my @delimitors;
my @finalizeFunctions;

sub addPrintFunction{
	push(@printFunctions,@_);
}

# Main print function
sub printInfos{
	my ($who,$type,@msgs) = @_;
		
	foreach my $printFunc (@printFunctions){
		$printFunc->($who,$type,@msgs)
	}
	return;
}

sub addDelimitor{
	push(@delimitors,@_);
}

# Main delimitor function
sub delimitor{
			
	foreach my $delimitor (@delimitors){
		$delimitor->();
	}
}

sub addFinalizeFunction{
	push(@finalizeFunctions,@_);
}

# function called to correctly shut down an execution
sub finalize{
	
	printMsgs($progName, "Shutting down the program...");
	foreach my $finalize (@finalizeFunctions){
		$finalize->();
	}
	exit();
}

# function used to catch signals
sub catch_int{
	my ($sig) = @_;
	printMsgs($progName,"Signal $sig received");  
	finalize();
}


# function for regular messages
sub printMsgs{
	my ($who,@msgs) = @_;
	printInfos($who,"MSG",@msgs);
	return;
}

# function for error messages
sub printErrors{
	my ($who,@errorMsgs) = @_;
	
	printInfos($who,"ERROR",@errorMsgs);
	finalize();
}

# function for warning messages
sub printWarns{
	my ($who,@errorMsgs) = @_;
	
	printInfos($who,"WARNING",@errorMsgs);
	return;
}

# function for initialization errors messages
sub initErrors{
 	my ($who,@errorMsgs) = @_;

 	unshift(@errorMsgs,"Incorrect Use!!");
  	push(@errorMsgs,"Type $progCmd -h or --help for help or consult documentation");
	
	printErrors($who,@errorMsgs);
}

# function for config errors messages
sub configErrors{
	initErrors($progName,"Config error",@_); 
}

# function for debug messages
sub debug{
	my ($who,$levelasked,@msgs) = @_;

	if($debugLevel >= $levelasked){
		printInfos($who,"DEBUG $levelasked",@msgs);
 	}
	return;
}


sub setHelpContents{
	$helpContents = shift @_; 
	return;
}


sub setVersionContents{
	$versionContents = shift @_; 
	return;
}


##Redirecting signals in case to correctly end an execution
my @signaux = split ( / / , $Config{'sig_name'} ) ;
foreach my $sig (@signaux){ if(($sig ne 'CLD') && ($sig ne 'CHLD') && ($sig ne 'SIGZERO') && ($sig ne 'WINCH')){ $SIG{$sig}= \&catch_int;} }

return 1;
