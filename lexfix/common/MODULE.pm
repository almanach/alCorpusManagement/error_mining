package MODULE;

require Exporter;
use strict;

our @ISA = qw(Exporter);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 1.0;

=pod

=head2 B<MODULE.pm>

Main module file. 

=over 1

=item * 
B<new($conf,$funcMsg,$modName,$modConfPrefix,$variablesHashRef)>

This is the module builder, It expects four arguments. 
The fisrt '$conf' should be an hash table reference in which the module can find 
all the variables described and asked in the $variables hash table reference.
The second '$funcMsg' is a hash reference to the different messages functions (debug,msgs,errors,initErrors,warnings);
The third '$modName' is the module's name.
The fourth '$modConfPrefix' is the prefix that the variables should have in the initialization conf
The fifth '$variablesHashRef' is an hash reference where all the variables expected by the module are described.

=cut

sub new{
	my ($class,$generalConf,$funcMsg,$modName,$modConfPrefix,$variablesHashRef) = @_;
	
	my $self = {}; bless $self, $class;

	# initialize module
	$self->{'debug'} = (exists($funcMsg->{'debug'}))? $funcMsg->{'debug'} : sub {};
	$self->{'msgs'} = (exists($funcMsg->{'msgs'}))? $funcMsg->{'msgs'} : sub {};
	$self->{'errorFunc'} = (exists($funcMsg->{'errors'}))? $funcMsg->{'errors'} : sub {};
	$self->{'initErrFunc'} = (exists($funcMsg->{'initErrors'}))? $funcMsg->{'initErrors'} : sub {};
	$self->{'warnFunc'} = (exists($funcMsg->{'warnings'}))? $funcMsg->{'warnings'} : sub {};
	$self->{'delimit'} = (exists($funcMsg->{'delimit'}))? $funcMsg->{'delimit'} : sub {};
	$self->{'modName'} = $modName;


	# check required variables
	if(defined($variablesHashRef)){
		my $conf = {};
		
		if($modConfPrefix ne ""){
			$modConfPrefix = $modConfPrefix."_"; 
		}
		# make a copy of keys without prefix
		while(my ($cle,$valeur) = each %$generalConf){ 
			if($cle =~ s/$modConfPrefix(.*)/$1/){
			       $conf->{$cle} = $valeur;
			}
		}
			$conf->{'debug'} = $generalConf->{'debug'};
		$self->{'conf'} = $conf;

		## Check if all expected variables are in the conf and of the expected type
	
		my $allVariablesInitialized = 1;
		my @initErrors = ();
		foreach my $variableName (keys %{$variablesHashRef}){
			if(!defined($conf->{$variableName})){
				push(@initErrors,"Variable $variableName is not defined");
				$allVariablesInitialized = 0;
			}
			else{
				my $var = $conf->{$variableName};
				my $varRef = \$var;
				$varRef =~ s/\(.*\)//;
				#print "$variableName est du type ${$variablesHashRef}{$variableName} $varRef $var\n";
				if(!($varRef =~ m/^$variablesHashRef->{$variableName}$/)){
					if(($varRef eq "REF") and !($var =~ m/^$variablesHashRef->{$variableName}\(.*\)$/)){
						push(@initErrors,"Variable $variableName is not of the type expected");
						$allVariablesInitialized = 0;
					}
				}
			}
		}
	
		if(!$allVariablesInitialized){
			$self->initErrors(@initErrors);
		}
	}

	return $self;
}


=pod

=item * 
B<item finalize()>

DESTROY is a function automatically called before destroying the module. The default one doesn't make anything.

=cut

sub finalize{}

=pod

=item * 
B<item debug($debugLevel,@msgs)>

Send debug messages of level $debugLevel contained  in the array @msgs to the registered debug function.

=cut

sub debug{
	my ($self,$debug,@msgs) = @_;
	
	$self->{'debug'}($self->{'modName'},$debug,@msgs);
}


=pod

=item * 
B<item msgs(@msgs)>

Send regular messages contained  in the array @msgs to the registered msgs function.

=cut

sub msgs{
	my ($self,@msgs) = @_;
	
	$self->{'msgs'}($self->{'modName'},@msgs);
}

=pod

=item * 
B<item initErrors(@msgsErr)>

Send initialization errors messages contained  in the array @msgsErr to the registered initErrFunc function.

=cut

sub initErrors{
	my ($self,@msgsErr) = @_;
	
	$self->{'initErrFunc'}($self->{'modName'},@msgsErr);
}

=pod

=item * 
B<errors(@msgsErr)>

Send errors contained  in the array @msgsErr to the registered errorFunc function.

=cut

sub errors{
	my ($self,@msgsErr) = @_;
	
	$self->{'errorFunc'}($self->{'modName'},@msgsErr);
}

=pod

=item * 
B<warnings(@msgsWarn)>

Send warning messages contained  in the array @msgsWarn to the registered warnFunc function.

=cut

sub warnings{
	my ($self,@msgsWarn) = @_;
	
	$self->{'warnFunc'}($self->{'modName'},@msgsWarn);
}

=pod

=item * 
B<delimit()>

Just ask to print a delimitor to the registered delimit function.

=cut

sub delimit{
	my ($self) = @_;
	
	$self->{'delimit'}();
}


=pod

=item * 
B<getVariables()>

getVariables is a function called to get an hash reference describing all the expected by the module variables.
Default one reply an empty hash reference.

=back

=cut

sub getVariables{
	my $class = shift @_;
	
	if(%{$class->variables}){
		my $variables = $class->variables;
		if($class->modPrefix){
			my $prefix = $class->modPrefix;
			my $newVariables = {};  
			while(my($key,$value) = each %$variables){
				$newVariables->{$prefix."_".$key} = $value;
			}

			return $newVariables;
		}
		else{
		   	return $variables;
		}
	}
	else{
		return  {};
	}
}


