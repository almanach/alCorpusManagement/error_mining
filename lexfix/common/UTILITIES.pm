package UTILITIES;

require Exporter;
use strict;

our @ISA = qw(Exporter);
our @EXPORT = qw(checkVariables @ERRORS);
our @EXPORT_OK = qw();
our $VERSION = 1.0;

our @ERRORS;

=pod

=head2 B<UTILITIES.pm>

=over 1

=item * 
B<checkVariables($variablesHashRef,$confHashRef)>

Check if the required variables in %$confHashRef match those described in %$variablesHashRef 

=back

=cut

sub checkVariables {
	@ERRORS = ();

	my ($variablesHashRef,$confHashRef) = @_;

	my $allVariablesInitialized = 1;
	foreach my $variableName (keys %{$variablesHashRef}){
		if(!defined(${$confHashRef}{"$variableName"})){
			push(@ERRORS,"Variable $variableName is not defined");
			$allVariablesInitialized = 0;
		}
		else{
			my $var = ${$confHashRef}{"$variableName"};
			my $varRef = \$var;
			$varRef =~ s/\(.*\)//;
			#print "$variableName est du type ${$variablesHashRef}{$variableName} $varRef $var\n";
			if(!($varRef =~ m/^${$variablesHashRef}{$variableName}$/)){
				if(($varRef eq "REF") and !($var =~ m/^${$variablesHashRef}{$variableName}\(.*\)$/)){
					push(@ERRORS,"Variable $variableName is not of the type expected");
					$allVariablesInitialized = 0;
				}
			}
		}
	}

	return $allVariablesInitialized;
}

return 1;

