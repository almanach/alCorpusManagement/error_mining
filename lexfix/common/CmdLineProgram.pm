package CmdLineProgram;

require Exporter;
use strict;
use Program;

our @ISA = qw(Exporter);
our @EXPORT = qw(&finalize &catch_int &printMsgs &printErrors &printWarns &initErrors &configErrors &debug $debugLevel &delimitor &setHelpContents &helpContents &setVersionContents &versionContents &addFinalizeFunction);
our @EXPORT_OK = qw();
our $VERSION = 1.00;

sub outPutMsg{
	my ($who,$type,@msgs) = @_;
	my $time = localtime();

	my $prefix = my $msgForOutput = "$time $type from $who: ";
	foreach my $msg (@msgs){
		my $msgForOutput = "$prefix: $msg\n";	

		# Regular output  
		if(!($type =~ /DEBUG/) and !($type eq "MSG")){
			print STDERR $msgForOutput;
		}else{
			print $msgForOutput;
		}	
	}
}

addPrintFunction(\&outPutMsg);

sub lineDelimitor{
	print "-------------------------------------------------------------\n";
}

addDelimitor(\&lineDelimitor);


sub versionContents{
	print $versionContents;
}

sub helpContents{
	print $helpContents;
}

