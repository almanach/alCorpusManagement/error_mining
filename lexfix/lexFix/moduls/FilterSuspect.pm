package FilterSuspect;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.8;

=pod

=head2 B<FilterSuspect.pm>

Module used to filter uncorrect suspects. 

=cut

# The variables required by the module to work
sub variables { return {};}

# The module name to send to errors/warnings Functions
my $modName="FilterSuspect";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName);
	bless $self, $class;
	
	return $self;
}

################################Functions###############################################

=pod 

=item *
B<checkValidity()>

Answers a couple (answer,explanation) with answer set to 1 or undef about the validity of the suspect to be treated. 
Right now this module just exclude suspects wih punctuation within or without any related sentences.

=back

=cut

sub checkValidity{
	my ($self,$suspect) = @_;
	
	if($suspect->{'key'} =~ /.*([\?\.\�\!\:\;\,\%\$\=\}\]\)\@\`\|\[\\(\{\'\"\~\&\*\-\_]).*/){
		return (undef,"Found '$1' in ".$suspect->{'key'});
	}

	if($suspect->{'key'} =~ /.*([0123456789]).*/){
		return (undef,"Found '$1' in ".$suspect->{'key'});
	}


	my $nbSentences = @{$suspect->{'sentences'}};
       	if($nbSentences <= 4){
		return (undef,"Not enough sentences ($nbSentences)");
	}	       

	return (1,"")
}

return 1;
