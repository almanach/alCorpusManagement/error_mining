package HypothesisFilterByMark;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.9;

=pod

=head2 B<HypothesisFilterByMark.pm>

Module used to eliminate useless Hypothesis based on the marks recived for each. This version erase 'noisy' 
hypothesis that have very poor marks inferior to minimumScore.

=cut

# The variables required by the module to work
sub variables{
	return {'minimumScore'=> 'SCALAR'};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "HypothesisFilter";}

# The module name to send to errors/warnings Functions
my $modName = "HypothesisFilterByMark";

##########################General Functions#####################################
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	bless $self, $class;	

	return $self;
}
##########################Functions#############################################

=pod 

=over 1

=item * 
B<filterHypothesis($hypothesisHR,$marksHR)>

Erase from $hypothesisHR and $marksHR all the hypothesis that have a score < minimumScore.

=back

=cut

sub filterHypothesis{
	my ($self,$hypothesisHR,$marksHR) = @_;
	my $minimumScore = $self->{'conf'}->{'minimumScore'};

	while(my ($cle,$valeur) = each %$marksHR){
		if($valeur->{'points'} < $minimumScore){
			delete($marksHR->{$cle});
			delete($hypothesisHR->{$cle});
		}
	}
	
}

return 1;
