package OrderSuspects;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.8;

=pod

=head2 B<OrderSuspects.pm>

Module used to order the list of suspects from the most interesting to the less one.

=cut

# The variables required by the module to work
sub variables { return {};}

# The module name to send to errors/warnings Functions
my $modName = "OrderGroups";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;

	my $self = MODULE->new($conf,$funcMsg,$modName);
	bless $self, $class;
	
	return $self;
}

################################Functions###############################################

=pod

=over 1

=item * 
B<OrderGroups($groupsArrayRef)>

Function used to order and answer the entire unordered list of groups of suspects @$groupsArrayRef.

=cut

sub OrderSuspects{
	my ($self,$suspectsAR) = @_; 
	return  sort {if($self->score($a) < $self->score($b)) {return 1} else {return -1;}} @$suspectsAR; 

}	


=pod

=item * 
B<reInsertAndOrder($groupHashRef,$groupsArrayRef)>

Function used to reinsert to the correct place a group $groupHashRef in the list @$groupsArrayRef. Answers 
the ordered list.

=cut

#sub reInsertAndOrder{
#	my($self,$suspectHR,$suspectsAR) = @_;
#
#	my @res;
#	my $temp;
#	my $score = $self->score($suspectHR);
#	while(($temp = shift @$suspectsAR) and ($score < $self->score($temp))) {
#			push(@res,$temp);
#	}

#	push(@res,$suspectHR);
	
#	if(defined($temp)){ push(@res,$temp);}
#	if(@$suspectsAR) {push(@res,@$suspectsAR);}
	
#	return @res;
#}

=pod 

=over 1

=item * 
B<score($group)>

Function that calculate and answer a number representing the interest given to the group of suspects $group.

=back

=cut

sub score{
	my($self,$suspect) = @_;
	
	#my $nbSentUndone = @{$suspect->{'sentences'}} - $suspect->{'bestRes'};
	#if($nbSentUndone < 1){
	#	$nbSentUndone = 1;
	#}

	return $suspect->{'weight'}*log(@{$suspect->{'sentences'}});
}
