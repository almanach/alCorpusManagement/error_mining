package ParseManager;

use MODULE;
use strict;
use IO::Socket::INET;
use IO::Select;

use ModifyDag;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.5;

=pod

=head2 B<ParseManager.pm>

Module used to distribute task beetween the different parse servers.
=cut

# The variables required by the module to work
sub variables {
	return {'serverPort' => 'SCALAR'};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "ParseManager";}


# The module name to send to errors/warnings Functions
my $modName = "ParseManager";

my $io;
my $select;

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	bless $self, $class;	
	
	$self->{"ModifyDag"} = ModifyDag->new($conf,$funcMsg);

	$self->{'select'} = new IO::Select();
	$self->{'io'} = IO::Socket::INET->new( 	LocalPort=> $self->{'conf'}->{'serverPort'}, 
					Proto => 'tcp',
                                  	Listen => 10,
					"Timeout" => 2,#$self->{'conf'}->{'serverTimeOut'},
					"Reuse" => 0
					) or $self->errors("Could not set the open a port to receive requests");
	$self->{'select'}-> add($self->{'io'});
	
	return $self;	
}

sub getVariables{
	my $class = shift @_;
	my $variables = $class->SUPER::getVariables();
	
	my $modulVariables = ModifyDag->getVariables();
	while(my($name,$value) = each %$modulVariables){
		$variables->{$name} = $value;
	}

	return $variables;
}


sub finalize{
	my $self = shift @_;

	$self->{'select'}->remove($self->{'io'});
	$self->{'io'}->close();
	my $worstCase = 0;
	while(my($cle,$value) = each %{$self->{'hosts'}}){
		if($value->{'bridgeTimeOut'} > $worstCase){
			$worstCase = $value->{'bridgeTimeOut'};
		}
	}
	$self->msgs("Disconnecting all connected bridges");
	while(my @readys = $self->{'select'}->can_read($worstCase)){
		foreach my $socket (@readys){
			$self->removeHost($socket,0,1);
		}
	}	
	$self->{'io'}->close();
	$self->msgs("Done");
	
}

################################Functions###############################################




sub setCurrentData{
	my ($self,$suspects,$jokers) = @_;

	$self->{'data'} = { 	'suspects' => $suspects,
			 	'jokers'   => $jokers
			  };
	$self->prepareSomeWork();
}


sub stillWorkToDo{
	my $self = shift @_;
	
	return defined($self->{'nextJob'});
}

sub prepareSomeWork{
	my ($self) = @_;

	my ($suspect,$joker); 
	$suspect = shift @{$self->{'data'}->{'suspects'}};
	## does this suspect has still jokers to process ?
	if(defined($suspect) && !@{$self->{'data'}->{'jokers'}->{$suspect->{'id'}}}){
		## if no we take the next one
		$suspect = shift @{$self->{'data'}->{'suspects'}}
	}

	my $defSspect = defined($suspect);
	if(!defined($suspect)){
		my @jobs = values %{$self->{'jobs'}};
		if(@jobs){
			$self->{'nextJob'} = $jobs[int(rand(scalar @jobs))];
		}else{
			$self->{'nextJob'} = undef;	
		}
		return;
	}

	$joker = shift (@{$self->{'data'}->{'jokers'}->{$suspect->{'id'}}});

	my $job = { 'suspect' => $suspect, 
		    'joker' => $joker,
		    'modifiedSents' => {}
	};
	
	if($joker->{'joker'} eq 'none'){
		$job->{'modifiedSents'} = $suspect->{'sentences'};
	}
	else{
		my @modifiedSents;
		foreach my $sentence (@{$suspect->{'sentences'}}){
			my $modifiedDag = $self->{"ModifyDag"}->ModifyDag($suspect->{'key'},$suspect->{'lex'},$joker->{'joker'},$sentence->{'dag'});
			push(@modifiedSents,{ 'sentId' => $sentence->{'sentId'},
					     'corpusId' => $sentence->{'corpusId'},
					     'dag' => $modifiedDag."\n"
			});
		}
		$job->{'modifiedSents'} = \@modifiedSents;
	}
	
	$self->{'nextJob'} = $job;

	return;
}


=pod 

=over 1

=item * 
B<>


=back

=cut


sub doSomeWork{
	my ($self) = @_;
 	
	my @analysisOutputs;
	my $somethingDone = 0;
	
	while(my @readys = $self->{'select'}->can_read(2)){
	IO:foreach my $socket (@readys){
		if($socket == $self->{'io'}){
			my $new = $self->{'io'}->accept();
			$new->autoflush(1);
			$new->blocking(1);

			$self->{'select'}->add($new);
			#print "Connection de ".$new->peerhost()." sur ".$new->peerport()."\n";
		}
		else{				
			my @curReq;
			while(1){
				my $line = <$socket>;
				if(defined($line)){
				       chomp($line);
				       if($line eq "\@END\@"){
					       if(@curReq){
					       		last;
						}
						else{
							$self->warnings("Received an empty request from the ".$self->{'hosts'}->{$socket}->{'descr'});
							$self->removeHost($socket,1,1);
							next IO;
						}
					}
					else{
						push(@curReq,$line);
					}
				}elsif(!defined($socket->connected())){
					$self->warnings("Connection has been lost with the ".$self->{'hosts'}->{$socket}->{'descr'}." !!");
					$self->removeHost($socket,0,0);	
					#<STDIN>;
					next IO;
				}else{
					$self->warnings("Read an empty string on the connection with the ".$self->{'hosts'}->{$socket}->{'descr'});
					$self->removeHost($socket,0,0);
					#<STDIN>;
					next IO;
				}					
			}	

			my $curReq = shift @curReq;
			if(exists($self->{'hosts'}->{$socket})){
				$self->debug(3,"Server received a request: $curReq");
				if($curReq =~ /^NextJob$/){ ## A bridge is asking for a dag parserAddress=(.+?) parserPort=(.+?)
					$self->nextJob($socket);
				}
				elsif($curReq =~ /^Results$/){ ## a bridge is sending back results  
					my @outputs = $self->results($socket,\@curReq);
					if(@outputs){
						push(@analysisOutputs,@outputs);
					}
				}
				elsif($curReq =~ /^RemoveHost$/){ ## some bridge is asking to be removed
					$self->removeHost($socket,1,1)
				}
				else{
					$self->warnings("Received an unknown request $curReq");	
					print $socket "Error mess=Unknow request ($curReq)\n\@END\@\n";
					$self->removeHost($socket,1,0);
				}

				$somethingDone = 1;
			}
			elsif($curReq =~ /^AddHost parserAddress=(.+?) parserPort=(.+?) bridgeTimeOut=(.+?)$/){ ## some bridge is identifying
				$self->addHost($socket,$1,$2,$3);
			}
			else{
				$self->warnings("A non registered bridge asked for a command $curReq...");
				print $socket "Error mess=identify first\n\@END\@\n";
			}

		}

	}
	}

	if(!$somethingDone){
		my $time = time();
		while(my($cle,$valeur) = each %{$self->{'hosts'}}){
			if(($time - $valeur->{'lastWorkGiven'}) > (10 * $valeur->{'bridgeTimeOut'})){
				$self->warnings($valeur->{'descr'}." is too slow, I remove it.");
				my $socket = $valeur->{'socket'};
				if($socket->connected()){
					print $socket "Error mess=you are too slow\n\@END\@\n";
					$self->removeHost($socket,1,0);
				}
				else{
					$self->warnings("Its connection was lost...");
					$self->removeHost($socket,1,0);	
				}
			}	
		}

	}

	return @analysisOutputs;
}

sub addHost{
	my ($self,$socket,$parserAddress,$parserPort, $bridgeTimeOut) = @_;

	my $oui = 1;

	if(exists($self->{'parsers'}->{$parserAddress.":".$parserPort})){
		if($self->{'parsers'}->{$parserAddress.":".$parserPort}->{'socket'}->connected()){
			$self->warnings("A Bridge is trying to use an already used parser => $parserAddress:$parserPort...");
			print $socket "Error mess=already used parser\n\@END\@\n";	
			$oui = 0;	
		}else{
			$self->warnings("Connection has been lost with the ".$self->{'hosts'}->{$socket}->{'descr'}." !!");
			$self->removeHost($socket,0,0);
		}
	}
		
	if($oui){
		my %host; 
		$host{'address'} = $parserAddress;
		$host{'port'} = $parserPort;
		$host{'bridgeTimeOut'} = $bridgeTimeOut;
		$host{'lastWorkGiven'} = time(); 
		$host{'descr'} = "bridge managing $parserAddress:$parserPort";
		$host{'socket'} = $socket;
		$self->{'hosts'}->{$socket} = \%host; 
		$self->{'parsers'}->{$parserAddress.":".$parserPort} = \%host;
					
		print $socket "Welcome\n\@END\@\n";
		$self->msgs("Added ".$host{'descr'}." with timeout = ".$host{'bridgeTimeOut'});
	}	
}

sub removeHost{
	my ($self,$socket,$removeJob,$sayBye) = @_;

	my $bridge = $self->{'hosts'}->{$socket};
	$self->debug(4,"Removing ".$bridge->{'descr'});
	if($removeJob && exists($bridge->{'doing'})){
		my $jobOnProg = $self->{'jobs'}->{$bridge->{'doing'}};
		unshift(@{$self->{'data'}->{'suspects'}},$jobOnProg->{'suspect'});
		unshift(@{$self->{'data'}->{'jokers'}->{$jobOnProg->{'suspect'}->{'id'}}},$jobOnProg->{'joker'});
		$self->debug(3,"The bridge was having work on progress ".$bridge->{'doing'},"I put it back in the list");
	}
	$self->{'select'}->remove($socket);
	if($sayBye){
		print $socket "SeeYouSpaceCowboy\n\@END\@\n";
	}
	$socket->close();
	delete($self->{'hosts'}->{$socket});
	delete($self->{'parsers'}->{$bridge->{'address'}.":".$bridge->{'port'}});
}


sub nextJob{
	my ($self,$socket) = @_;

	$self->prepareSomeWork();
	$self->debug(4,$self->{'hosts'}->{$socket}->{'descr'}." ask for work");
	if(!$self->stillWorkToDo()){
		$self->debug(4,"Nothing left, it shall exit");
		$self->removeHost($socket,0,1);
	}else{
		my $job = $self->{'nextJob'}; 
		my @modifiedDags = @{$job->{'modifiedSents'}};
					
		my $pref = "suspect=".$job->{'suspect'}->{'id'}." joker=".$job->{'joker'}->{'joker'};
		my $message = "Job ".$pref."\n";
		foreach my $modifiedDag (@modifiedDags){
			$message .= $modifiedDag->{'dag'};
			$message .= "dagId=".$modifiedDag->{'corpusId'}."-".$modifiedDag->{'sentId'}."\n";
		}
		print $socket $message."\@END\@\n"; 
		$self->debug(4,"Sent $pref");
		
		$self->{'jobs'}->{$job->{'suspect'}->{'id'}."-".$job->{'joker'}->{'joker'}} = $job;
					
		my $time = time();
		$self->{'hosts'}->{$socket}->{'lastWorkGiven'} = $time; 	
		$self->{'hosts'}->{$socket}->{'doing'} = $job->{'suspect'}->{'id'}."-".$job->{'joker'}->{'joker'};
	}
}

sub results{
	my ($self,$socket,$curReq) = @_;

	my @analysisOutputs;

	my $jobIdent = $self->{'hosts'}->{$socket}->{'doing'};
	my $bridge = $self->{'hosts'}->{$socket};
	if(exists($self->{'jobs'}->{$jobIdent})){
		my $job = $self->{'jobs'}->{$jobIdent};
		$self->debug(4,"Received a result for $jobIdent by bridge ".$bridge->{'descr'});
		my ($curXml,$dagId,$line,@parsedSents,@failedSents,@tmoSents);
		while($line = shift @$curReq){
			if($line =~ /^dagId=(.*)-(.*)$/){
				if(!defined($curXml)){
					push(@tmoSents,{'corpusId' => $1, 'sentId' => $2});
				}
				elsif($curXml =~ /<dependencies \/>/){
					push(@failedSents,{'corpusId' => $1, 'sentId' => $2});
				}else{
					push(@parsedSents,{'corpusId' => $1, 'sentId' => $2, 'xml' => $curXml});
				}
				$curXml = undef;
			}
			else{
				$curXml .= $line."\n";
			}
		}
		push(@analysisOutputs, {'suspect' => $job->{'suspect'}, 'joker' => $job->{'joker'}, 
					'modifiedSents' => $job->{'modifiedSents'}, 
				        'parser' => $bridge->{'address'}.":".$bridge->{'port'}, 
					'success' => \@parsedSents,
					'failures' => \@failedSents,
					'timeouts' => \@tmoSents});
		delete $self->{'jobs'}->{$jobIdent};
	}else{
		$self->debug(5,"Bridge ".$bridge->{'descr'}." finished a dag analysis that wasn't expected anymore ($jobIdent)");
	}
	delete $bridge->{'doing'};
	print $socket "Thanks\n\@END\@\n"; ## let's be polite

	return @analysisOutputs;
}

return 1;


## CODE garbage


	#if(!exists($self->{'bridgeLaunched'})){ ## first turn of analysis ??
	#	$self->launchBridges(); # yes so let's launch some local bridges if described in dispatch conf
	#	$self->{'bridgeLaunched'} = 1;
	#}else{ # no so let's wake up all the bridges
	#	$self->wakeUpSomeBridge(1);
	#}

	#my @analysisOutputs;
	#my $dagsInProcess = {};


		
	#if(!@$dagsArrayRef){ ## if everything has already been given, check if some bridges/parsers are not late
	#	while(my($key,$value) = each %$dagsInProcess){
	#		if($value->{'when'} + $self->{'hosts'}->{$value->{'who'}}->{'bridgeTimeOut'} < time()){
	#			push @$dagsArrayRef, $value->{'which'};
	#				$self->warnings($value->{'who'}." is taking to much time to analyze dag $key, I put it back in the list");
	#				delete($dagsInProcess->{$key});
	#			}				
	#		}	
	#	}

	#IO:while(@$dagsArrayRef || (keys %$dagsInProcess)){

