package SearchCatsByStem;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.99;

=pod

=head2 B<SearchCatsByStem.pm>

Module used to detect categories for the suspects according to their lexical roots and the suffixes.

=cut

# The variables required by the module to work
sub variables { return {};}

# The module name to send to errors/warnings Functions
my $modName = "SearchCatsByStem";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName);
	bless $self, $class;

	return $self;	
}

################################Functions###############################################

=pod 

=over 1

=item * 
B<getStemAndCats($suspectHR)>

Given an suspect reference $suspectHR add the stem 'stem' and the detected categories 'detectedCats' to the suspect.

=back

=cut

sub getStemAndCats{
	my ($self,$suspectHR) = @_; 
	
 	my $resStem = $self->stemmer($suspectHR->{'key'});
	
	my @detectedCats;
	my @types = @{$resStem->{'types'}};
	if(@types){
		foreach my $type (@types){
			push(@detectedCats,$type->{'cat'});
		}
	}
	else{ # etude des signatures
		my @leftOverSuffix = @{$resStem->{'leftOverSuf'}};
		my $couldbeNc = 0;
		my $couldBeAdj = 0;
		foreach my $leftOverSuffix (@leftOverSuffix){
			if($leftOverSuffix =~ /s/){
				$couldbeNc++;
				$couldBeAdj++;
			}
		}
		if($couldbeNc >= 1){
			push(@detectedCats,'nc');
		}
		if($couldBeAdj >= 1){
			push(@detectedCats,'adj');
		}
	}

	$suspectHR->{'stem'} = $resStem->{'stem'};
	$suspectHR->{'detectedCats'} = \@detectedCats;
}

my $ic_suffix = {'suffix' => 'ance'};
my $if2_suffix = {'suffix' => 'if'};
my $at2_suffix = {'suffix' => 'at'};
my $eus_suffix = {'suffix' => 'eus'};
my $alb_suffix = {'suffix' => 'alb'};
my $iqu_suffix = {'suffix' => 'iqu'};
my $ier2_suffix = {'suffix' => 'ier'};
my $abil_suffix = {'suffix' => 'abil'};
my $iv_suffix = {'suffix' => 'iv'};


my $aire_suffix = { 'type' => ['nc-aire','adj-aire','v'], 'suffix' => 'aire'};
my $iaire_suffix = { 'type' => ['adj-iaire'], 'suffix' => 'iaire'};
my $aise_suffix = { 'type' => ['nc-ais','adj-ais'], 'suffix' => 'aise'};

my $ade_suffix = { 'type' => ['nc-ade'], 'suffix' => 'ade'};
my $al_suffix = { 'type' => ['adj-al'], 'suffix' => 'al'};
my $ale_suffix = { 'type' => ['adj-al'], 'suffix' => 'ale'};

my $age_suffix = { 'type' => ['nc-age'], 'suffix' => 'age'};
my $aille_suffix = { 'type' => ['nc-aille'], 'suffix' => 'aille'};

my $ance_suffix = { 'type' => ['nc-ance'], 'suffix' => 'ance'};
my $ique_suffix = { 'type' => ['adj-ique'], 'suffix' => 'ique'};
my $isme_suffix = { 'type' => ['nc-isme'], 'suffix' => 'isme'};
my $able_suffix = { 'type' => ['adj-able'], 'suffix' => 'able'};
my $iste_suffix = { 'type' => ['nc-iste','adj'], 'suffix' => 'iste'};
my $eux_suffix = { 'type' => ['adj-eux'], 'suffix' => 'eux'};

my $ain_suffix = { 'type' => ['nc-ain','adj-ain'], 'suffix' => 'ain'};
my $aine_suffix = { 'type' => ['nc-ain','adj-ain'], 'suffix' => 'aine'};

my $atrice_suffix = { 'type' => ['nc-eur'], 'suffix' => 'atrice'};
my $ateur_suffix = { 'type' => ['nc-eur'], 'suffix' => 'ateur'};
my $ation_suffix = { 'type' => ['nc-ation'], 'suffix' => 'ation'};
my $ition_suffix = { 'type' => ['nc-ition'], 'suffix' => 'ition'};
my $ssion_suffix = { 'type' => ['nc-ssion'], 'suffix' => 'ssion'};

my $logie_suffix = { 'type' => ['nc-logie'], 'suffix' => 'logie'};

my $usion_suffix = { 'type' => ['nc-usion'], 'suffix' => 'usion'};
my $ution_suffix = { 'type' => ['nc-ution'], 'suffix' => 'ution'};

my $ence_suffix = { 'type' => ['nc-ence'], 'suffix' => 'ence'};

my $et_suffix = { 'type' => ['nc-et'], 'suffix' => 'et'};
my $ette_suffix = { 'type' => ['nc-ette'], 'suffix' => 'ette'};

my $ement_suffix = { 'type' => ['nc-ement','adv'], 'suffix' => ''};

my $ite_suffix = { 'type' => ['nc-ite'], 'suffix' => 'it�'};

my $ible_suffix = { 'type' => ['adj-ible'], 'suffix' => 'ible'};

my $ien_suffix = { 'type' => ['nc-ien','adj-ien'], 'suffix' => 'ien'};
my $ienne_suffix = { 'type' => ['nc-ien','adj-ien'], 'suffix' => 'ienne'};

my $in_suffix = { 'type' => ['nc-in','adj-in'], 'suffix' => 'in'};
my $ine_suffix = { 'type' => ['nc-in','adj-in'], 'suffix' => 'ine'};
my $otin_suffix = { 'type' => ['nc-otin','adj-otin'], 'suffix' => 'otin'};
my $otine_suffix = { 'type' => ['nc-otin','adj-otin'], 'suffix' => 'otine'};

my $if_suffix = { 'type' => ['adj-if'], 'suffix' => 'if'};
my $ive_suffix = { 'type' => ['adj-if'], 'suffix' => 'ive'};
my $ise_suffix = { 'type' => ['adj-ise'], 'suffix' => 'ise'};

my $eaux_suffix = { 'type' => ['nc-eau'], 'suffix' => 'x'}; #am�liorer le traitement de eau

my $aux_suffix = { 'type' => ['adj-au'], 'suffix' => 'aux'};

my $eur_suffix = { 'type' => ['adj-eur','nc-eur'], 'suffix' => 'eur'}; # et eur?????
my $euse_suffix = { 'type' => ['adj-eur','nc-eur'], 'suffix' => 'euse'}; # et eur?????
my $sse_suffix = { 'type' => ['nc'], 'suffix' => 'sse'}; 

my $issement_suffix = { 'type' => ['adv'], 'suffix' => 'issement'};#bof

my $amment_suffix = { 'type' => ['adv'], 'suffix' => 'amment'}; #bof
my $emment_suffix = { 'type' => ['adv'], 'suffix' => 'emment'};#bof
my $ment_suffix = { 'type' => ['nc-ment','[s]adv'], 'suffix' => 'ment'};

my $oir_suffix = { 'type' => ['nc-oir'], 'suffix' => 'oir'};
my $oire_suffix = { 'type' => ['nc-oire'], 'suffix' => 'oire'};

my $uble_suffix = { 'type' => ['adj-uble'], 'suffix' => 'uble'};

my $el_suffix = { 'type' => ['adj-el'], 'suffix' => 'el'};
my $iel_suffix = { 'type' => ['adj-iel'], 'suffix' => 'iel'};
my $uel_suffix = { 'type' => ['adj-uel'], 'suffix' => 'uel'};
my $elle_suffix = { 'type' => ['adj-el'], 'suffix' => 'elle'};
my $ielle_suffix = { 'type' => ['adj-iel'], 'suffix' => 'ielle'};
my $uelle_suffix = { 'type' => ['adj-uel'], 'suffix' => 'uelle'};



my $icircmes_suffix = { 'type' => ['v'], 'suffix' => '�mes'};
my $icirct_suffix = { 'type' => ['v'], 'suffix' => '�t'};
my $icirctes_suffix = { 'type' => ['v'], 'suffix' => '�tes'};
my $i_suffix = { 'type' => ['v'], 'suffix' => 'i'};
my $ie_suffix = { 'type' => ['v','nc-ie'], 'suffix' => 'ie'};
#my $ies_suffix = { 'type' => ['v'], 'suffix' => 'ies'};
my $ir_suffix = { 'type' => ['v'], 'suffix' => 'ir'};
my $ira_suffix = { 'type' => ['v'], 'suffix' => 'ira'};
my $irai_suffix = { 'type' => ['v'], 'suffix' => 'irai'};
my $iraient_suffix = { 'type' => ['v'], 'suffix' => 'iraient'};
my $irais_suffix = { 'type' => ['v'], 'suffix' => 'irais'};
my $irait_suffix = { 'type' => ['v'], 'suffix' => 'irait'};
my $iras_suffix = { 'type' => ['v'], 'suffix' => 'iras'};
my $irent_suffix = { 'type' => ['v'], 'suffix' => 'irent'};
my $irez_suffix = { 'type' => ['v'], 'suffix' => 'irez'};
my $iriez_suffix = { 'type' => ['v'], 'suffix' => 'iriez'};
my $irions_suffix = { 'type' => ['v'], 'suffix' => 'irions'};
my $irons_suffix = { 'type' => ['v'], 'suffix' => 'irons'};
my $int_suffix = { 'type' => ['v'], 'suffix' => 'int'};
my $iront_suffix = { 'type' => ['v'], 'suffix' => 'iront'};
my $is_suffix = { 'type' => ['v'], 'suffix' => 'is'};
my $issaient_suffix = { 'type' => ['v'], 'suffix' => 'issaient'};
my $issais_suffix = { 'type' => ['v'], 'suffix' => 'issais'};
my $issait_suffix = { 'type' => ['v'], 'suffix' => 'issait'};
my $issant_suffix = { 'type' => ['adj-issant'], 'suffix' => 'issant'};
my $issante_suffix = { 'type' => ['adj-issant'], 'suffix' => 'issante'};
#my $issantes_suffix = { 'type' => ['adj'], 'suffix' => 'issantes'};
#my $issants_suffix = { 'type' => ['adj'], 'suffix' => 'issants'};
my $isse_suffix = { 'type' => ['v'], 'suffix' => 'isse'};
my $issent_suffix = { 'type' => ['v'], 'suffix' => 'issent'};
my $isses_suffix = { 'type' => ['v'], 'suffix' => 'isses'};
my $issez_suffix = { 'type' => ['v'], 'suffix' => 'issez'};
my $issiez_suffix = { 'type' => ['v'], 'suffix' => 'issiez'};
my $issions_suffix = { 'type' => ['v','nc'], 'suffix' => 'issions'};
my $issons_suffix = { 'type' => ['v'], 'suffix' => 'issons'};
my $it_suffix = { 'type' => ['v'], 'suffix' => 'it'};


my $ions_suffix = { 'type' => ['v','<t|s>nc'], 'suffix' => 'ions'};

my $eaigu_suffix = { 'type' => ['v','<t>nc-te'], 'suffix' => '�'};
my $ee_suffix = { 'type' => ['v','nc-ee'], 'suffix' => '�e'};
#my $ees_suffix = { 'type' => ['v'], 'suffix' => '�es'};
#my $es_suffix = { 'type' => ['v'], 'suffix' => '�s'};
my $erent_suffix = { 'type' => ['v'], 'suffix' => '�rent'};
my $er_suffix = { 'type' => ['v'], 'suffix' => 'er'};
my $era_suffix = { 'type' => ['v'], 'suffix' => 'era'};
my $erai_suffix = { 'type' => ['v'], 'suffix' => 'erai'};
my $eraient_suffix = { 'type' => ['v'], 'suffix' => 'eraient'};
my $erais_suffix = { 'type' => ['v'], 'suffix' => 'erais'};
my $erait_suffix = { 'type' => ['v'], 'suffix' => 'erait'};
my $eras_suffix = { 'type' => ['v'], 'suffix' => 'eras'};
my $erez_suffix = { 'type' => ['v'], 'suffix' => 'erez'};
my $eriez_suffix = { 'type' => ['v'], 'suffix' => 'eriez'};
my $erions_suffix = { 'type' => ['v'], 'suffix' => 'erions'};
my $erons_suffix = { 'type' => ['v'], 'suffix' => 'erons'};
my $eront_suffix = { 'type' => ['v'], 'suffix' => 'eront'};
my $ez_suffix = { 'type' => ['v'], 'suffix' => 'ez'};
my $iez_suffix = { 'type' => ['v'], 'suffix' => 'iez'};

my $re_suffix = { 'type' => ['<t>v','<t>nc-re'], 'suffix' => 're'};
my $ndre_suffix = { 'type' => ['v'], 'suffix' => 'ndre'};

my $ames_suffix = { 'type' => ['v'], 'suffix' => '�mes'};
my $at_suffix = { 'type' => ['v'], 'suffix' => '�t'};
my $ates_suffix = { 'type' => ['v'], 'suffix' => '�tes'};
my $a_suffix = { 'type' => ['v'], 'suffix' => 'a'};
my $ai_suffix = { 'type' => ['v'], 'suffix' => 'ai'};
my $aient_suffix = { 'type' => ['v'], 'suffix' => 'aient'};
my $ais_suffix = { 'type' => ['v','nc-ais','adj-ais'], 'suffix' => 'ais'};
my $ait_suffix = { 'type' => ['v'], 'suffix' => 'ait'};
my $ant_suffix = { 'type' => ['adj-ant','nc-ant'], 'suffix' => 'ant'};
my $ante_suffix = { 'type' => ['adj-ant'], 'suffix' => 'ante'};
#my $antes_suffix = { 'type' => ['adj'], 'suffix' => 'antes'};
#my $ants_suffix = { 'type' => ['adj'], 'suffix' => 'ants'};
my $as_suffix = { 'type' => ['v'], 'suffix' => 'as'};
my $asse_suffix = { 'type' => ['v'], 'suffix' => 'asse'};
my $assent_suffix = { 'type' => ['v'], 'suffix' => 'assent'};
my $asses_suffix = { 'type' => ['v'], 'suffix' => 'asses'};
my $assiez_suffix = { 'type' => ['v'], 'suffix' => 'assiez'};
my $assions_suffix = { 'type' => ['v'], 'suffix' => 'assions'};

my $ons_suffix = { 'type' => ['v'], 'suffix' => 'ons'};


my $ion_suffix = {'suffix' => 'ion'};
my $ier_suffix = {'suffix' => 'ier'};
my $iere_suffix = {'suffix' => 'i�re'};
my $e_suffix = {'suffix' => 'e'};
my $etrema_suffix = {'suffix' => '�'};


my $regular = "(?:b|c|d|f|g|h|j|k|l|m|n|p|k|r|s|t|v|w|w|z|�)";
my $vowel = "(?:a|e|i|o|u|y|�|�|�|�|�|�|�|�|�|�|�)";
my $whev = "(?:$regular|$vowel)";

my $rv = "$whev$vowel$vowel|$vowel(?:$regular+?|$regular+?$vowel)";
my $rvandr1 = "(?:$regular+?$vowel|$whev*$vowel$regular)";
my $r1 = "$regular$vowel$whev*?";
my $r2 = "$regular$vowel$whev*?$r1";



sub isIn{
	my ($self,$lemme,$motif) = @_;

	if($lemme->{'inword'} =~ /.*$motif$/){
		return 1;
	}
	else{
		return 0;
	}	
}


sub validateIfIn{
	my($self,$lemme,$motif,$suffixe) = @_;

	if(!($lemme->{'inword'}=~ /.*$motif$/)){
		return 0;	
	}
	else{
		if(exists($suffixe->{'type'})){
			my @types = @{$suffixe->{'type'}};
			my @suffixes = ($suffixe->{'suffix'},@{$lemme->{'suffixesGen'}});
			$lemme->{'suffixesGen'} = [];
		
			my $typeFound;
			TYPE:foreach my $type (@types){
				if($type =~ s/^\[(.*)\]//){
					foreach my $suffixGen (@{$lemme->{'suffixesGen'}}){
						if($suffixGen =~ /^$1$/){
							next TYPE;
						}
					}
				}
				if($type =~ s/^\<(.*)\>//){
					if($lemme->{'inword'} =~ /^$1/){
						push @{$lemme->{'types'}} , {'cat' => $type, 'suffixes' => \@suffixes};
					       	$typeFound = 1;	
					}
				}
				else{
					push @{$lemme->{'types'}} , {'cat' => $type, 'suffixes' => \@suffixes}; 
					$typeFound = 1;
				}

			}
			if($typeFound){
				return 1;
			}
		}
		else{
			push @{$lemme->{'suffixesGen'}}, $suffixe->{'suffix'};
			return 1;
		}
	}
	return 0;
}


#################################### Phase "0" ########################################################

sub stemmer{
	my ($self,$word) = @_;

	
	$word =~ s/($vowel)u($vowel)/$1U$2/g;
	$word =~ s/($vowel)i($vowel)/$1I$2/g;
	$word =~ s/qu/qU/g;
	$word =~ s/($vowel)y/$1Y/g;
	$word =~ s/y($vowel)/Y$1/g;

	my $inword = reverse $word;
	
	my %lemme = (   'inword' => $inword,
			'types' => [],
	      		'suffixesGen' => []
		);

	$self->phase1(\%lemme);

	my $stem = reverse ($lemme{'inword'});
	
	$stem  =~ s/($vowel)U($vowel)/$1u$2/g;
	$stem  =~ s/($vowel)I($vowel)/$1i$2/g;
	$stem  =~ s/qU/qu/g;
	$stem  =~ s/($vowel)Y/$1y/g;
	$stem  =~ s/Y($vowel)/y$1/g;

	return {"stem" => $stem, "types" => $lemme{'types'}, "leftOverSuf" => $lemme{'suffixesGen'}};
}


#################################### Phase 1 ########################################################

sub ic_suffix{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^ci(.*)/$1/){
	       	if($self->validateIfIn($lemme,$r2,$ic_suffix)){
			return 1; 
		}
	}
	return 0;
}

sub at_suffix{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^ta(.*)/$1/){
	       	if($self->validateIfIn($lemme,$r2,$at2_suffix)){
			return 1;
		}
	}
	return 0;
}

sub if_suffix{
	my ($self,$lemme,$previous) = @_;
	
	if($self->validateIfIn($lemme,$r2,$previous)){
		if($self->at_suffix($lemme)){
			$self->ic_suffix($lemme);
		}	
		return 1;
	}
	else{
		return 0;
	}
}

sub phase1_e{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^Uqi(.*)/$1/){ #ique
		return $self->validateIfIn($lemme,$r2,$ique_suffix);
	}
	elsif($lemme->{'inword'} =~ s/^c(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^irta(.*)/$1/){ #atrice
			if($self->validateIfIn($lemme,$r2,$atrice_suffix)){
				$self->ic_suffix($lemme);
				return 1;
			}
		}
		elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){
			if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #ance
				return $self->validateIfIn($lemme,$r2,$ance_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ #ence
				if($self->validateIfIn($lemme,$r2,$ence_suffix)){
					$lemme->{'inword'} = "tne".$lemme->{'inword'} ;	 return 1;
				}
			}	
		}
	}
	elsif($lemme->{'inword'} =~ s/^da(.*)/$1/){ #ade	Lionel
		return $self->validateIfIn($lemme,"",$ade_suffix);	
	}
	elsif($lemme->{'inword'} =~ s/^igol(.*)/$1/){ #logie
		return $self->validateIfIn($lemme,$r2,$logie_suffix);	
	}
	elsif($lemme->{'inword'} =~ s/^ga(.*)/$1/){ #age	Lionel
		return $self->validateIfIn($lemme,"",$age_suffix);	
	}
	elsif($lemme->{'inword'} =~ s/^l(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #ale	Lionel
			return $self->validateIfIn($lemme,$r2,$ale_suffix);
		}
		if($lemme->{'inword'} =~ s/^b(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #able
				return $self->validateIfIn($lemme,$r2,$able_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ #ible	Lionel
				return $self->validateIfIn($lemme,$r2,$ible_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^u(.*)/$1/){ #uble	Lionel
				return $self->validateIfIn($lemme,$r2,$uble_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^l(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^ia(.*)/$1/){ #aille	Lionel
				return $self->validateIfIn($lemme,"",$aille_suffix);	
			}
			elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){
				if($lemme->{'inword'} =~ s/^i(.*)/$1/){ # ielle 	Lionel
					return $self->validateIfIn($lemme,"",$ielle_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^u(.*)/$1/){ # uelle	Lionel
					return $self->validateIfIn($lemme,"",$uelle_suffix);
				}
				else{ #elle 	Lionel
					return $self->validateIfIn($lemme,"",$elle_suffix);
				}
			}
		}
	}
	elsif($lemme->{'inword'} =~ s/^msi(.*)/$1/){ #isme
		return $self->validateIfIn($lemme,$r2,$isme_suffix);
	}
	elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){
		if($lemme->{'inword'} =~ s/^nei(.*)/$1/){ #ienne		Lionel
			if($self->validateIfIn($lemme,$r2,$ienne_suffix)){
				$self->ic_suffix($lemme);
				return 1;
			}
			else{
				return $self->validateIfIn($lemme,$r1,$ienne_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^to(.*)/$1/){ #otine 	Lionel
				return $self->validateIfIn($lemme,"",$otine_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^a(.*)/$1/){ #aine	Lionel
				return $self->validateIfIn($lemme,"",$aine_suffix);
			}
			else{ #ine Lionel
				return $self->validateIfIn($lemme,"",$ine_suffix);
			}
		}
	}
	elsif($lemme->{'inword'} =~ s/^ri(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^a(.*)/$1/){
			if($lemme->{'inword'} =~ s/^i(.*)/$1/){ #iaire	Lionel
				return $self->validateIfIn($lemme,"",$iaire_suffix);
			}	
			else{#iaire	Lionel
				return $self->validateIfIn($lemme,"",$aire_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^o(.*)/$1/){ #oire	Lionel
			return $self->validateIfIn($lemme,"",$oire_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^s(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^ue(.*)/$1/){ #euse
			if(!$self->validateIfIn($lemme,$r2,$euse_suffix)){
				if($self->validateIfIn($lemme,$r1,$euse_suffix)){
					$lemme->{'inword'} = "xue".$lemme->{'inword'} ;	 return 1;
				}
			}
			else{
				return 1;
			}
		}
		elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ #ise 		Lionel
			return $self->validateIfIn($lemme,"",$ise_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^se(.*)/$1/){ #esse 		Lionel
			return $self->validateIfIn($lemme,$rv,$sse_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^ia(.*)/$1/){ #aise 		Lionel
			return $self->validateIfIn($lemme,"",$aise_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^si(.*)/$1/){ #iste
			return $self->validateIfIn($lemme,$r2,$iste_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^tte(.*)/$1/){ #ette	Lionel
			return $self->validateIfIn($lemme,"",$ette_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^vi(.*)/$1/){ #ive
		return $self->if_suffix($lemme,$ive_suffix);
	}

	return 0;
}

sub phase1_fi{  #if
	my ($self,$lemme) = @_;

	return $self->if_suffix($lemme,$if_suffix);

}

sub phase1_noi{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^s(.*)/$1/){ #usion
		if($lemme->{'inword'} =~ s/^u(.*)/$1/){ #usion
			if($self->validateIfIn($lemme,$r2,$usion_suffix)){
				$lemme->{'inword'} = "u".$lemme->{'inword'} ; return 1;
			}
		}
		elsif($lemme->{'inword'} =~ s/^s(.*)/$1/){ #ssion	Lionel
			return $self->validateIfIn($lemme,"",$ssion_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #ation
			if($self->validateIfIn($lemme,$r2,$ation_suffix)){
				$self->ic_suffix($lemme);
				return 1;
			}
			return 0;
		}elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ #ition	Lionel
			return $self->validateIfIn($lemme,"",$ition_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^u(.*)/$1/){ #ution
			if($self->validateIfIn($lemme,$r2,$ution_suffix)){
				$lemme->{'inword'} = "u".$lemme->{'inword'} ; return 1;
			}
		}
	}	
	
	return 0;
}


sub phase1_n{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^ei(.*)/$1/){ #ien 	Lionel
		if($self->validateIfIn($lemme,$r2,$ien_suffix)){
			$self->ic_suffix($lemme);
			return 1;
		}
		else{
			return $self->validateIfIn($lemme,$r1,$ien_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^oi(.*)/$1/){
		return $self->phase1_noi($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^to(.*)/$1/){ #otin 	Lionel
			return $self->validateIfIn($lemme,"",$otin_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^a(.*)/$1/){ #ain 	Lionel
			return $self->validateIfIn($lemme,"",$ain_suffix);
		}
		else{ #in Lionel
			return $self->validateIfIn($lemme,"",$in_suffix);
		}
	}


	return 0;
}

sub phase1_r{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^ue(.*)/$1/){
       		if($lemme->{'inword'} =~ s/^ta(.*)/$1/){ # ateur
			return $self->phase1($lemme);
		}
		elsif(!$self->validateIfIn($lemme,$r2,$eur_suffix)){
			if($self->validateIfIn($lemme,$r1,$eur_suffix)){
				$lemme->{'inword'} = "xue".$lemme->{'inword'} ;	 return 1;
			}
		}
		else{
			return 1;
		}
	}
	elsif($lemme->{'inword'} =~ s/^io(.*)/$1/){ # oir	Lionel
		return $self->validateIfIn($lemme,"",$oir_suffix);
	}

	return 0;
}


sub phase1_ueta{
	my ($self,$lemme) = @_;

	if($self->validateIfIn($lemme,$r2,$ateur_suffix)){
		$self->ic_suffix($lemme);
		return 1;
	}
	
	return 0;
}

sub phase1_s{
	my ($self,$lemme) = @_;

	my $change;

	push @{$lemme->{'suffixesGen'}}, 's';	
	if($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
		$change = $self->phase1_e($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^fi(.*)/$1/){
		$change = $self->phase1_fi($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^l(.*)/$1/){ 
		$change = $self->phase1_l($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){ 
		$change = $self->phase1_n($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^r(.*)/$1/){ 
		$change = $self->phase1_r($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^tnem(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
			$change = $self->phase1_tneme($lemme);
		}else{
			$change = $self->phase1_tnem($lemme);
		}
	}
	elsif($lemme->{'inword'} =~ s/^�ti(.*)/$1/){ 
		$change = $self->phase1_eti($lemme);
	}

	return $change;	
}

sub phase1_tnem{ #ment
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^($vowel)(.*)/$2/){ 
		if($self->validateIfIn($lemme,$rv,$ment_suffix)){
			$lemme->{'inword'} = $1.$lemme->{'inword'};
			return 1;
		}
	}

	return 0;
}

sub phase1_tneme{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issement
		if($self->validateIfIn($lemme,$r1,$issement_suffix) && ($lemme->{'inword'} =~ /^($regular)/)){
			return 1;
		}
	}else{  #ement
		if($self->validateIfIn($lemme,$rv,$ement_suffix)){
			if($lemme->{'inword'} =~ s/^vi(.*)/$1/){
				$self->if_suffix($lemme);
			}elsif($lemme->{'inword'} =~ s/^sue(.*)/$1/){
				if(!$self->validateIfIn($lemme,$r2,$eus_suffix)){
					if($self->validateIfIn($lemme,$r1,$eus_suffix)){
						$lemme->{'inword'} = 'xue'.$lemme->{'inword'};
					}
				}
			}elsif($lemme->{'inword'} =~ s/^lba(.*)/$1/){
				$self->validateIfIn($lemme,$r2,$alb_suffix);
			}elsif($lemme->{'inword'} =~ s/^Uqi(.*)/$1/){
				$self->validateIfIn($lemme,$r2,$iqu_suffix);
			}elsif($lemme->{'inword'} =~ s/^(r�i|r�I)(.*)/$2/){
				if($self->validateIfIn($lemme,$rv,$ier2_suffix)){
					$lemme->{'inword'} = 'i'.$lemme->{'inword'};
				}
				else{
					$lemme->{'inword'} = $1.$lemme->{'inword'};
				}
			}
			return 1;
		}
	}
	return 0;
}

sub phase1_tnemm{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #amment 
		if($self->validateIfIn($lemme,$rv,$amment_suffix)){
			$lemme->{'inword'} = 'tna'.$lemme->{'inword'}; return 1;
		}	
	}elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ #emment 
		if($self->validateIfIn($lemme,$rv,$emment_suffix)){
			$lemme->{'inword'} = 'tne'.$lemme->{'inword'}; return 1;
		}
	}		
	
	return 0;
}

sub phase1_xu{
	my ($self,$lemme) = @_;
	
	if($lemme->{'inword'} =~ s/^a(.*)/$1/){
		if($lemme->{'inword'} =~ s/^e(.*)/$1/){ #eaux
			$lemme->{'inword'} = 'uae'.$1.$lemme->{'inword'}; return 1;
		}elsif($self->validateIfIn($lemme,$r1,$aux_suffix)){ #aux
			$lemme->{'inword'} = 'la'.$1.$lemme->{'inword'}; return 1;
		}
	}
	elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ #eux
		return $self->validateIfIn($lemme,$r2,$eux_suffix);
	}

	return 0;
}

sub phase1_eti{
	my ($self,$lemme) = @_;

	if($self->validateIfIn($lemme,$r2,$ite_suffix)){
		if($lemme->{'inword'} =~ s/^liba(.*)/$1/){
			if(!$self->validateIfIn($lemme,$r2,$abil_suffix)){
				$lemme->{'inword'} = 'lba'.$lemme->{'inword'};
			}
		}elsif($lemme->{'inword'} =~ /^ci.*/){
			$self->ic_suffix($lemme);
		}elsif($lemme->{'inword'} =~ s/^vi(.*)/$1/){
			$self->validateIfIn($lemme,$r2,$iv_suffix);
		}
		return 1;
	}
	return 0;
}

sub phase1_l{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^i(.*)/$1/){ # iel 	Lionel
			return $self->validateIfIn($lemme,"",$iel_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^u(.*)/$1/){ # uel	Lionel
			return $self->validateIfIn($lemme,"",$uel_suffix);
		}
		else{ #el
			return $self->validateIfIn($lemme,"",$el_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^a(.*)/$1/){ #al 	Lionel
		return $self->validateIfIn($lemme,"",$al_suffix);
	}
}


sub phase1{
	my ($self,$lemme) = @_;

	my $change;

	my $startValue = $lemme->{'inword'};
	my @startTypes = @{$lemme->{'types'}};
	my @startSuff = @{$lemme->{'suffixesGen'}};

	if($lemme->{'inword'} =~ s/^e(.*)/$1/){
		 $change = $self->phase1_e($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^fi(.*)/$1/){
		 $change = $self->phase1_fi($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^l(.*)/$1/){ 
		$change = $self->phase1_l($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){ 
		$change = $self->phase1_n($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^r(.*)/$1/){ 
		$change = $self->phase1_r($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^s(.*)/$1/){ 
		$change = $self->phase1_s($lemme);	
	}
	elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^nem(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
				$change = $self->phase1_tneme($lemme);
			}
			elsif($lemme->{'inword'} =~ s/^m(.*)/$1/){
				$change = $self->phase1_tnemm($lemme);
			}else{
				$change = $self->phase1_tnem($lemme);
			}
		}
		elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ #et
			$change = $self->validateIfIn($lemme,"",$et_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^xu(.*)/$1/){ 
		$change = $self->phase1_xu($lemme);
	}
	elsif($lemme->{'inword'} =~ s/^�ti(.*)/$1/){ 
		$change = $self->phase1_eti($lemme);
	}

	if(!$change){
		$lemme->{'inword'} = $startValue;
		$lemme->{'types'} = \@startTypes;
		$lemme->{'suffixesGen'} = \@startSuff;
	}

	my $suffixes = $lemme->{'suffixes'};
	if(!$change || exists($suffixes->{'amment'}) || exists($suffixes->{'emment'}) || exists($suffixes->{'ment'})){
		return $self->phase2a($lemme);
	}
	else{
		$self->phase3($lemme);
		return 1;
	}
}


#################################### Phase 2 ########################################################


sub validate2a{
	my ($self,$lemme,$suffixFound) = @_;

	if($lemme->{'inword'} =~ /^$regular.*/){
		return $self->validateIfIn($lemme,$rv,$suffixFound);
	}

	return 0;
}

sub phase2a{
	my ($self,$lemme) = @_;

	my $change; 

	my $startValue = $lemme->{'inword'};
	my @startTypes = @{$lemme->{'types'}};
	my @startSuff = @{$lemme->{'suffixesGen'}};

	if($lemme->{'inword'} =~ s/^ari(.*)/$1/){ #ira
		$change = $self->validate2a($lemme,$ira_suffix);
	}
	elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){
		if($lemme->{'inword'} =~ s/^i(.*)/$1/){ #ie
			$change = $self->validate2a($lemme,$ie_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #isse
			$change = $self->validate2a($lemme,$isse_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^tnassi(.*)/$1/){ #issante
			$change = $self->validate2a($lemme,$issante_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){
		if($lemme->{'inword'} =~ s/^ari(.*)/$1/){ #irai
			$change = $self->validate2a($lemme,$irai_suffix);
		}
		else{ #i
			$change = $self->validate2a($lemme,$i_suffix);
		}

	}
	elsif($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #ir
		$change = $self->validate2a($lemme,$ir_suffix);
	}
	elsif($lemme->{'inword'} =~ s/^s(.*)/$1/){
		if($lemme->{'inword'} =~ s/^ari(.*)/$1/){ #iras
			$change = $self->validate2a($lemme,$iras_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^i(.*)/$1/){ #ies
				push @{$lemme->{'suffixesGen'}}, 's';
				$change = $self->validate2a($lemme,$ie_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^m�(.*)/$1/){ #�mes
				$change = $self->validate2a($lemme,$icircmes_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #isses
				$change = $self->validate2a($lemme,$isses_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^nassi(.*)/$1/){ #issantes
					push @{$lemme->{'suffixesGen'}}, 's';
					$change = $self->validate2a($lemme,$issante_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�tes
					$change = $self->validate2a($lemme,$icirctes_suffix);
				}
			}	
		}
		elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^a(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irais
					$change = $self->validate2a($lemme,$irais_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issais
					$change = $self->validate2a($lemme,$issais_suffix);
				}
			}
			else{ #is
				$change = $self->validate2a($lemme,$is_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^no(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^i(.*)/$1/){ #i
				if($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irions
					$change = $self->validate2a($lemme,$irions_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issions
					$change = $self->validate2a($lemme,$issions_suffix);
				}
			}
			elsif($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irons
				$change = $self->validate2a($lemme,$irons_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issons
				$change = $self->validate2a($lemme,$issons_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^tnassi(.*)/$1/){ #issants
			push @{$lemme->{'suffixesGen'}}, 's';
			$change = $self->validate2a($lemme,$issant_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){
		if($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^a(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irait
					$change = $self->validate2a($lemme,$irait_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issait
					$change = $self->validate2a($lemme,$issait_suffix);
				}
			}
			else{
				$change = $self->validate2a($lemme,$it_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^assi(.*)/$1/){ #issant
				$change = $self->validate2a($lemme,$issant_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^Ia(.*)/$1/){ 
					if($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #iraient
						$change = $self->validate2a($lemme,$iraient_suffix);
					}
					elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issaient
						$change = $self->validate2a($lemme,$issaient_suffix);
					}				
				}
				elsif($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irent
					$change = $self->validate2a($lemme,$irent_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issent
					$change = $self->validate2a($lemme,$issent_suffix);
				}	
			}
			elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){ #int 	Lionel
				$change = $self->validate2a($lemme,$int_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^ori(.*)/$1/){ #iront
				$change = $self->validate2a($lemme,$iront_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�t
			$change = $self->validate2a($lemme,$icirct_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^ze(.*)/$1/){
		if($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #iriez
				$change = $self->validate2a($lemme,$iriez_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issiez
				$change = $self->validate2a($lemme,$issiez_suffix);
			}
		}
		
		elsif($lemme->{'inword'} =~ s/^ri(.*)/$1/){ #irez
				$change = $self->validate2a($lemme,$irez_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^ssi(.*)/$1/){ #issez
				$change = $self->validate2a($lemme,$issez_suffix);
		}
	}

	if($change){
		$self->phase3($lemme);
		return 1;
	}
	else{
		$lemme->{'inword'} = $startValue;
		$lemme->{'types'} = \@startTypes;
		$lemme->{'suffixesGen'} = \@startSuff;

		return $self->phase2b($lemme);
	}
	

}


sub validate2b1{
	my ($self,$lemme,$suffixFound) = @_;
	
	return $self->validateIfIn($lemme,$rv,$suffixFound);
}

sub validate2b2{
	my ($self,$lemme,$suffixFound) = @_;
	
	$lemme->{'inword'} =~ s/^e(.*)/$1/;
	
	return $self->validate2b1($lemme,$suffixFound);

}

sub phase2b{
	my ($self,$lemme) = @_;

	my $change;

	my $startValue = $lemme->{'inword'};
	my @startTypes = @{$lemme->{'types'}};
	my @startSuff = @{$lemme->{'suffixesGen'}};

	if($lemme->{'inword'} =~ s/^a(.*)/$1/){
		if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #era
			$change = $self->validate2b1($lemme,$era_suffix);		
		}
		else{ #a
			$change = $self->validate2b2($lemme,$a_suffix);
		}			
	}
	elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^ssa(.*)/$1/){ #asse
			$change = $self->validate2b2($lemme,$asse_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^tna(.*)/$1/){ #ante
			$change = $self->validate2b2($lemme,$ante_suffix);			
		}	
		elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�e
			$change = $self->validate2b1($lemme,$ee_suffix);		
		}
		elsif($lemme->{'inword'} =~ s/^r(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^dn(.*)/$1/){ # ndre		Lionel 
				$change = $self->validate2b1($lemme,$ndre_suffix);
			}else{#re			Lionel
						$change = $self->validate2b1($lemme,$re_suffix)
			}
		}
	}
	elsif($lemme->{'inword'} =~ s/^ia(.*)/$1/){
		if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erai
			$change = $self->validate2b1($lemme,$erai_suffix);		
		}
		else{ #ai
			$change = $self->validate2b2($lemme,$ai_suffix);
		}		
	}
	elsif($lemme->{'inword'} =~ s/^re(.*)/$1/){ #er
		$change = $self->validate2b1($lemme,$er_suffix);			
	}
	elsif($lemme->{'inword'} =~ s/^s(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^a(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #eras
				$change = $self->validate2b1($lemme,$eras_suffix);	
			}
			else{ #as
				$change = $self->validate2b2($lemme,$as_suffix);
			}	
		}
		elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^m�(.*)/$1/){ #�mes
				$change = $self->validate2b2($lemme,$ames_suffix);		
			}
			elsif($lemme->{'inword'} =~ s/^ssa(.*)/$1/){ #asses
				$change = $self->validate2b2($lemme,$asses_suffix);		
			}	
			elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^na(.*)/$1/){ #antes
					push @{$lemme->{'suffixesGen'}}, 's';
					$change = $self->validate2b2($lemme,$ante_suffix);
				}
				elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�tes
					$change = $self->validate2b2($lemme,$ates_suffix);		
				}		
			}	
			elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�es
				push @{$lemme->{'suffixesGen'}}, 's';
				$change = $self->validate2b1($lemme,$ee_suffix);
			}			
		}
		elsif($lemme->{'inword'} =~ s/^ia(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erai
				$change = $self->validate2b1($lemme,$erai_suffix);		
			}
			else{ #ais
				$change = $self->validate2b2($lemme,$ais_suffix);
			}			
		}
		elsif($lemme->{'inword'} =~ s/^no(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erions
					$change = $self->validate2b1($lemme,$erions_suffix);	
				}	
				elsif($lemme->{'inword'} =~ s/^ssa(.*)/$1/){ #assions
					$change = $self->validate2b2($lemme,$assions_suffix);	
				}	
				else{ #ions
					if($self->isIn($lemme,$r2)){
						$change = $self->validate2b2($lemme,$ions_suffix); 
					}
				}	
			}
			elsif($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erons
				$change = $self->validate2b1($lemme,$erons_suffix);	
			}
			else{ # ons 		Lionel
				$change = $self->validate2b1($lemme,$ons_suffix);	
			}	
		}
		elsif($lemme->{'inword'} =~ s/^tna(.*)/$1/){ #ants
			push @{$lemme->{'suffixesGen'}}, 's';
			$change = $self->validate2b2($lemme,$ant_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�s
			push @{$lemme->{'suffixesGen'}}, 's';
			$change = $self->validate2b1($lemme,$eaigu_suffix);	
		}			
	}
	elsif($lemme->{'inword'} =~ s/^t(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^ia(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erait
				$change = $self->validate2b1($lemme,$erait_suffix);	
			}
			else{ #ait
				$change = $self->validate2b2($lemme,$ait_suffix);
			}
		}
		elsif($lemme->{'inword'} =~ s/^n(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^a(.*)/$1/){ #ant
				$change = $self->validate2b2($lemme,$ant_suffix);	
			}
			elsif($lemme->{'inword'} =~ s/^e(.*)/$1/){ 
				if($lemme->{'inword'} =~ s/^Ia(.*)/$1/){ 
					if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #eraient
						$change = $self->validate2b1($lemme,$eraient_suffix);	
					}
					else{ #aient
						$change = $self->validate2b2($lemme,$aient_suffix);
					}
				}
				elsif($lemme->{'inword'} =~ s/^r�(.*)/$1/){ #�rent
					$change = $self->validate2b1($lemme,$erent_suffix);	
				}
				elsif($lemme->{'inword'} =~ s/^ssa(.*)/$1/){ #assent
					$change = $self->validate2b1($lemme,$assent_suffix);	
				}		
			}	
			elsif($lemme->{'inword'} =~ s/^ore(.*)/$1/){ #eront
				$change = $self->validate2b1($lemme,$eront_suffix);		
			}		
		}
		elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�t
			$change = $self->validate2b2($lemme,$at_suffix);					
		}			
	}
	elsif($lemme->{'inword'} =~ s/^ze(.*)/$1/){ 
		if($lemme->{'inword'} =~ s/^i(.*)/$1/){ 
			if($lemme->{'inword'} =~ s/^re(.*)/$1/){ #eriez
				$change = $self->validate2b1($lemme,$eriez_suffix);	
			}
			elsif($lemme->{'inword'} =~ s/^ssa(.*)/$1/){ #assiez
				$change = $self->validate2b2($lemme,$assiez_suffix);		
			}
			else{ #ait
				$change = $self->validate2b2($lemme,$ait_suffix);
			}	
		}
		elsif($lemme->{'inword'} =~ s/^re(.*)/$1/){ #erez
			$change = $self->validate2b1($lemme,$erez_suffix);
		}
		else{ #ez
			$change = $self->validate2b1($lemme, $ez_suffix);
		}		
	}
	elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�
		$change = $self->validate2b1($lemme,$eaigu_suffix);
			
	}

	if($change){
		$self->phase3($lemme);
		return 1;
	}
	else{
		$lemme->{'inword'} = $startValue;
		$lemme->{'types'} = \@startTypes;
		$lemme->{'suffixesGen'} = \@startSuff;

		return $self->phase4($lemme);
	}

}

#################################### Phase 3 ########################################################


sub phase3{
	my ($self,$lemme) = @_;

	$lemme->{'inword'} =~ s/y(.*)/i$1/;
       	$lemme->{'inword'} =~ s/�(.*)/c$1/;
	
	
	return $self->phase5($lemme);
	
}

################################### Phase 4 ########################################################


sub validate4{
	my ($self,$lemme,$suffixFound) = @_;

	return $self->validateIfIn($lemme,$rv,$suffixFound);
}

sub validate4ier{
	my ($self,$lemme,$suffixFound) = @_;

	my $return = $self->validate4($lemme,$suffixFound);
	if($return){
		$lemme->{'inword'} = 'i'.$lemme->{'inword'}; 
	}
	
	return $return;
}


sub phase4{
	my ($self,$lemme) = @_;

	if($lemme->{'inword'} =~ s/^s([^aiou�s])/$1/){
		push @{$lemme->{'suffixesGen'}}, 's'; 
	}

	my $change;

	my $startValue = $lemme->{'inword'};
	my @startTypes = @{$lemme->{'types'}};
	my @startSuff = @{$lemme->{'suffixesGen'}};

	if($lemme->{'inword'} =~ s/^e(.*)/$1/){
		if($lemme->{'inword'} =~ s/^r�(.*)/$1/){
			if($lemme->{'inword'} =~ s/^I(.*)/$1/){ #I�re
				$change = $self->validate4ier($lemme,$iere_suffix);
			}
			elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){#i�re
				$change = $self->validate4ier($lemme,$iere_suffix);
			}
		}
		else{
			$change = $self->validate4($lemme,$e_suffix);		
		}
	}
	elsif($lemme->{'inword'} =~ s/^noi(.*)/$1/){#ion
		if($self->isIn($lemme,$rv) && $self->isIn($lemme,$r2) && ($lemme->{'inword'} =~ /^(s|t)/)){
			push @{$lemme->{'suffixesGen'}}, 'ion'; $change = 1;
		}
	}
	elsif($lemme->{'inword'} =~ s/^re(.*)/$1/){
		if($lemme->{'inword'} =~ s/^I(.*)/$1/){ #Ier
			$change = $self->validate4ier($lemme,$ier_suffix);
		}
		elsif($lemme->{'inword'} =~ s/^i(.*)/$1/){#ier
			$change = $self->validate4ier($lemme,$ier_suffix);
		}
	}
	elsif($lemme->{'inword'} =~ s/^�(.*)/$1/){ #�
		if($lemme->{'inword'} =~ /^ug/){
			$change = $self->validate4($lemme,$etrema_suffix);
		}
	}

	if(!$change){
		$lemme->{'inword'} = $startValue;
		$lemme->{'types'} = \@startTypes;
		$lemme->{'suffixesGen'} = \@startSuff;
	}

	my $retour5 = $self->phase5($lemme);
	if($change){
		return 1;
	}
	else{

		return $retour5;
	}	
}

################################### Phase 5 ########################################################

sub phase5{
	my ($self,$lemme) = @_;

	$lemme->{'inword'} =~ s/^l(l(e|ie).*)/$1/; 
	$lemme->{'inword'} =~ s/^n(n(e|o).*)/$1/;
	$lemme->{'inword'} =~ s/^t(te.*)/$1/;
	
	return $self->phase6($lemme);
}

################################### Phase 6 ########################################################

sub phase6{
	my ($self,$lemme) = @_;

	$lemme->{'inword'} =~ s/^($regular+)(�|�)(.*)/$1e$3/;
	
	return 1;
}


return 1;
