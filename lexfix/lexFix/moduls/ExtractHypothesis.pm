package ExtractHypothesis;

use MODULE;
use strict;
use XML::LibXML;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.9;

=pod

=head2 B<ExtractHypothesis.pm>

Module used to extract Hypothesis in the xml produced by the syntax analyser.

=cut

sub variables{ return {};}

# The module name to send to errors/warnings Functions
my $modName = "ExtractHypothesis";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName);
	$self->{'nbprem'} = [3];
	bless $self, $class;

	return $self;
}
################################Functions###############################################
my $basicPath="/dependencies";

my $clusterPath=$basicPath;
my $clusterStr="${clusterPath}/cluster";
my $clusterIdStr="id";
my $clusterLexStr="lex";

my $nodePath=$basicPath;
my $nodeStr="${nodePath}/node";
my $nodeCatStr="cat";
my $nodeClusterStr="cluster";
my $nodeLemmaStr="lemma";
my $nodeIdStr="id";
my $nodeDerivStr="deriv";

my $edgePath=$basicPath;
my $edgeStr="${edgePath}/edge";
my $edgeSrcStr="source";
my $edgeTargetStr="target";
my $edgeLabelStr="label";


my $derivStr="deriv";
my $derivIdStr="name";
my $derivSpanStr="span";
my $derivSopstr="source_op";
my $derivTopstr="target_op";


## Internal function looking in the xml object $rootNode the cluster representing the suspect lexical form
sub getClusterLink{
	my ($self,$lex,$rootNode) = @_;
	
	my @clusterNodes = $rootNode->findnodes("$clusterStr");
	my $res;

	my $clusterNode;
	while(!defined($res) and ($clusterNode = shift @clusterNodes)){
		if($clusterNode->getAttribute($clusterLexStr) eq $lex){
			$res = $clusterNode->getAttribute($clusterIdStr);
		}
	}

	undef @clusterNodes;

	return $res;
}



##Establish the list of nodes $nodesHashRef plus additional informations(derivations) 
##in $lexNodesHashRef for nodes concerned by $clusterLink.
sub getNodeNodes{
	my ($self,$rootNode,$clusterLink,$lexNodesHashRef,$nodesHashRef) = @_;
	my @nodeNodes = $rootNode->findnodes("$nodeStr");

	my($cat,$cluster,$id,$derivs);
	foreach my $nodeNode (@nodeNodes){
		$cat=$nodeNode->getAttribute($nodeCatStr);
		$cluster=$nodeNode->getAttribute($nodeClusterStr);
		$id=$nodeNode->getAttribute($nodeIdStr);
		$derivs=$nodeNode->getAttribute($nodeDerivStr);
		
		$nodesHashRef->{$id}= $cat;
		##if $nodeNode is concerned by $clusterLink
		if("$cluster" eq "$clusterLink"){
			my @derivs;
		       	if(!defined($derivs) || ($derivs eq '')){
		       		@derivs = ();
			}else{
		 		@derivs	= split ' ',$derivs;
			}
			${$lexNodesHashRef}{$id}= \@derivs;	
		}
	}

	undef @nodeNodes;
}

sub getMorphoInfos{
	my ($self,$rootNode,$opId)= @_;
		
	#my  = $rootNode->findnodes("$nodeStr");


}

## Examines the edges to establish the list of relations connected to the nodes representing 
## $lexNodesHashRef the suspect. The result is put in $fromHashRef, $toHashRef.
sub getLexRelationFromAndTo{
	my ($self,$rootNode,$lexNodesHashRef,$nodesHashRef,$fromHashRef,$toHashRef) = @_;
	my @edgeNodes = $rootNode->findnodes("$edgeStr");

	my ($srcId,$targetId,$label);
	foreach my $edgeNode (@edgeNodes){
		$srcId = $edgeNode->getAttribute($edgeSrcStr);
		$targetId = $edgeNode->getAttribute($edgeTargetStr);
		$label = $edgeNode->getAttribute($edgeLabelStr);
		my @derivNodes= $edgeNode->findnodes("$derivStr");

 		if(exists ${$lexNodesHashRef}{$srcId}){
 			 			
 			my ($derivId,$sourceOp,@span);
 			foreach my $derivNode (@derivNodes){
 				$derivId = $derivNode->getAttribute($derivIdStr);
				#$sourceOP = $derivNode->getAttribute($derivSopStr);
				

				@span = split / /,($derivNode->getAttribute($derivSpanStr));
 				push(@{$fromHashRef->{$srcId}->{$derivId}},{'label'=> $label, 'type' => 'from', 'catDestOrg' => $nodesHashRef->{$targetId},
									     'spanBot' => (shift @span), 'spanTop' => (pop @span)});
 			}
 		}
 		if(exists ${$lexNodesHashRef}{$targetId}){
			my @derivNodes= $edgeNode->findnodes("$derivStr");
 			
 			my @span;
 			foreach my $derivNode (@derivNodes){
				@span = split / /, $derivNode->getAttribute($derivSpanStr);
 				push(@{${$toHashRef}{$targetId}}, {'label'=> $label, 'type' => 'to', 'catDestOrg' => $nodesHashRef->{$srcId},
								   'spanBot' => (shift @span), 'spanTop' => (pop @span)});
			}

			undef @derivNodes;
 		}
	}

	undef @edgeNodes;
}

## Internal function to extract all the group of relations (hypothesis) made about the suspect $lex 
## by the syntax analyser in the $stringXml output.
sub extractGroupRelations{
	my ($self,$lex, $stringXml) = @_; 
		
	my $doc;
	eval{
 		$doc = XML::LibXML->new()->parse_string($stringXml);
	};
	if($@){
		$self->{'errorFunc'}($modName,"Could not create Xml representation...",$@," String $stringXml");
	}

	my $rootElem = $doc->documentElement();

	my $idCluster = $self->getClusterLink($lex, $rootElem);
	if(!$idCluster){
		$self->{'warnFunc'}($modName,"No matching Lex found in Xml Doocument");
		return undef;
	}
	## listing the nodes 
	my(%lexNodesHashRef,%nodesHashRef);
	$self->getNodeNodes($rootElem,$idCluster,\%lexNodesHashRef,\%nodesHashRef);
	## getting interesting relations  
	my(%fromHashRef,%toHashRef);
	$self->getLexRelationFromAndTo($rootElem,\%lexNodesHashRef,\%nodesHashRef,\%fromHashRef,\%toHashRef);
	
	#my (%relationsFrom, $nodeId);
	#foreach my $key (keys %fromHashRef){
	#	$nodeId = $key;
		#$nodeId =~ s/(.*)-.*/$1/;
		#$relationsFrom{$key}->{"cat"} = $nodesHashRef{$nodeId};

		#	foreach my $relationFrom (@{$fromHashRef{$key}}){
		#	push(@{$relationsFrom{$key}->{"from"}},{"label" => $relationFrom->{"label"}, "catTo" => $nodesHashRef{$relationFrom->{"to"}}});
		#}
	#}
 	
	my (%groupsRelations,@relationsFrom);
 	while(my($keyNode,$toRelationsArrayRef) = each %toHashRef){
		$groupsRelations{$keyNode}->{'cat'} = $nodesHashRef{$keyNode};
		foreach my $toRelation (@$toRelationsArrayRef){
				
			my $relationsFromByDerivs = $fromHashRef{$keyNode};
			my $atLeastOneGroup = 0;
 			foreach my $relationsFromByDeriv (values %$relationsFromByDerivs){
				my $ToAndFromCompt = 1;
				foreach my $relationFrom (@$relationsFromByDeriv){
					if($toRelation->{'spanBot'} > $relationFrom->{'spanBot'} &&
					   $toRelation->{'spanTop'} < $relationFrom->{'spanTop'}){
					  	$ToAndFromCompt = 0;
					}
				}
				if($ToAndFromCompt){
					$atLeastOneGroup = 1;
					my @relationsTemp;
					push(@relationsTemp,$toRelation);
					push(@relationsTemp, @$relationsFromByDeriv);
					push(@{$groupsRelations{$keyNode}->{'groupsRelations'}}, \@relationsTemp);
				}
			}
			if($atLeastOneGroup == 0){
				push(@{$groupsRelations{$keyNode}->{'groupsRelations'}},[$toRelation]);
			}
 		}
 	}

	## to free memory manually
	undef $doc;
	while(my($keyNode,$toRelationsArrayRef) = each %toHashRef){ 
		undef @$toRelationsArrayRef;
	}
	undef %toHashRef;

	while(my($cle1,$valeur1) = each %fromHashRef){
		while(my($cle2,$valeur2) = each %$valeur1){
			undef @$valeur2;
		}
		undef %$valeur1;	
	}
	undef %fromHashRef;

	undef %nodesHashRef;

	while(my($cle1,$valeur1) = each %lexNodesHashRef){ undef @$valeur1;}
       	undef %lexNodesHashRef;
	
	return \%groupsRelations;
}


## Internal function that deliver each time a new premium number used to index hypothesis.
sub getNextPremNumber{
	my ($self) = @_;
	
	# the list of already computed numbers
	my @premNumbers = @{$self->{'nbprem'}};
	my $numberAlReadyComputed = @premNumbers;
	# maybe the asked number as already been computed by a previous cycle
	if($self->{'nbpremCount'} <= $numberAlReadyComputed){
		$self->{'nbpremCount'}++;
		return $premNumbers[$self->{'nbpremCount'} - 2];
	}
	# if no let compute a new one
	my $lastChallenger = $premNumbers[$numberAlReadyComputed - 1];
	my $newChallenger =  $lastChallenger + 2;
	my $isPrem = 0;
	for(my $i = 0; $premNumbers[$i] <= sqrt($newChallenger); $i++){
		if(($newChallenger % $premNumbers[$i]) == 0){
			$i = 0;
			$newChallenger += 2;
		}	
	}
	# we record it for next time
	push(@{$self->{'nbprem'}},$newChallenger);

	return $newChallenger;
}

## Function that sort and index the hypothesis contained in the different sets of hypothesis in 
## @$analysisHypothesesArrayRef. The main idea is to assign a premium number to each relation and 
## to index hypothesis with a number obtained by multiplicating the numbers assigned to the relations 
## contained by this hypothesis.
sub getHypothesis{
	my ($self,$analysisHypothesisArrayRef) = @_;
	
	my %relationIndices;	
	my %hypothesis;
	foreach my $analysisHypothesis (@$analysisHypothesisArrayRef){	
		my $cat;
		while(my ($key,$value) = each %{$analysisHypothesis->{'relations'}}){
			$cat = $value->{'cat'};
			my $groupIndice;
			foreach my $groupRelation (@{$value->{'groupsRelations'}}){
				$groupIndice = 1;
				my $nameRelation;
				foreach my $relation (@{$groupRelation}){
					if($relation->{'type'} eq 'from'){
						$nameRelation = 's('.$cat.')=>'.$relation->{'catDestOrg'}.'::'.$relation->{'label'};
					}else{
						$nameRelation= $relation->{'catDestOrg'}.'=>s('.$cat.')::'.$relation->{'label'};
					}
					
					my $relationIndice;
					if(!exists($relationIndices{$nameRelation})){
					# this relation has not been indexed before let's give her a number and record her informations
						$relationIndice = $self->getNextPremNumber();
				 		$relationIndices{$nameRelation} = $relationIndice;
					}
					else{
						$relationIndice = $relationIndices{$nameRelation};
					}
					
					$relation->{'number'} = $relationIndice;
					$groupIndice*= $relationIndice;
				
				}
			
				if(!exists($hypothesis{$groupIndice})){
					# This group of relations (hypothesis) has not been indexed before.
					# let's record his informations.
					$hypothesis{$groupIndice}->{'relations'} = $groupRelation;
					$hypothesis{$groupIndice}->{'cat'} = $cat;

				}

				#to remember in which analysis this theory has been found
				push(@{$hypothesis{$groupIndice}->{'analysisOutputs'}},$analysisHypothesis->{'analysisOutput'});
			}
		}
		
	}
	
	## calcultaing dependances beetween hypothesis
	my @listHypo = (keys %hypothesis);
	my ($hypoGraphDepHashRef) = $self->getDeps(\@listHypo);
	while(my ($cle,$valeur) = each %hypothesis){
		$hypothesis{$cle}->{'graphDep'} = [];
	}
	while(my ($cle,$valeur) = each %$hypoGraphDepHashRef){ 
		my @included =  keys %{$valeur}; 	
		push(@{$hypothesis{$cle}->{'graphDep'}},@included);
	}
	## to free memory memory manually
	undef %relationIndices;

	return %hypothesis;
	
}



## Function that delivers two dependencies graphs trought two hash references of hashes of hash. 
## One is for the points dependencies while the other one is for the graph dependencies. 
sub getDeps{
	my ($self,$hypos)= @_;
	
	my ($graphDep,$pointDep);
	foreach my $hypo1 (@{$hypos}){
		foreach my $hypo2 (@{$hypos}){
			if($hypo1 != $hypo2){
				if(($hypo2 % $hypo1) == 0){
					$pointDep->{$hypo2}->{$hypo1} = 1;
				}	
				elsif(($hypo1 % $hypo2) == 0){
					$pointDep->{$hypo1}->{$hypo2} = 1;
				}
			}		
		}
	}

	while(my ($cle, $valeur) = each  %{$pointDep}){
		my %hashCpy = %{$pointDep->{$cle}};
		$graphDep->{$cle} = \%hashCpy;
	}

	while(my ($key, $value) = each %{$pointDep}){
		my @elements = keys %{$value};
		foreach my $includer1 (@elements){
			foreach my $includer2 (@elements){
				if(exists($pointDep->{$includer1}->{$includer2})){
					delete($graphDep->{$key}->{$includer2});
				}
			}
		}
	}

	return $graphDep;
}

=pod 

=over 1

=item * 
B<extractHypothesis($lex,$suspectKey,$analysisOutputsArrayRef)>

This function extracts all the hypothesis contained in the syntax analyser outputs designed by  
@$analysisOutputsArrayRef.

=back

=cut	

sub extractHypothesis{
	my ($self,$suspectKey,$analysisOutputsArrayRef) = @_;

	#put back the counter allowing us to generate dynamic number to index hypothesis of a cycle
	$self->{'nbpremCount'} = 1;

	#first getting the different 'brut' hypothesis in the different analysis outputs
	my @brutHypothesis;
	foreach my $analysisOutput (@$analysisOutputsArrayRef){
		my $relations = $self->extractGroupRelations($suspectKey,$analysisOutput->{'xml'});
		if(defined($relations)){
			push(@brutHypothesis,{  'analysisOutput'=> $analysisOutput,
						'relations' => $relations
					     });
		}
		else{
			$self->warnings("No relations found with ".$suspectKey."in analysis output...",
					"Sentence ".$analysisOutput->{'modifiedDag'}->{'sentence'}->{'corpusId'}."-".
					$analysisOutput->{'modifiedDag'}->{'sentence'}->{'sentId'}."should be examined");

		}
	}

	#Merging the 'brut' hypothesis to get a final table of the hypothesis producted
	return $self->getHypothesis(\@brutHypothesis);
}



return 1;
