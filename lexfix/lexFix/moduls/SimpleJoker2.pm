package SimpleJoker2;

use MODULE;
use strict;
use IPC::Run qw( run ) ;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.7;

=pod

=head2 B<SimpleJoker2.pm>

Module used to generate new under-specified or differently categorised lexical forms(jokers) 
Jokers are calculated according to the detected categories calculated by the 'GroupSuspect' 
modul plus the categories already defined for the suspects in the Lefff.
Jokers radicals are generated according to the format described by 'jokerRad' and 'jokerRadAj'.

=cut

# The variables required by the module to work
sub variables {
	return {'jokerRad'=>'SCALAR',
		'jokerRadAj'=>'SCALAR',
		'cmdLexed' => 'SCALAR'
		};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "SimpleJoker2";}

# The module name to send to errors/warnings Functions
my $modName = "SimpleJoker2";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;

	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	bless $self, $class;
	
	return $self;
}

################################Functions###############################################

=pod 

=over 1

=item * 
B<getJokers($suspectsHR)>

Computing all the jokers for the hash of suspects @$suspectHR. Answers
a hash containing array reference of jokers indexed the way @$suspectsHR is.

=back

=cut

sub getJokers{
	my ($self,$suspectsHR) = @_; 
	## getting all lexical informations about all the suspects
	my %lexInfosHash = $self->getLexInfos($suspectsHR);

	my $jokerPref = $self->{'conf'}->{'jokerRad'}.$self->{'conf'}->{'jokerRadAj'};

	my %res;
	while(my ($suspectId,$suspect) = each %$suspectsHR){
		my @cats; my %cats;

		my @detectedCats = @{$suspect->{'detectedCats'}};
		foreach my $detectedCat (@detectedCats){
			$detectedCat =~ s/(.*)-(.*)/$1/;
			if(!exists($cats{$detectedCat})){
				my $newRef = {'joker' => $jokerPref.$detectedCat, 
						'type' => {'detected'=> 1}
					     };
				unshift(@cats, $newRef);
				$cats{$detectedCat}= $newRef;
			}
			else{
				$cats{$detectedCat}->{'type'}->{'detected'} = 1;
			}
		}
	
		my @registeredCats = @{$lexInfosHash{$suspect->{'id'}}};
		foreach my $registeredCat (@registeredCats){
			if(!exists($cats{$registeredCat->{'cat'}})){
				my $newRef = {'joker' => $jokerPref.$registeredCat->{'cat'}, 
						'type' => {'lexicon' => 1}
					     }; 
				unshift(@cats, $newRef);
				$cats{$registeredCat->{'cat'}} = $newRef; 
			}
			else{
				$cats{$registeredCat->{'cat'}}->{'type'}->{'lexicon'} = 1;
			}
		}
		if(!@cats){
			unshift(@cats, {'joker' => $jokerPref.'v', 'type' => ['default']});
			unshift(@cats, {'joker' => $jokerPref.'nc', 'type' => ['default']});
			unshift(@cats, {'joker' => $jokerPref.'adj', 'type' => ['default']});
		}else{
			foreach my $cat (@cats){ $cat->{'type'} = [keys %{$cat->{'type'}}];}
		}

		push(@cats, {'joker' => 'none', 'type' => ['default']});
		$res{$suspectId} = \@cats;
	}

	return %res;
}


## Get informations trough lexed.
## The informations are answered through an hash containing an array of hash references for each suspect.
## Each hash as an 'cat' and 'datas' field matching each lexed answer.
sub getLexInfos{
	my ($self,$suspectsHashRef) = @_;

	#Preparing the command
	my @cmd = split(/ /,$self->{'conf'}->{'cmdLexed'});

	#Preparing input and output
	my %res; my @link; my $in = "";
	while(my ($cle,$valeur) = each %$suspectsHashRef){
		$res{$cle} = [];
		push(@link,$cle);
		$in .= $valeur->{'key'}."\n";
	}

	my ($out,$err);
	eval{
		run (\@cmd, \$in, \$out, \$err);
	};
	if($@){
		$self->errors("Errors occured during '@cmd'  execution","System returned $@"); 
 	}		

	my @out = ();# split /\n/,$out;

	foreach my $line (@out){
		my $cleConcerned = shift @link;
		while ($line =~ m{\s+?(.+?)\s+?(\[.*?\])}og){
			push(@{$res{$cleConcerned}},{"cat" => $1, "datas" => $2});	
		}
		
		# if nothing as been registered then lexed has answered <unknown> which doesn't match previous format
		if(!exists($res{$cleConcerned})){
			$res{$cleConcerned} = [] ;
		}
	} 	

	return %res;
}

return 1;
