package GroupSuspectsByStem;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.99;

=pod

=head2 B<GroupSuspectsByStem.pm>

Module used to group the suspects according to them stem and them detected categories.

=cut

# The variables required by the module to work
sub variables { return {};}

# The module name to send to errors/warnings Functions
my $modName = "GroupSuspectsByStem";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName);
	bless $self, $class;

	return $self;	
}

################################Functions###############################################

=pod 

=over 1

=item * 
B<getGroups($suspectsHashRef)>

Given an hash reference of suspects answers an array of group of suspects.
Each suspect is put in a group according to his lexical root and his suffixes

=back

=cut

sub getGroups{
	my ($self,$suspectsHashRef) = @_; 
	
	my %groupsByStem;
	my @groups;

	while(my($suspectId,$suspect) = each %$suspectsHashRef){
		my $stem = $suspect->{'stem'};

		foreach my $detectedCat (@{$suspect->{'detectedCats'}}){
			push(@{$groupsByStem{$stem}->{$detectedCat}},$suspectId);
		}
	}

	while(my($stem,$groupsByCat) = each %groupsByStem){
		while(my($cat,$group) = each %$groupsByCat){
			if(@{$group} != 1){
				push(@groups,$group);
			}	
		}
	}

	return @groups;
}



return 1;
