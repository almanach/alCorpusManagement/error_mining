package ModifyDag;

require Exporter;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.99;

=pod

=head2 B<ModifyDag.pm

Module used to modify a dag by replacing a lexical form by a joker. Each dag line is supposed to match
the format designed by 'dagLineBeforeComment', 'dagLineComment', 'dagLineAfterComment' and 'dagLineAfterLex'
variables.

=cut

# The variables required by the module to work
sub variables{	
	return {'dagLineBeforeComment'=>'SCALAR',
		'dagLineComment'=>'SCALAR',
		'dagLineAfterComment'=>'SCALAR',
		'dagLineAfterLex'=>'SCALAR'
		};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "ModifyDag";}

# The module name to send to errors/warnings Functions
my $modName = "ModifyDag";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	bless $self, $class;

	return $self;		
}
################################Functions###############################################

=pod 

=over 1

=item * 
B<ModifyDag($suspectLex,$suspectInLexico,$joker,$dag)>

Function called to modified a dag $dag by replacing the lexical form described by $suspectLex/$suspectInLexico
by the joker $joker.

=back

=cut

sub ModifyDag{
	my ($self,$suspectLex,$suspectInLexico,$joker,$dag) = @_;
	
	#print "On recherche $suspectLex $suspectInLexico pour $joker\n";

	my @suspectPart = split(/_/,$suspectLex);
	my @dagLines = split("\n",$dag);
	my $beforeFormat = $self->{'conf'}->{'dagLineBeforeComment'};
	my $commentFormat = $self->{'conf'}->{'dagLineComment'};
	my $afterCommentFormat = $self->{'conf'}->{'dagLineAfterComment'};
	my $afterFormat = $self->{'conf'}->{'dagLineAfterLex'};
	
	my @dagResult;
	foreach my $dagLine (@dagLines){
		my ($before,$comment,$afterComment,$after) = ($dagLine =~ /^($beforeFormat)($commentFormat)($afterCommentFormat)${suspectInLexico}($afterFormat)$/);
		
		if($before and $comment and $afterComment and $after){
			my @suspectPartsCpy = @suspectPart;
			my $match=1;
			
			my $suspectPart;
			while($comment =~ m/<F id=".*?">(.*?)<\/F>/og ){
				## checkin if all parts of the suspect lexical form are matched
				if(!($suspectPart = shift @suspectPartsCpy) or !($suspectPart eq $1)){
					$match = 0;
					last;
				}
			} 
			if($match){
				push(@dagResult,"${before}${comment}${afterComment}${joker}${after}");
			}
			else{
				push(@dagResult,$dagLine);
			}
		}		
		else{	
			push(@dagResult,$dagLine);
		}
	}

	return join("\n",@dagResult);
}


return 1;
