package SuspectInfosDb;

use MODULE;
use strict;
use DBI;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.99;

=pod

=head2 B<SuspectInfosDb.pm>

Module used to get all informations about suspects and associated sentences in a suspect database.
The suspect dase is given by 'dbURI', 'dbUserName' and 'dbPassword' variables.
The variable 'MaxSizeSentence' gives the maximum number of word admit for a sentence associated to a suspect (to avoid pointless analysis)
Finally the 'dagCorpusPath' variable gives the path where the dags are stored in '.udag' file

=cut

# The variables required by the module to work
sub variables {
	return {'dbURI' => 'SCALAR', 
		'dbUserName' => 'SCALAR', 
		'dbPassword' => 'SCALAR',
	        'maxSizeSentence' => 'SCALAR',
		'dagCorpusPath' => 'SCALAR' # The path to the dags, only useful if the database has not been built yet,
	};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "SuspectInfosDb";}

# The module name to send to errors/warnings Functions
our $modName = "SuspectInfosDb";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	bless $self, $class;
	
	##Initialize database connection to work with
	$self->{'dbConnection'} = DBI->connect($self->{'conf'}->{'dbURI'},$self->{'conf'}->{'dbUserName'},
				     $self->{'conf'}->{'dbPassword'},{RaiseError => 1});

	##Check database connection
	if(!$self->{'dbConnection'}){
 		$self->initErrors('Can\'t Connect'.$self->{'conf'}->{'dbURI'},'Error=>'.$DBI::errstr);
	}

	opendir(DIRHANDLE,$self->{'conf'}->{'dagCorpusPath'}) || $self->errors("Can't open dag corpus directory",$!);
	
	foreach my $corpus (readdir(DIRHANDLE)){
		my ($corpusName,$corpusExtension) = ($corpus =~ /^(.+)(\..+)$/);
		
		if(($corpus eq ".") || ($corpus eq "..")){next;	}

		#first we open it
		if(!open(CORPUS, "< ".$self->{'conf'}->{'dagCorpusPath'}."/$corpus")){
			$self->initErrors("Can't open corpus file ".$self->{'conf'}->{'dagCorpusPath'}."/$corpus !",$!);
		}
			
		close(CORPUS);
	}
	closedir DIRHANDLE;

	return $self;	
}

sub DESTROY{
	my ($self) = @_;

	if(exists($self->{'dbConnection'}) and defined($self->{'dbConnection'})){
		$self->{'dbConnection'}->disconnect();
	}
}

################################Functions###############################################

	
sub sqlCmd {
	my ($self,$cmd) = @_;

	my $request;
	eval{
		$request = $self->{'dbConnection'}->prepare($cmd);
	 	if(!$request->execute()){	
	 		$self->errors("Can\'t execute $cmd","Error(s) => ".$DBI::errstr);
	 	}
	};
	if($@){
		$self->errors("Can't execute $cmd",'Error(s)=>'.$DBI::errstr);
 	}

	return 	$request;
}


=pod 

=over 1

=item * 
B<getSuspects()>

Deliver an hash of suspects

=back

=cut

sub getSuspects{
 	my ($self) = @_; 

	$self->indexDags();

	my $sqlCommand = "SELECT m.id id, m.key key, m.weight weight, m.occ occ ".
			 "FROM mine m";# WHERE m.weight > 0.6";
	my $request = $self->sqlCmd($sqlCommand);
	
	$self->debug(3,"Retrieving suspects");

	my %suspects;
	while(my $row = $request->fetchrow_hashref()){
		my ($suspectLex,$suspectInLexico) = split(/\//,$row->{'key'}); 
		
		if(!$suspectInLexico) { $suspectInLexico = $suspectLex;}
		$suspects{$row->{'id'}}={'id' => $row->{'id'},
					'key' => $suspectLex, 
					'lex' => $suspectInLexico,
					'weight' => $row->{'weight'},
					'orgNbSucc' =>  0,
					'orgNbTimO' => 0,
					'orgNbFail' => 0,
					'bestRes' => 0,
					'bestJoker' => 'none',
					'sentences' => []
				     };
	}

	$sqlCommand = "SELECT s.id id, s.corpus corpusId ,s.sid sentId, s.sentence sent FROM sentence s";
	$request = $self->sqlCmd($sqlCommand);
	
	my @sentencesToRetrieve;
	my $maxSizeSentence = $self->{'conf'}->{'maxSizeSentence'};
	while(my $row = $request->fetchrow_hashref()){
		if(!exists($suspects{$row->{'id'}})){
			#$self->warnings("Incorrect input data, a sentence related to an unknow suspect found...",
			#		"Unknown suspect's id: ".$row->{'id'});
			next;
		}
		my @sizeSentence = split(' ',$row->{'sent'});
		my $sizeSentence = @sizeSentence;
		if($sizeSentence > $maxSizeSentence){ ## to avoid pointless analysis
			next;
		}
		
		my $sentence = {'corpusId' => $row->{'corpusId'},
				'sentId' => $row->{'sentId'},
				'sent' => $row->{'sent'}
		};
		
		push(@{$suspects{$row->{'id'}}->{'sentences'}},$sentence);
		push(@sentencesToRetrieve,$sentence);
	}
	
	$self->debug(3,"Retrieving dags");

	my $dags = $self->getDags(\@sentencesToRetrieve);	
	while(my($suspectId,$suspect) = each %suspects){
		foreach my $sentence (@{$suspect->{'sentences'}}){
			$sentence->{'dag'} = $dags->{$sentence->{'corpusId'}."-".$sentence->{'sentId'}};
		}
	}

	return \%suspects;
}


sub indexDags{
	my ($self) = @_;

	$self->debug(4,"Indexing dags...");

	opendir(DIRHANDLE,$self->{'conf'}->{'dagCorpusPath'})|| $self->errors("Can't open dag corpus directory",$!);
	
	foreach my $corpus (readdir(DIRHANDLE)){
		my ($corpusName,$corpusExtension) = ($corpus =~ /^(.+)(\..+)$/);

		if(($corpus eq ".") || ($corpus eq "..")){next;	}

		$self->debug(3,"Indexing dag corpus=>$corpus");

		#first we open it
		if(!open(CORPUS, "< ".$self->{'conf'}->{'dagCorpusPath'}."/$corpus")){
			$self->errors("Can't open corpus file ".$self->{'conf'}->{'dagCorpusPath'}."/$corpus !",$!);
		}
			
		my $startOffset = tell CORPUS;
		my $endOffset;
		my $dagIndices;
		while(my $line = <CORPUS> ){
			

			if($line =~ /^\d+\s+\{(.+)\}\s+.+\s+\d+$/){
				# we register here all the indices given to a dag
				my $infos = $1;
				while($infos =~ /<F id="E(\d+)F\d+">.*<\/F>/g){
					$dagIndices->{$1} = 1;
				}
			}
			elsif($line =~ /^## DAG END$/){
				## We arrive at the end of a dag let's register it
				$endOffset = tell CORPUS;
				my @dagIndices =  keys %$dagIndices;
				foreach my $dagIndice (@dagIndices){
					$self->{'corpus'}->{$corpusName}->{$dagIndice} = {'sentId' => $dagIndice, 
											  'start' => $startOffset, 'end' => $endOffset	};
				}
				#$self->addDag(\@dagIndices,$corpusName,$startOffset,$endOffset - 1);
				$dagIndices = {};
				$startOffset = $endOffset;
			}
			elsif($line =~ /^## DAG BEGIN$/){
				next;
			}
			else{
				$self->warnings("$corpus is not a valid dag corpus","$line doesn't match format");
				last;
			}
		}
		close(CORPUS);
	}
	closedir DIRHANDLE;

	$self->debug(4,"Indexing dags done.");
}


sub getDags{
	my ($self,$sentencesArrayRef) = @_;

	## grouping required dags by them corpus 
	my %sentencesListByCorpus;
	foreach my $sentence (@$sentencesArrayRef){
		my ($corpusId,$sentId) = ($sentence->{'corpusId'},$sentence->{'sentId'});
		if(exists($self->{'corpus'}->{$corpusId}->{$sentId})){
			$sentencesListByCorpus{$corpusId}->{$sentId} = $self->{'corpus'}->{$corpusId}->{$sentId};
		}
		else{
			## check if the corpus is referenced 
			if(!exists($self->{'corpus'}->{$corpusId})){
				$self->errors("Dag $sentId in corpus $corpusId has not been found when indexing");
			}
			else{## it is not the corpus then it is the dag
				$self->errors("Unreferenced Corpus $corpusId in configuration");
			}
		}
	}

	my $res;
	## Getting dags for each group
	while(my ($corpus,$dagsHR) = each %sentencesListByCorpus){
		my $corpusFile = $self->{'conf'}->{'dagCorpusPath'}."/".$corpus.".udag";
		if(!open(CORPUS, "< $corpusFile")){ $self->errors("Can't open corpus file '$corpusFile'",$!);}

		my @dagsArray = values %$dagsHR;
		
		## ordering them
		@dagsArray = sort {if($a->{'start'} < $b->{'start'}) {return  -1;}else{ return 1;}} @dagsArray; 
		my $endOffset = 0;
		my $temp;
		foreach my $dag (@dagsArray){
			## read the ''space''
			if(($dag->{'start'} !=  $endOffset) && (!sysread(CORPUS,$temp,$dag->{'start'} - $endOffset))){
				$self->errors("Could not read until beginning of dag '".$dag->{'sentId'}."' from corpus '$corpus'",$!);
			}

			## read the dag
			if(!sysread(CORPUS,$res->{$corpus."-".$dag->{'sentId'}},$dag->{'end'} - $dag->{'start'})){
				$self->errors("Could not read dag '".$dag->{'sentId'}."' from '$corpusFile'",$!);
			}

			$endOffset = $dag->{'end'};	
		}
		close(CORPUS);
	}
		
	return $res;
}



















return 1;
