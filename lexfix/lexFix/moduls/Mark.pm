package Mark;

use MODULE;
use strict;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.9;

=pod

=head2 B<Mark.pm>

Module used to evaluate (give marks to) a group of hypothesis. To give the marks the module use a numerical
suite which behaviour depends on 'coeff' variable. 

=cut

# The variables required by the module to work
sub variables{
	return {'coeff' => 'SCALAR'};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "Mark";}

# The module name to send to errors/warnings Functions
my $modName = "Mark";

##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	$self->{'pointsNumberElements'} = [1];
	bless $self, $class;	

	return $self;
}

################################Functions###############################################

## Internal function that gives a mark according to $numberOfElems. 
## Right now it's just a simple numerical suite.
sub getGroupPoints{
	my ($self,$numberOfElems) = @_;
	
	my $numberAlReadyComputed = @{$self->{'pointsNumberElements'}};
	if($numberOfElems > $numberAlReadyComputed){
		for(my $i = $numberAlReadyComputed + 1; $i <= $numberOfElems; $i++){ 
			$self->{'pointsNumberElements'}[$i - 1] = ($self->{'pointsNumberElements'}[($i - 2)] * ($i - 1) * $self->{'conf'}->{'coeff'})/ $i;
		}
	}

	return $self->{'pointsNumberElements'}[$numberOfElems - 1];
}

=pod 

=over 1

=item * 
B<getMarks($hypothesisHashRef)>

According to the hypothesis contained in %$hypothesisHashRef, gives a two marks for each hypothesis 
and answers them in a hash of hashes indexed teh same way %$hypothesisHashRef is. The first mark 'points' is the 
mark obtained by the hypothesis herself and the second mark 'secPoints' is the mark obtained by "legacy".

=back

=cut

sub getMarks{
	my ($self,$hypothesisHashRef) = @_;

	my $analysis;
	my %res; 
	while(my ($cle,$valeur) = each %$hypothesisHashRef){
		foreach my $analysisOutput (@{$valeur->{'analysisOutputs'}}){
			push(@{$analysis->{$analysisOutput}}, $cle);
		}
	}

	while(my ($cle,$valeur) = each %$analysis){
		my $nbHypoToGrant = @$valeur;
		my $nbPointsToGrant = $self->getGroupPoints($nbHypoToGrant);
		foreach my $hypoToGrant (@$valeur){
			$res{$hypoToGrant}->{'points'} += $nbPointsToGrant;
		}
	}

	
	## to free memory manually
	while(my ($cle,$valeur) = each %$analysis){
		my $hypo;	
	       	while(my $hypo = shift @$valeur){}
		delete($analysis->{$cle});
	}
		
	return %res;
	
}

return 1;
