package ClassicInput;

use MODULE;
use strict;
use SuspectInfosDb;
use FilterSuspect;
use SearchCatsByStem;
use GroupSuspectsByStem;
use SimpleJoker2;
use OrderSuspects;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.5;

=pod

=head2 B<ClassicInput.pm>

Module used to prepare all data to process. That's why it doesn't have much functions on his own,
it just coordinate the other moduls.

=cut

# The module name to send to errors/warnings Functions
my $modName = "ClassicInput";

# The moduls managed
my $moduls = {
	'SuspectInfos' => 'SuspectInfosDb',
	'FilterSuspect' => 'FilterSuspect',
	'SearchCats' => 'SearchCatsByStem',
	'GroupSuspects' => 'GroupSuspectsByStem',
	'Joker' => 'SimpleJoker2',
	'OrderSuspects' => 'OrderSuspects'
};	


##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName);
	bless $self, $class;	
	
	while(my($key,$modul) = each %$moduls){
		$self->{$key} = $modul->new($conf,$funcMsg);
	}
	#$self->{'FilterSuspect'} = FilterSuspect::new($conf,$funcMsg);
	#$self->{'SearchCats'} = SearchCatsByStem::new($conf,$funcMsg);
	#$self->{'GroupSuspects'} = GroupSuspectsByStem::new($conf,$funcMsg);
	#$self->{'Joker'} = SimpleJoker2::new($conf,$funcMsg);
	#$self->{'OrderSuspects'} = OrderSuspects::new($conf,$funcMsg);

	return $self;	
}

sub getVariables{
	my $variables = {};
	while(my($key,$modul) = each %$moduls){
		my $modulVariables = $modul->getVariables();
		while(my($name,$value) = each %$modulVariables){
			$variables->{$name} = $value;
		}
	}

	return $variables;
}

################################Functions###############################################

sub prepareData{
	my ($self) = @_;

	my (%suspectsHash,@suspectsArr,@invalidSuspects, @validSuspects);
	{
		$self->msgs("Computing the list of suspects...");
		#	if($TraceModule->checkSuspectsDone()){
		#	$self->msgs("List of suspects already computed => resuming");
		#	%suspectsHash = $TraceModule->getSuspects();
		#}
		#else{
		
		%suspectsHash= %{$self->{'SuspectInfos'}->getSuspects()};
		
		while(my ($cle,$valeur) = each %suspectsHash){
			my ($answer,$explanation) = $self->{'FilterSuspect'}->checkValidity($valeur);
			if($answer){
				$self->{'SearchCats'}->getStemAndCats($valeur);
				push(@validSuspects,$valeur);
			}
			else{
				push(@invalidSuspects,{'suspect'=> $valeur, 'explanation' => $explanation});
				$self->debug(5,"Suspect ".$valeur->{'lex'}." excluded => $explanation");
				delete $suspectsHash{$cle};			
			}
		}	
		
		@suspectsArr = values %suspectsHash;
		$self->msgs("Number of suspects listed => ".(@invalidSuspects+@validSuspects),
			    @validSuspects." valid / ".@invalidSuspects." non-valid");
	}
	
	my @suspectsGroups;
	{
		$self->msgs("Computing groups of suspects...");
		#if(!($hashModules{'TraceResume'}->checkGroupsDone())){
			@suspectsGroups = $self->{'GroupSuspects'}->getGroups(\%suspectsHash);
		#}
		#else{
		#	$self->msgs($progName,"Groups already computed => skipping");
		#}
		$self->msgs("=> Groups done");
	}



	my %jokersHash;
	{
		$self->msgs("Computing jokers...");
		#if($hashModules{'TraceResume'}->checkJokersPrepared()){
		#	printMsgs($progName,"Jokers already computed => resuming");
		#	%jokersHash = $hashModules{'TraceResume'}->getJokers();
		#}
		#else{
			%jokersHash = $self->{'Joker'}->getJokers(\%suspectsHash);
		#}
		$self->msgs("=> Jokers are ready");
	}

	{
		$self->msgs("Ordering the suspects...");
		@validSuspects = $self->{'OrderSuspects'}->OrderSuspects(\@validSuspects);
		$self->msgs("=> Order done...");
	}

	return { 'suspects' => \@validSuspects,
		 'invalidSuspects' => \@invalidSuspects,
		 'suspectsGroups' => \@suspectsGroups,
		 'jokers' => \%jokersHash
	};

}


sub stillMoreData{
	return undef;
}


