package TraceResumeSql;

use MODULE;
use strict;
use DBI;

our @ISA = qw(MODULE);
our @EXPORT = qw();
our @EXPORT_OK = qw();
our $VERSION = 0.7;

=pod

=head2 B<TraceResumeSql.pm>

This module is used to trace the results and resume an execution if needed. Everything is stored in an sql 
database given by 'dbUserName', 'dbPassword' and 'dbURI' variables.


=cut


# The variables required by the module to work
sub variables {
	return {'dbUserName' => 'SCALAR',
		'dbPassword' => 'SCALAR',
		'dbURI' => 'SCALAR'
		};
}

# The module prefix to distinguish the values in the initialization conf
sub modPrefix { return "TraceResumeSql";}

# The module name to send to errors/warnings Functions
our $modName="TraceResumeSql.pm";



# the tables names
my @tablesNames = ('conf','suspects','invalidSuspects','suspectsStem','suspectsGroups','jokers','sentences','assoSuspectSent',
		   'analysis','relations','hypothesis','hypothesisRelations','hypothesisAnalysis','hypothesisGraphDep');

# the sql commands used to create the tables
my $tablesCreateCmds = {'conf'=>"CREATE TABLE conf (key,value,type, PRIMARY KEY(key) ON CONFLICT REPLACE)",
			'suspects'=>"CREATE TABLE suspects (id INTEGER PRIMARY KEY AUTOINCREMENT, primaryId INTEGER UNIQUE,key,lex,weight,nbSent INTEGER, 
						stem)",
			'invalidSuspects'=>"CREATE TABLE invalidSuspects (id INTEGER PRIMARY KEY, explanation)",
			'suspectsStem'=>"CREATE TABLE suspectsStem (suspectId,stem,detectedCat,PRIMARY KEY(suspectId,stem,detectedCat))",
			'suspectsGroups'=>"CREATE TABLE suspectsGroups (groupId, suspectId, PRIMARY KEY(groupId,suspectId))",
			'jokers'=>"CREATE TABLE jokers (suspectId INTEGER,joker,status,type,nbSucc INTEGER, nbTimO INTEGER, nbFail INTEGER,".
				  " PRIMARY KEY(suspectId,joker))",
			'sentences'=>"CREATE TABLE sentences (sentId,corpusId,sent,dag, PRIMARY KEY(sentId,corpusId) ON CONFLICT IGNORE)",
			'assoSuspectSent'=>"CREATE TABLE assoSuspectSent (suspectId INTEGER,sentId INTEGER, corpusId, PRIMARY KEY(suspectId,sentId,corpusId) ON CONFLICT IGNORE)",
			'analysis'=>"CREATE TABLE analysis(id INTEGER PRIMARY KEY AUTOINCREMENT, joker,suspectId INTEGER, sentId INTEGER, corpusId,".
		       					   "status, UNIQUE (joker,suspectId,sentId,corpusId) ON CONFLICT IGNORE)",
			'relations'=>"CREATE TABLE relations(id INTEGER,joker,suspectId INTEGER,type, catDestOrig, label, PRIMARY KEY(id,joker,suspectId) ON CONFLICT IGNORE)",
			'hypothesis'=>"CREATE TABLE hypothesis(id INTEGER, joker, suspectId INTEGER, cat, points, PRIMARY KEY(id,joker,suspectId))",
			'hypothesisAnalysis'=>"CREATE TABLE hypothesisAnalysis(id INTEGER PRIMARY KEY AUTOINCREMENT,analysisId INTEGER, hypoId INTEGER, joker, suspectId INTEGER)",
			'hypothesisRelations'=>"CREATE TABLE hypothesisRelations(id INTEGER PRIMARY KEY AUTOINCREMENT, idHypo INTEGER,joker,suspectId INTEGER, idRel INTEGER)",
			'hypothesisGraphDep'=>"CREATE TABLE hypothesisGraphDep(idSrc INTEGER,joker,suspectId INTEGER, idDest INTEGER, PRIMARY KEY(idSrc,joker,suspectId,IdDest))"
};

# some useful indexs
my $indexCreateCmds = {
			'suspectsStem'=>"CREATE INDEX indexSt ON suspectsStem(stem,detectedCat)",
			'hypothesisRelations'=>"CREATE INDEX indexHR ON hypothesisRelations(idHypo,joker,suspectId,idRel)",
			'hypothesisAnalysis'=>"CREATE INDEX indexHA ON hypothesisAnalysis(analysisId,hypoId,joker,suspectId)"
};


my $dbConnect;


##########################General Functions#####################################"
sub new{
	my ($class,$conf,$funcMsg) = @_;
	
	my $self = MODULE->new($conf,$funcMsg,$modName,modPrefix(),variables());
	$self->{'EXECUTIONERRORS'} = [];
	$self->{'cycleEngaged'} = 0;
	$self->{'toPrint'} = "";
	bless $self, $class;	
	

	##Initialize connection to work with
	$self->{'dbConnection'} = DBI->connect($self->{'conf'}->{'dbURI'},$self->{'conf'}->{'dbUserName'},
				     		$self->{'conf'}->{'dbPassword'},{RaiseError => 1});
	if(!$self->{'dbConnection'}){
		$self->initErrors('Can\'t Connect to '.$self->{'conf'}->{'dbURI'},'Error=>'.$DBI::errstr);
	}
	
	$self->checkAndCreateNecessitedTables();

	return $self;
}



#############################Functions##########################################

# an internal function to manage sql requests.
sub prepareAndExecute{
	my ($self,$command) = @_;
	my $request;	
	
	#print "Command $command\n";
	eval{ 
		$request = $self->{'dbConnection'}->prepare($command); 
		if(!defined($request)){
			$self->{'EXECUTIONERRORS'} = ["Could not prepare request $command",'Error(s)=>'.$DBI::errstr];
			return undef;
		}
		$request->execute();
	};
	if($@){
		$self->{'EXECUTIONERRORS'} = ["Can't execute $command",'Error(s)=>'.$DBI::errstr];
		return undef;
 	}
	return $request;
}

# an internal function to manage sql parameters.
sub quote{
	my ($self,@parameters) = @_;

	my @res;
	while(@parameters){
		my $parameter = shift(@parameters);
		if(!defined($parameter)){
			$parameter = "NULL";
		}
		else{
			$parameter =~ s/'/''/g;
			$parameter = "'".$parameter."'";
		}
		push(@res,$parameter);
	}

	return @res;
}

# an internal function to check a sql condition given by $sqlRequest.
sub checkCondition{
	my ($self,$sqlRequest) = @_;

	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	my $row = $request->fetchrow_hashref();
}

=pod 

=over 1

=item * 
B<beginCycle()>

Begins a transaction cycle for storing informations. No previous transaction should be engaged. 

=cut

sub beginCycle{
	my ($self)= @_; 

	if(!($self->prepareAndExecute("BEGIN DEFERRED"))){ 
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}	
}

=pod 

=item * 
B<endCycle()>

Ends a transaction cycle for storing informations. A previous transaction should be engaged. 

=cut

sub endCycle{
	my ($self,$idJoker)= @_; @{$self->{'EXECUTIONERRORS'}} = ();

	if(!($self->prepareAndExecute("COMMIT"))){ 
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}	
}

# an internal function to prepare the database
sub checkTableExistence{
	my ($self,$nameTable)= @_; ($nameTable) = $self->quote($nameTable);
	my $sqlRequest = "SELECT * FROM sqlite_master WHERE type='table' and name=$nameTable";

	return $self->checkCondition($sqlRequest);
}

# an internal function to prepare the database
sub createTable{
	my ($self,$nameTable)= @_; 

 	if(!($self->prepareAndExecute($tablesCreateCmds->{$nameTable}))){ 
		$self->initErrors(@{$self->{'EXECUTIONERRORS'}});
	}

	if(exists($indexCreateCmds->{$nameTable})){
		if(!($self->prepareAndExecute($indexCreateCmds->{$nameTable}))){ 
			$self->initErrors(@{$self->{'EXECUTIONERRORS'}});
		}
	}
}

# an internal function to prepare the database
sub compareTableCreateCmd{
	my ($self,$nameTable)= @_; my ($nameTableCpy) = $self->quote($nameTable);
	my $sqlRequest = "SELECT sql cmd FROM sqlite_master WHERE type='table' and name=$nameTableCpy";

	my $request;
 	if(!($request = $self->prepareAndExecute($sqlRequest))){
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	my %row = %{$request->fetchrow_hashref()};
	if(%row){
		my $createCmd = $row{'cmd'};
		if(!($createCmd eq $tablesCreateCmds->{$nameTable})){
			$self->initErrors('Create table command clash for '.$nameTable,
				"Conf's create table command".$tablesCreateCmds->{$nameTable},
				"Existing table's create table command $createCmd");
		}
	}	
}

# an internal function to prepare the database
sub checkAndCreateNecessitedTables{
	my ($self) = @_;

	foreach my $tableName (@tablesNames){
		if($self->checkTableExistence($tableName)){
			$self->compareTableCreateCmd($tableName);
		}
		else{
			$self->createTable($tableName);
		}
	}
}


=pod 

=item * 
B<checkConfExistence()>

Check if the previous execution parameters has been stored.

=cut

sub checkConfExistence{
	my ($self)= @_; 
	my $sqlRequest = "select 1 from conf where exists (select * from conf)";

	return $self->checkCondition($sqlRequest);
}

=pod 

=item * 
B<getConf()>

Retrieve a previous execution parameters.

=cut

sub getConf{
	my ($self)= @_; 

	my %res;
	my $request;

 	if(!($request = $self->prepareAndExecute("SELECT key, ifnull(value,' ') value, type FROM conf where key<>'step'"))){ 
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}
 	
	my ($rowHashRef,$key,$value,$type);
	while($rowHashRef = $request->fetchrow_hashref()){
		$key = ${$rowHashRef}{'key'}; $value = ${$rowHashRef}{'value'}; $type = ${$rowHashRef}{'type'};
		if($type eq "SCALAR"){
			$res{$key} = {'type' => 'SCALAR', 'value' =>  $value};
		}elsif($type eq "HASH"){
			my @tempArray = split /\[\@\]/, $value;
			my %tempHash;
			my($cle,$valeur);
			while(@tempArray){
				$cle = shift @tempArray; $valeur = shift @tempArray;
				if(!defined($valeur)) {$valeur = "";}
				$tempHash{$cle} = $valeur;
			}
			$res{$key} = {'type' => 'HASH', 'value' => \%tempHash};
		}else{
			my @tempArray= split /\[\@\]/, $value;
			$res{$key} = {'type' => 'ARRAY', 'value' => \@tempArray};
		}
	}	

	return %res;
}

=pod 

=item * 
B<saveConf()>

Save the execution's execution parameters.

=cut

sub saveConf{
	my ($self,$conf)= @_; 
	$self->beginCycle();	

	while(my ($key, $value) = each (%$conf)){
		my $valueToSave;
		my $typeToSave;
		my $valueRef = \$value;
		$valueRef =~ s/\(.*\)//;

		if($valueRef =~ m/^SCALAR$/){
			$valueToSave = $value; $typeToSave= "SCALAR";
		}
		elsif($value =~ m/^HASH\(.*\)$/){
			$valueToSave = join "[\@]", %$value; $typeToSave= "HASH"
		}
		else{
			print "$valueRef\n";
			$valueToSave = join "[\@]", @$value; $typeToSave= "ARRAY"
		}
		if(!defined($value)){
			$valueToSave = "''";
		}

		($key,$valueToSave,$typeToSave) = $self->quote($key,$valueToSave,$typeToSave);		
		my $sqlRequest = "INSERT INTO conf(key,value,type) VALUES($key,$valueToSave,$typeToSave)";
 		if(!($self->prepareAndExecute($sqlRequest))){
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}
	}

	$self->endCycle();
	return 1;
}



#registerSuspect($suspectHashRef)
#Save the suspects calculated and given by the references contained in the array @$suspectHashRef.

sub registerSuspects{
	my($self,$suspectsAR,$invalidSuspectsAR) = @_; 

	foreach my $suspectHashRef (@$suspectsAR){
		my ($id,$key,$lex,$weight,$stem) = $self->quote($suspectHashRef->{'id'},$suspectHashRef->{'key'},$suspectHashRef->{'lex'},
								$suspectHashRef->{'weight'},$suspectHashRef->{'stem'});
		my @sentences = @{$suspectHashRef->{'sentences'}};
		my $nbSentences = @sentences;
		
		my $sqlRequest = "INSERT INTO suspects(primaryId,key,lex,weight,nbsent,stem) VALUES($id,$key,$lex,$weight,$nbSentences,$stem)";
	
		if(!$self->prepareAndExecute($sqlRequest)){
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}
		
		foreach my $detectedCat (@{$suspectHashRef->{'detectedCats'}}){
			my ($detectedCatQu) = $self->quote($detectedCat);
			my $sqlRequest = "INSERT INTO suspectsStem(suspectId,stem,detectedCat) VALUES($id,$stem,$detectedCatQu)";
			if(!$self->prepareAndExecute($sqlRequest)){
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
		}

		foreach my $sentence (@sentences){
			$self->registerSentence($sentence);
			my ($corpusId) = $self->quote($sentence->{'corpusId'});
			my $sqlRequest = "INSERT INTO assoSuspectSent(suspectId,sentId,corpusId) VALUES($id,".$sentence->{'sentId'}.",$corpusId)";
			if(!$self->prepareAndExecute($sqlRequest)){
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
		}

	}

	foreach my $invalidSuspect (@$invalidSuspectsAR){
		my $suspectHashRef = $invalidSuspect->{'suspect'};
		my $explanationStr = $invalidSuspect->{'explanation'};
		my ($id,$explanation) = $self->quote($suspectHashRef->{'id'},$explanationStr);
		my $sqlRequest = "INSERT INTO invalidSuspects(id,explanation) VALUES($id,$explanation)";
	
		if(!$self->prepareAndExecute($sqlRequest)){
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}
	}

}


# an internal function to save sentences linked with suspects.
sub registerSentence{
	my ($self,$sentenceHashRef)= @_;

	my ($sentId,$corpusId,$sent,$dag) = $self->quote($sentenceHashRef->{'sentId'},$sentenceHashRef->{'corpusId'},$sentenceHashRef->{'sent'},$sentenceHashRef->{'dag'});
	my $sqlRequest = "INSERT INTO sentences(sentId,corpusId,sent,dag) VALUES($sentId,$corpusId,$sent,$dag)";			
	if(!$self->prepareAndExecute($sqlRequest)) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}
}


#sub registerOrgResult{
#	my($self,$suspectId,$nbSucc,$nbFail,$nbTimO) = @_; 
		
#	my $sqlRequest = "UPDATE suspects SET orgNbSucc=$nbSucc, orgNbTimO=$nbTimO, orgNbFail=$nbFail WHERE id=$suspectId";
#	if(!$self->prepareAndExecute($sqlRequest)){
#		$self->errors(@{$self->{'EXECUTIONERRORS'}});
#	}
#}



#registerGroup($groupHashRef)
#Save the groups of suspects calculated and given by the references contained in the array @$groupHashRef.

sub registerGroups{
	my($self,$groupsAR) = @_; 

	my $cmpt = 1;
	foreach my $group (@$groupsAR){
		foreach my $suspectId (@$group){
			my $sqlRequest = "INSERT INTO suspectsGroups(groupId,suspectId) VALUES($cmpt,$suspectId)";
 			if(!$self->prepareAndExecute($sqlRequest)){
 				$self->errors(@{$self->{'EXECUTIONERRORS'}});
 			}
		}
		$cmpt++;
	}
}


#registerJokers($suspectId,$jokersAR)
#Save the jokers associated to the suspect designated by $suspectId and given by the references contained in the array @$jokersAR.

sub registerJokers{
	my ($self,$jokersHR)= @_;

	while(my ($suspectId,$jokersAR) = each %$jokersHR){
		foreach my $joker (@$jokersAR){
			my $sqlRequest = "INSERT INTO jokers(suspectId,joker,status,type,nbSucc,nbFail,nbTimO) VALUES($suspectId,'".$joker->{'joker'}.
								"','TODO','".join("@", @{$joker->{'type'}})."',0,0,0)";	
			if(!$self->prepareAndExecute($sqlRequest)) {
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
		}
	}

}


=pod 

=item * 
B<registerData($dataHR)>

Save the prepared data hash ref composed of four fields:
- 'suspects' =>  an array ref corresponding to the valid suspects,
- 'invalidSuspects' => an array ref corresponding to the invalid suspects,
- 'suspectsGroups' => an array ref corresponding to the suspects groups,
- 'jokers' => and hash ref corresponding to the jokers; .

=cut

sub registerData{
	my ($self,$data)= @_;

	$self->beginCycle();

	$self->registerSuspects($data->{'suspects'},$data->{'invalidSuspects'});
	$self->registerGroups($data->{'suspectsGroups'});
	$self->registerJokers($data->{'jokers'});
	
	my $sqlRequest = "INSERT INTO conf (key,value,type) VALUES ('step',1,'SCALAR')";	
	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	$self->endCycle();

}

=pod 

=item * 
B<dataAlreadyReady()>

Check if the data has already been prepared .

=cut


sub dataAlreadyReady{
	my ($self) = @_;
	my $sqlRequest = "select 1 from conf where key='step' AND value>=1";

	return $self->checkCondition($sqlRequest);
}




#getSuspects()
#Retrieve the suspects previously calculated and stored.

sub getSuspects{
	my ($self) = @_;

	my $sqlRequest = "SELECT * FROM suspects ORDER BY id";	
	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	my %res; 
 	while(my $row = $request->fetchrow_hashref()){
		$res{$row->{'id'}} = {	'id' => $row->{'primaryId'},
					'key' => $row->{'key'}, 
					'lex' => $row->{'lex'}, 
					'weight' => $row->{'weight'},
					'stem' => $row->{'stem'},
					'bestRes' => $row->{'bestRes'},
					'bestJoker' => $row->{'bestJoker'},
					'sentences' => $self->getAssSentences($row->{'primaryId'}),
					'detectedCats' => $self->getDetectedCats($row->{'primaryId'},$row->{'stem'})
				     };
	}
	
	my @res = values %res;
	return \@res;
}

# internal function to retrieve the sentences associated with the suspects.
sub getAssSentences{
	my ($self, $suspectId) = @_;

	my $sqlRequest = "SELECT corpusId corpusId, sentId sentId FROM assoSuspectSent WHERE suspectId='$suspectId' ";	
	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	my $row = $request->fetchrow_hashref();
	
	my @res; 
 	while($row){
		my ($sentId,$corpusId) = ($row->{'sentId'},$row->{'corpusId'});
		my $sqlRequest = "SELECT sent, dag FROM sentences WHERE sentId='$sentId' and corpusId='$corpusId'";	
		my $requestSent;
		if(!($requestSent = $self->prepareAndExecute($sqlRequest))) {
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}
		my $rowSent = $requestSent->fetchrow_hashref();

		push(@res, {	'corpusId' => $row->{'corpusId'},
				'sentId' => $row->{'sentId'},
				'sent' => $rowSent->{'sent'},
				'dag' => $rowSent->{'dag'}
			   });
		$row = $request->fetchrow_hashref();
	}
	return \@res;
}

sub getDetectedCats{
	my ($self, $suspectId, $stem) = @_;
	my ($suspectIdStr,$stemStr) = $self->quote($suspectId, $stem);

	my @res;
	my $sqlRequest = "SELECT detectedCat from suspectsStem where suspectId=$suspectIdStr AND stem=$stemStr";
	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))){
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}
	while(my $row = $request->fetchrow_hashref()){
		push(@res,$row->{'detectedCat'});	
	}

	return \@res;
}


#getJokers()
#Retrieve the jokers previously calculated and stored.

sub getJokers{
	my($self) = @_;

	my $sqlRequest = "SELECT suspectId, joker, type FROM jokers WHERE status='TODO'";	
	my $request;
	if(!($request = $self->prepareAndExecute($sqlRequest))) {
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}

	my %res; 
 	while(my $row = $request->fetchrow_hashref()){
		push(@{$res{$row->{'suspectId'}}},{'joker'=> $row->{'joker'}, 'type' => split("@", $row->{'type'}), 'status' => 'TODO'});
	}
	return \%res;
}


=pod 

=item * 
B<getData()>

Retrieve the data previously prepared and stored.

=cut

sub getData{
	my($self) = @_;

	my $res = {};
	$self->msgs("Retrieving the list of suspects...");
	$res->{'suspects'} = $self->getSuspects();
	
	$self->msgs("Retrieving the jokers...");
	$res->{'jokers'} = $self->getJokers();

	return $res;
}


#<registerAnalysis($joker,$suspectId,$sentId,$corpusId,$status)>
#Register a $status analysis with the joker $joker of the sentence $sentId $corpusId linked to the suspect $suspectId.

sub registerAnalysis{
	my ($self,$jokerStr,$suspectId,$sentId,$corpusId,$status)= @_; 
	($sentId,$corpusId,$status) = $self->quote($sentId,$corpusId,$status);

	my $sqlRequest = "INSERT INTO analysis(joker,suspectId,sentId,corpusId,status) VALUES($jokerStr,$suspectId,$sentId,$corpusId,$status)";
	
	if(!$self->prepareAndExecute($sqlRequest)){
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}
}

#<analysisCycleCompleted($joker,$suspectId,$nbSucc,$nbTimO,$nbFail)>
#Register a completed cycle of analysis indexed by ($joker,$suspectId) with $nbSucc success ,$nbTimO timeouts ,$nbFail failures
sub analysisCycleCompleted{
	my ($self,$joker,$suspectId,$nbSucc,$nbTimO,$nbFail)= @_; 

	my $sqlRequest = "UPDATE jokers SET status='DONE', nbSucc=$nbSucc, nbTimO=$nbTimO, nbFail=$nbFail WHERE joker=$joker AND suspectId=$suspectId";
	
	if(!$self->prepareAndExecute($sqlRequest)){
		$self->errors(@{$self->{'EXECUTIONERRORS'}});
	}
}


=item * 
B<registerResults($suspect,$joker,$parses,$hypothesisHashRef,$markHashRef)>

Register all the results produced by hypothesis produced during a cycle indexed by ($suspectId,$joker).
=back

=cut

sub registerResults{
	my ($self,$suspect,$jokerHash,$job,$hypothesisHashRef,$markHashRef) = @_;
	my $sqlRequest;

	my ($suspectId,$joker) = $self->quote($suspect->{'id'},$jokerHash->{'joker'});

	$self->beginCycle();
	my @success = @{$job->{'success'}};
	my @failures = @{$job->{'failures'}};
	my @timeouts = @{$job->{'timeouts'}};

	# we record the analyses 
	foreach my $parse (@success){ $self->registerAnalysis($joker,$suspectId,$parse->{'sentId'},$parse->{'corpusId'},'S');}
	foreach my $parse (@failures){ $self->registerAnalysis($joker,$suspectId,$parse->{'sentId'},$parse->{'corpusId'},'F');}
	foreach my $parse (@timeouts){ $self->registerAnalysis($joker,$suspectId,$parse->{'sentId'},$parse->{'corpusId'},'T');}

	my %relations;
	while(my ($cle,$valeur) = each %{$hypothesisHashRef}){	
		my ($id,$cat,$points) = $self->quote($cle,$valeur->{'cat'},$markHashRef->{$cle}->{'points'});
		$sqlRequest = "INSERT INTO hypothesis(id,suspectId,joker,cat,points) VALUES($id,$suspectId,$joker,$cat,$points)";
		if(!($self->prepareAndExecute($sqlRequest))) {
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}

		foreach my $analysisOutput (@{$valeur->{'analysisOutputs'}}){
			my($sentId,$corpusId) = $self->quote($analysisOutput->{'modifiedDag'}->{'sentence'}->{'sentId'},$analysisOutput->{'modifiedDag'}->{'sentence'}->{'corpusId'});
			$sqlRequest = "INSERT INTO hypothesisAnalysis(analysisId,hypoId,joker,suspectId) VALUES((SELECT id FROM analysis WHERE joker=$joker AND suspectId=$suspectId AND sentId=$sentId AND corpusId=$corpusId),$cle,$joker,$suspectId)";
			
			if(!($self->prepareAndExecute($sqlRequest))) {
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
		}

		foreach my $relation (@{$valeur->{'relations'}}){
			$relations{$relation->{'number'}} = $relation;

			my ($idRel) = $self->quote($relation->{'number'});
			$sqlRequest = "INSERT INTO hypothesisRelations(idHypo,joker,suspectId,idRel) VALUES($id,$joker,$suspectId,$idRel)";
		
			if(!($self->prepareAndExecute($sqlRequest))) {
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
			
		}

		foreach my $graphDep (@{$valeur->{'graphDep'}}){
			my ($idDest) = $self->quote($graphDep);
			$sqlRequest = "INSERT INTO hypothesisGraphDep(idSrc,joker,suspectId,idDest) VALUES($id,$joker,$suspectId,$idDest)";
			
			if(!($self->prepareAndExecute($sqlRequest))){
				$self->errors(@{$self->{'EXECUTIONERRORS'}});
			}
		}
	}


	while(my ($cle,$valeur) = each %relations){
		my ($id,$type,$catDestOrig,$label) = $self->quote($cle,$valeur->{'type'},$valeur->{'catDestOrg'},$valeur->{'label'});
		$sqlRequest = "INSERT INTO relations(id,joker,suspectId,type,catDestOrig,label) VALUES($id,$joker,$suspectId,$type,$catDestOrig,$label)";
		
		if(!($self->prepareAndExecute($sqlRequest))){
			$self->errors(@{$self->{'EXECUTIONERRORS'}});
		}
	}

	$self->analysisCycleCompleted($joker,$suspectId,scalar(@success),scalar(@timeouts),scalar(@failures));
	$self->endCycle();
}


return 1;


