#!/usr/bin/perl -w

use lib '../common';
use lib './moduls';

#require Exporter;

use strict;
use CmdLineProgram;
use AppConfig qw(:expand :argcount);
use Config;
use TraceResumeSql;
use ClassicInput;
use ParseManager;
use ExtractHypothesis;
use Mark;
use HypothesisFilterByMark;

=pod

=head2 B<lexFix.pl>

Program used to make hypothesis on suspected lemmas.
Its command line parameters are described in the helps contents given by 'lexFix.pl -v'.
Some of the command lines parameters are given for the moduls used.

B<Example of command line execution with TraceResumeSql.pm as the 'TraceResume' modul :>
./lexFix.pl -c lexFix.conf -d 4 -resume_dbURI dbi:SQLite:/home/mush/exportbuild/src/errorMining/test.dat -resume_dbPassword " " -resume_dbUserName " "

=cut


## This hash is used to connect the moduls used
my %hashModulesNames = ('TraceResume'=>'TraceResumeSql', 
			'Input' => 'ClassicInput',
			'Parser' => 'ParseManager',
			'ExtractHypothesis'=>'ExtractHypothesis', 
			'Mark'=>'Mark',
			'HypothesisFilter'=>'HypothesisFilterByMark'
			);
## We store the modul objects in this hash one object for each modul in @listModule
my %hashModules;

## the global configuration object
my $conf = AppConfig->new({CASE => 1,  ERROR => \&configErrors}); ## config

#######################Program Description Variables###########################
my $progName = 'Lex Fix';
my $progCmd = 'lexFix.pl';
my $progVersion = 0.7;

#######################General Variables#######################################
$conf->define('directory|d=s',{DEFAULT => `pwd`});
$conf->define('confFilePath|c=s',{DEFAULT => './lexFix.conf'});
$conf->define('debug|d=s>',{DEFAULT => "0",VALIDATE => sub{ my ($name,$value) = @_; (($value>=0) and ($value <= 5))? 1:initErrors($progName,'Incorrect debugLevel value');}});
$conf->define('version|v!');
$conf->define('help|h!');
$conf->define('skey=s>');
$conf->define('breath=s>',{DEFAULT => 0});
$conf->define('log=s');

##############################Functions#############################################
my %funcMsgs;
$funcMsgs{'debug'} = \&debug;
$funcMsgs{'msgs'} = \&printMsgs;
$funcMsgs{'errors'} = \&printErrors;
$funcMsgs{'initErrors'} = \&initErrors;
$funcMsgs{'warnings'} = \&printWarns;
$funcMsgs{'delimit'} = \&delimitor;

#########################General Execution Functions############################

my $versionContents = "<$progName version $progVersion>\n";
setVersionContents($versionContents);

my $helpContents = "$versionContents\n\n".
	"Usage: $progCmd OPTIONS\n\n".
	"The Options are:\n".
	"--confFilePath=PATH or -c=PATH\tto specify the config file PATH default=".$conf->confFilePath."\n".
	"--version -v\t\t\tto get Version number\n".
	"--debug=[1..5] -d=[1..5]\t\tto specify the debug level from 1 (no information)".
	" to 5 (maximum information)\n".
	"--help -h\t\t\tto display these help content\n\n";
setHelpContents($helpContents);

  
##############Initialization Functions####################

# this function is used to check and prepare everything before runnin the program
sub init{

	while(my($module,$moduleName) = each %hashModulesNames){
		my $variablesModule = $moduleName->getVariables();
		
		while(my ($key,$value) = each (%$variablesModule)){
			if("$value" eq "SCALAR"){
				$key = $key."=s";
			}elsif("$value" eq "ARRAY"){
				$key = $key."=s\@";
			}elsif("$value" eq "HASH"){
				$key = $key."=s%";
			}
			# define the modul variable in the global configuration object
			$conf->define($key);
		}
	}

	# Keep a copy of command line arguments for later
	my @commandLineArgsCpy = @ARGV; 
	# Parse command line arguments
	$conf->args();	

	# user asked for the help menu ?
	if($conf->help()){ print helpContents(); exit();}	
	# user asked for the version menu ?
	if($conf->version()){ print versionContents(); exit();}

	# Parse ConfigFile
	$conf->file($conf->confFilePath());

	# Initialize the resume and trace results moduls 
 	$hashModules{"TraceResume"} = $hashModulesNames{"TraceResume"}->new({$conf->varlist("^.*\$")},\%funcMsgs);
	
	# Shall we continue previous execution? 
	# if there is already a registered configuration in result file, yes
 	if($hashModules{"TraceResume"}->checkConfExistence()){
		## Reset Conf
		#$conf = AppConfig->new({CASE => 1,  ERROR => \&configErrors});
 		printMsgs($progName,"Previous Execution detected => let's get back the configuration");
		my %previousConf = $hashModules{"TraceResume"}->getConf();

		while(my ($cle,$valeur) = each %previousConf){
			if($valeur->{'type'} eq 'SCALAR'){
				$conf->$cle($valeur->{'value'});
			}elsif($valeur->{'type'} eq 'HASH'){
				while(my ($key, $value) = each %{$valeur->{'value'}}){
					$conf->$cle("$key=$value");
				}
			}
			else{
				foreach my $elem (@{$valeur->{'value'}}){
					$conf->$cle($elem);
				}
			}
			my $varRef = $conf->$cle();
		}
 		printMsgs($progName,"Configuration successfully recuperated");
 	}
 
	# The command Line arguments could have been overwrited so we check them again;
	$conf->args(\@commandLineArgsCpy);
	
	$debugLevel = $conf->debug;
	my $confHash = {$conf->varlist("^.*\$")}; 
	
	if($conf->debug >= 5){
		my @debugMsgs;
		push(@debugMsgs,"execution values");
		
		while(my ($cle,$valeur) = each %$confHash) {
			if(defined($valeur)){
				push(@debugMsgs,"$cle = $valeur");
			}
		} 
		debug($progName,5,@debugMsgs);
	}

	# Intialize all moduls execpt resume and trace modul which has already been initialized 
	while(my($module,$moduleName) = each %hashModulesNames){
		if(!($module eq "TraceResume")){
			$hashModules{$module} = $moduleName->new({$conf->varlist("^.*\$")},\%funcMsgs);
		}
	}
	
 	# Save the configuration
 	$hashModules{"TraceResume"}->saveConf($confHash);
	addFinalizeFunction(sub { foreach my $module (keys %hashModulesNames){ $hashModules{$module}->finalize() } });

}


###########################Main Programm#############################################

printMsgs($progName,"STARTING NEW EXECUTION PID = $$");

#initialize program
delimitor(); printMsgs($progName,"Checking configuration...");
init();
printMsgs($progName,"=> Initialization done"); delimitor();


do{

my (%jokers,@suspects);
{
	my $data;
	if(!$hashModules{'TraceResume'}->dataAlreadyReady()){
		printMsgs($progName,"Preparing data...");

		$data = $hashModules{'Input'}->prepareData();
		$hashModules{'TraceResume'}->registerData($data);
	}
	else{
		printMsgs($progName,"Data already prepared before => resuming");
		$data = $hashModules{'TraceResume'}->getData()
	}
	
	%jokers = %{$data->{'jokers'}};
	@suspects = @{$data->{'suspects'}};
	printMsgs($progName,"Data is ready");

}


$hashModules{'Parser'}->setCurrentData(\@suspects,\%jokers);

while($hashModules{'Parser'}->stillWorkToDo()){
	
	my @jobsDone = $hashModules{'Parser'}->doSomeWork();
	
	foreach my $job (@jobsDone){
		my ($suspect, $joker) = ($job->{'suspect'},$job->{'joker'});
		my ($nbSuccess,$nbFail,$nbTmo) = (scalar(@{$job->{'success'}}),scalar(@{$job->{'failures'}}),scalar(@{$job->{'timeouts'}}));
				
		debug($progName,4,'Parses of '.$suspect->{'key'}."(".$suspect->{'id'}.") with joker ".$joker->{'joker'}." => ".
				"$nbSuccess success, $nbFail failures, $nbTmo timeouts");
	
		## we extract the hypothesis of the syntax analyser outputs
		my %hypothesisHash = $hashModules{"ExtractHypothesis"}->extractHypothesis($suspect->{'key'},$job->{'success'});
	
		## and we give them a mark
		my %marksHash = $hashModules{"Mark"}->getMarks(\%hypothesisHash);

		## and we filter useless hypothesis
		$hashModules{"HypothesisFilter"}->filterHypothesis(\%hypothesisHash,\%marksHash);

		## finaly we record them	
		$hashModules{"TraceResume"}->registerResults($suspect,$joker,$job,\%hypothesisHash,\%marksHash);
	}
	
	
}

}while($hashModules{'Input'}->stillMoreData());

finalize();


## Code Garbage => code that I suppose I won't use anymore but I keep for a time (just in case)

#my @ps = `ps u`;
#	foreach my $ps (@ps) { 
#		if($ps =~ /lexFix.pl/){ 
#			my $memory = (split /\s+/, $ps)[4];
#			printMsgs($progName, "Memory used => $memory");
#			if($memory > 600000){
#				printErrors($progName,"too much memory used!!!");
#		       	}	
#		}
#	}	

# 	$hashModules{'TraceResume'}->beginCycle();


#printMsgs($progName,"New cycle with suspect id => ".$suspect->{'id'}." key: '".$suspect->{'key'}.
#				"' score: ".$hashModules{'OrderSuspects'}->score($suspect));
	
#	## first we get a new joker for this group
#	my $joker = undef;
#	if(exists($jokersHash{$suspect->{'id'}})){
#     		$joker = shift @{$jokersHash{$suspect->{'id'}}};
#	}
#	if(!defined($joker)){
#		debug($progName,2,"No jokers left for this suspect, nothing left to do with it");
#		delete $jokersHash{$suspect->{'id'}};
#		$suspect = shift @suspectsArr;
#		$hashModules{'TraceResume'}->endCycle();
#		next;
#	}
#	else{
#		debug($progName,2,'New joker: '.$joker->{'joker'}.' Type: '.$joker->{'type'});
#	}


#	debug($progName,2,@{$suspect->{'sentences'}}." sentences to analyze");
#	debug($progName,2,$suspect->{'bestRes'}." sentences already succeed with joker ".$suspect->{'bestJoker'});
#	my %dagHash = $hashModules{'DagExtract'}->getDags($suspect->{'sentences'});

#	if(($suspect->{'orgNbSucc'} == 0) && ($suspect->{'orgNbFail'} == 0) && ($suspect->{'orgNbTimO'} == 0)){
#		debug($progName,4,"The original analasys rate has not been done so far for this suspect, I shall calcul it now");
#		
#		my @regularDags;
#		foreach my $sentence (@{$suspect->{'sentences'}}){
#			push(@regularDags,{ 'sentence' => $sentence,
#					    'dag' => $dagHash{$sentence->{'corpusId'}."-".$sentence->{'sentId'}}
#				    });
#		}
		
#		my @analysisOutputs = $hashModules{'AnalyzeDag'}->AnalyzeDags(\@regularDags);
	
#		my ($nbSucc,$nbFail,$nbTimO) = (0,0,0);
#		foreach my $analysisOutput (@analysisOutputs){
#			if($analysisOutput->{'status'} eq 'success'){ $nbSucc++;}
#			elsif($analysisOutput->{'status'} eq 'failed') { $nbFail++;}
#			elsif($analysisOutput->{'status'} eq 'timeout') { $nbTimO++;}
#			$hashModules{"TraceResume"}->registerAnalysis('none',$suspect->{'id'},$analysisOutput->{'dag'}->{'sentence'},
#									$analysisOutput->{'status'});		
#		}
		
#		($suspect->{'orgNbSucc'},$suspect->{'orgNbFail'},$suspect->{'orgNbTimO'}) = ($nbSucc,$nbFail,$nbTimO);
#		$hashModules{"TraceResume"}->registerOrgResult($suspect->{'id'},$nbSucc,$nbFail,$nbTimO);
#	}
#	debug($progName,3,"The original analasys rate is => Success: ".$suspect->{'orgNbSucc'}." Failed: ".$suspect->{'orgNbFail'}.
#				" TimeOuts: ".$suspect->{'orgNbTimO'}
#	     );

## we modify the dags
#	my @modifiedDags;
#	foreach my $sentence (@{$suspect->{'sentences'}}){
#		my $modifiedDag = $hashModules{'ModifyDag'}->ModifyDag($suspect->{'key'},$suspect->{'lex'},$joker->{'joker'},
#						$dagHash{$sentence->{'corpusId'}."-".$sentence->{'sentId'}}) ;
#		push(@modifiedDags,{ 'sentence' => $sentence,
#				     'dag' => $modifiedDag
#				    });

#		if($conf->debug() < 4){ next;}
#		debug($progName,4,"Sentence ".$sentence->{'corpusId'}."-".$sentence->{'sentId'}." => ".$sentence->{'sent'});
#		debug($progName,5,$modifiedDag);
#	}
	

#	if($nbSucc > $suspect->{'bestRes'}){
#		$suspect->{'bestRes'} = $nbSucc;
#		$suspect->{'bestJoker'} = $joker->{'joker'};
#		$hashModules{"TraceResume"}->updateBestRes($suspect->{'id'},$nbSucc,$joker->{'joker'})
#	}



#@suspectsArr = $hashModules{'OrderSuspects'}->reInsertAndOrder($suspect,\@suspectsArr); 
#	$suspect = shift @suspectsArr;

