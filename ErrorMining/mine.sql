CREATE TABLE mine (id INTEGER,key,weight FLOAT,occ INTEGER, focc INTEGER,
                  PRIMARY KEY(id),
                  UNIQUE(id)
);

CREATE TABLE history (id INTEGER,iter INTEGER,weight FLOAT,
                  PRIMARY KEY(id,iter)
);

CREATE TABLE sentence (id INTEGER, srank INTEGER,corpus,sid,sentence,
                  PRIMARY KEY(id,srank)
);

CREATE TABLE lemma (id INTEGER, lemma);
