
use Test::More tests => 3;
use_ok( Catalyst::Test, 'ErrorMining' );
use_ok('ErrorMining::Controller::Comment');

ok( request('comment')->is_success );

