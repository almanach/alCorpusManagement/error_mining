
use Test::More tests => 3;
use_ok( Catalyst::Test, 'ErrorMining' );
use_ok('ErrorMining::Controller::Mine');

ok( request('mine')->is_success );

