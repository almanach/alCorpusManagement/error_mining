package Catalyst::Plugin::MyTextile;

use strict;
use base 'Class::Data::Inheritable';
use Text::Textile;

our $VERSION = '0.01';

__PACKAGE__->mk_classdata('textile');
__PACKAGE__->textile( Text::Textile->new );

sub mytextile {
  my ($c,$s) = @_;
##  $s =~ s{rid:(\d+)\b}{"\"rid:$1\":".$c->uri_for('/report/list',{id=>$1})}eg;
##  $s =~ s{bid:(\d+)\b}{"\"bid:$1\":".$c->uri_for('/report/list',{bid=>$1})}eg;
##  $s =~ s{(\w+):(E\d+)\b}{"\"$1:$2\":".$c->uri_for('/view',{corpus=>$1,sentence=>$2, showreports=>1})}eg;
  $s =~ s{::(\S+)}{"\"::$1\":".$c->uri_for('/list',{rank => "::$1"})}eg;
  Text::Textile->new->process($s);
}

1;
