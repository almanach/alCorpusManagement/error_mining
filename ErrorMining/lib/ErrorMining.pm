package ErrorMining;

use strict;
use warnings;

use Moose;
use namespace::autoclean;

use Catalyst::Runtime '5.80';

#
# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
# Static::Simple: will serve static files from the applications root directory
#
use Catalyst qw/-Debug 
		ConfigLoader
		HTML::Widget
		Static::Simple
		MyTextile
		Session
                Session::State::Cookie
                Session::Store::FastMmap
                Authentication
                Authorization::Roles
                Compress::Gzip
	       /;

extends 'Catalyst';

our $VERSION = '0.01';
$VERSION = eval $VERSION;

#
# Configure the application
#
## loading errormining.yml
__PACKAGE__->config( name => 'ErrorMining',
		     encoding => 'UTF-8',                    
		     # Disable deprecated behavior needed by old applications
		     disable_component_resolution_regex_fallback => 1,
		   );

__PACKAGE__->config->{'Plugin::Authentication'} =
  {
   default_realm => 'members',
   members => {
               credential => {
                              class => 'Password',
                              password_field => 'password',
                              password_type => 'hashed',
                              password_hash_type => 'SHA-1'
                             },
               store => {
                         class => 'DBIx::Class',
                         ## warning about Catalyst::Authentication::Store::DBIx::Class
                         ## the class now uses a many_to_many relation in User to access the Role table
                         user_model => 'CommentSchema::User',
                         role_relation => 'roles',
                         role_field => 'role',
                        }
              }
  };

#
# Start the application
#
__PACKAGE__->setup;

=head1 NAME

ErrorMining - Catalyst based application

=head1 SYNOPSIS

    script/errormining_server.pl

=head1 DESCRIPTION

Catalyst based application.

=head1 METHODS

=over 4

=item default

=cut

#
# Output a friendly welcome message
#
sub default : Private {
    my ( $self, $c ) = @_;

    # Hello World
    $c->response->body( $c->welcome_message );
}

#
# Uncomment and modify this end action after adding a View component
#
#=item end
#
#=cut
#
#sub end : Private {
#    my ( $self, $c ) = @_;
#
#    # Forward to View unless response body is already defined
#    $c->forward('View::') unless $c->response->body;
#}

=back

=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
