package ErrorMining::View::Simple;

use strict;
use base 'Catalyst::View::TT';
use NEXT;

__PACKAGE__->config(
		    TEMPLATE_EXTENSION => '.tt2',
		    CATALYST_VAR => 'Catalyst',
		    INCLUDE_PATH => [
				     ErrorMining->path_to( 'root', 'src' ),
#				     ErrorMining->path_to( 'root', 'lib' )
				    ],
		    ERROR        => 'error.tt2',
		    TIMER        => 0,
		    COMPILE_DIR => '/tmp/template_cache'
		   );

sub process {
  my ( $self, $c ) = @_;
  $c->response->content_type('text/html; charset=ISO8859-1')
    unless ($c->res->content_type);
  $self->NEXT::process($c);
}

=head1 NAME

ErrorMining::View::Simple - TT View for ErrorMining

=head1 DESCRIPTION

TT View for ErrorMining. 

=head1 AUTHOR

=head1 SEE ALSO

L<ErrorMining>

De la Clergerie Eric

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
