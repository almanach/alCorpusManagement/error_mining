package ErrorMining::View::TT;

use strict;
use base 'Catalyst::View::TT';
use NEXT;

__PACKAGE__->config({
    INCLUDE_PATH => [
        ErrorMining->path_to( 'root', 'src' ),
        ErrorMining->path_to( 'root', 'lib' )
    ],
    PRE_PROCESS  => 'config/main',
    WRAPPER      => 'site/wrapper',
    ERROR        => 'error.tt2',
    TIMER        => 0,
		     CATALYST_VAR => 'Catalyst',
    render_die => 1
});

sub process {
  my ($self,$c) = @_;
  $c->response->content_type('text/html; charset=ISO-8859-1');
  $self->NEXT::process($c);
}

=head1 NAME

ErrorMining::View::TT - Catalyst TTSite View

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst TTSite View.

=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

