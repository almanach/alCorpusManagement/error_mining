package ErrorMining::CommentSchema;

use strict;
use warnings;

use Moose;
#use MooseX::NonMoose;
use namespace::autoclean;

# Our schema needs to inherit from 'DBIx::Class::Schema'
extends 'DBIx::Class::Schema';

__PACKAGE__->load_classes(qw/Comment User UserRole Role/);
__PACKAGE__->meta->make_immutable;

1;
