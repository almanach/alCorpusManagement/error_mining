package ErrorMining::CommentSchema::User;
    
use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';
    
# Load required DBIC stuff
__PACKAGE__->load_components(qw/HTMLWidget/);
# Set the table name
__PACKAGE__->table('users');
# Set columns in table
__PACKAGE__->add_columns(qw/id username password email_address first_name last_name/);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);
 
##__PACKAGE__->utf8_columns(qw/first_name last_name/);

   
#
# Set relationships:
#

# has_many():
#   args:
#     1) Name of relationship, DBIC will create accessor with this name
#     2) Name of the model class referenced by this relationship
#     3) Column name in *foreign* table
# __PACKAGE__->has_many(book_authors => 'MyAppDB::BookAuthor', 'book_id');

__PACKAGE__->has_many(map_user_role => 'ErrorMining::CommentSchema::UserRole', 'user_id');

#__PACKAGE__->has_many(comments => 'ErrorMining::CommentSchema::Comment', 'key');

__PACKAGE__->many_to_many(roles => 'map_user_role', 'role');

# many_to_many():
#   args:
#     1) Name of relationship, DBIC will create accessor with this name
#     2) Name of has_many() relationship this many_to_many() is shortcut for
#     3) Name of belongs_to() relationship in model class of has_many() above 
#   You must already have the has_many() defined to use a many_to_many().
#__PACKAGE__->many_to_many(authors => 'book_authors', 'author');
    
=head1 NAME
  
  MyAppDB::Book - A model object representing a book.
    
  =head1 DESCRIPTION
  
  This is an object that represents a row in the 'books' table of your application
  database.  It uses DBIx::Class (aka, DBIC) to do ORM.
  
  For Catalyst, this is designed to be used through EasyRef::Model::EasyRefDB.
  Offline utilities may wish to use this class directly.
  
=cut

__PACKAGE__->meta->make_immutable;
  
1;
