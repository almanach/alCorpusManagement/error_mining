package ErrorMining::CommentSchema::Comment;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

## __PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('comment');
__PACKAGE__->add_columns(
			 key => {
				 data_type => 'string'
				},
			 comment => {
				     data_type => 'string'
				 },
                         date => {},
                         status => { },
			 author_id => {},
			 type => {},
			 assigned => {}
			);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/key/);

__PACKAGE__->meta->make_immutable;

1;
