package ErrorMining::CommentSchema::Role;
    
use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';
    
# Load required DBIC stuff
#__PACKAGE__->load_components(qw/PK::Auto Core/);

# Set the table name
__PACKAGE__->table('roles');
# Set columns in table
__PACKAGE__->add_columns(qw/id role/);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);
    
#
# Set relationships:
#

# has_many():
#   args:
#     1) Name of relationship, DBIC will create accessor with this name
#     2) Name of the model class referenced by this relationship
#     3) Column name in *foreign* table
# __PACKAGE__->has_many(book_authors => 'MyAppDB::BookAuthor', 'book_id');

__PACKAGE__->has_many(map_user_role => 'ErrorMining::CommentSchema::UserRole', 'role_id');

# many_to_many():
#   args:
#     1) Name of relationship, DBIC will create accessor with this name
#     2) Name of has_many() relationship this many_to_many() is shortcut for
#     3) Name of belongs_to() relationship in model class of has_many() above 
#   You must already have the has_many() defined to use a many_to_many().
#__PACKAGE__->many_to_many(authors => 'book_authors', 'author');
    
=head1 NAME
  
  MyAppDB::Role - A model object representing a role.
    
  =head1 DESCRIPTION
  
  This is an object that represents a row in the 'roles' table of your application
  database.  It uses DBIx::Class (aka, DBIC) to do ORM.
  
  For Catalyst, this is designed to be used through EasyRef::Model::EasyRefDB.
  Offline utilities may wish to use this class directly.
  
=cut
  
__PACKAGE__->meta->make_immutable;

1;
