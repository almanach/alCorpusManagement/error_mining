package ErrorMining::Controller::User;

use strict;
use warnings;
use base 'Catalyst::Controller';
use Digest::SHA1 qw/sha1_hex/;

sub make_widget {
  my ( $self, $c, $mode, $user ) = @_;
    
  # Create an HTML::Widget to build the form
  my $w = $c->widget('users')->method('post');

  # Create the form fields
  $w->element('Textfield', 'username')->label('Username')->size(15);
  $w->element('Textfield', 'first_name')->label('Pr�nom')->size(30);
  $w->element('Textfield', 'last_name')->label('Nom')->size(30);
  $w->element('Textfield', 'email_address')->label('Email')->size(30);

  if ($c->check_user_roles('admin')) {
    $w->element('Span','Roles')->content('Roles');
    foreach my $r ($c->model('CommentSchema::Role')->all) {
      my $x = $w->element('Checkbox','role');
      $x->label($r->role);
      $x->value($r->id);
    }
  }

  if (defined $user) {
    $w->element('Password', 'old_passwd')->label('Ancien Mot de passe')->size(10);
  }
  $w->element('Password', 'new_passwd1')->label('Nouveau Mot de passe')->size(10);
  $w->element('Password', 'new_passwd2')->label('Retapper le Mot de passe')->size(10);
  $w->element('Reset',    'Cancel' )->value('Cancel');
  $w->element('Submit',    'Update' )->value('Submit');

  $w->constraint(Email => 'email_address');
  
  if (!defined $user) {
    $w->constraint(All => qw/username first_name last_name email_address new_passwd1 new_passwd2/);
  } else {
    $w->constraint(All => qw/username first_name last_name email_address/);
  }

  $w->constraint('Callback' => 'username')
    ->callback(sub { my $username = shift; 
		     return (defined $user && ($username eq $user->username)) 
		       || !($c->model('CommentSchema::User')->find({username => $username}));
		   })
      ->message( "Username already used");
  $w->constraint('Equal' => qw/new_passwd1 new_passwd2/)
    ->message( 'New passwords are not equal');

  # Return the widget    
  return $w;
}

sub view : Local {
  my ( $self, $c, $uid ) = @_;

  $uid ||= $c->user->obj->id;

  my $item = $c->model('CommentSchema::User')->find($uid);

  if ($c->check_user_roles('admin') || $c->user->obj->id == $uid) {

    $c->stash->{item} = $item;
    
    my $w = $self->make_widget($c,'updating',$item);  

    $c->req->params->{username} ||= $item->username;
    $c->req->params->{first_name} ||= $item->first_name;
    $c->req->params->{last_name} ||= $item->last_name;
    $c->req->params->{email_address} ||= $item->email_address;


    my @roles = ();
    foreach my $r ($item->map_user_role->all) {
      push(@roles,$r->role_id);
    }
    $c->req->params->{role} ||= [@roles];

    $w->action($c->uri_for("view/$uid"));
    
    
    # Validate the form parameters
    my $result = $w->process($c->req);
    $c->stash->{widget_result} = $result;
    
    # Were their validation errors?
    if ($result->has_errors) {
      # Warn the user at the top of the form that there were errors.
      # Note that there will also be per-field feedback on
      # validation errors because of '$w->process($c->req)' above.
      $c->stash->{error_msg} = 'Validation errors!';
    } else {
      $item->username($c->req->params->{username});
      $item->first_name($c->req->params->{first_name});
      $item->last_name($c->req->params->{last_name});
      $item->email_address($c->req->params->{email_address});
      $item->password( sha1_hex($c->req->params->{new_passwd1})) 
	if ($c->req->params->{new_passwd1});
      if ($c->check_user_roles('admin')) {
	my $roles = $c->req->params->{role};
	my %roles = ();
	$item->map_user_role->delete;
	foreach my $r (ref $roles ? @$roles : $roles) {
	  $item->find_or_create_related('map_user_role' => {role_id => $r});
	}
      }
#      $c->res->redirect( $c->uri_for('list') );
    }
    $c->stash->{template} = 'user/view.tt2';
##    $c->res->headers->push_header( 'Content-Type' => "text/html; charset=ISO-8859-1" );
  }
}


sub add : Local {
  my ( $self, $c ) = @_;

  if ($c->check_user_roles('admin')) {
    my $w = $self->make_widget($c,'creation',undef);  

    $c->req->params->{role} = [qw/1 2/];

    $w->action($c->uri_for('add'));
    
    # Validate the form parameters
    my $result = $w->process($c->req);
    $c->stash->{widget_result} = $result;
  
    # Were their validation errors?
    if ($result->has_errors) {
      # Warn the user at the top of the form that there were errors.
      # Note that there will also be per-field feedback on
      # validation errors because of '$w->process($c->req)' above.
      $c->stash->{error_msg} = 'Validation errors!';
    } else {
      my $item = {
		  username => $c->req->params->{username},
		  first_name => $c->req->params->{first_name},
		  last_name => $c->req->params->{last_name},
		  email_address => $c->req->params->{email_address},
		  password => sha1_hex($c->req->params->{new_passwd1})
		 };
      #    $c->stash->{item} = $item;
      my $x = $c->model('CommentSchema::User')->create($item);
      my $roles = $c->req->params->{role};
      foreach my $r (@$roles) {
	$x->add_to_map_user_role({role_id => $r});
      }
      $c->res->redirect( $c->uri_for('list') );
    }
    $c->stash->{template} = 'user/add.tt2';
##    $c->res->headers->push_header( 'Content-Type' => "text/html; charset=ISO-8859-1" );
  } else {
    $c->res->body('Unauthorized !');
  }
}

sub list : Local {
  my ( $self, $c ) = @_;
  if ($c->check_user_roles('admin')) {
    my $users = [$c->model('CommentSchema::User')->all];
    $c->stash->{users} = $users;
    $c->stash->{template} = 'user/list.tt2';
##    $c->res->headers->push_header( 'Content-Type' => "text/html; charset=ISO-8859-1" );
  } else {
    $c->res->body('Unauthorized');
  }
}

sub delete : Local {
 my ( $self, $c, $uid ) = @_;
  if ($c->check_user_roles('admin') && $uid ne $c->user->obj->id) {
    $c->model('CommentSchema::User')->find($uid)->delete;
##    $c->model('ErrorMiningDB::UserRole')->search({user_id => $uid})->delete;
    $c->res->redirect( $c->uri_for('list') );
  } else {
    $c->res->body('Unauthorized');
  }
}

=head1 NAME

ErrorMining::Controller::User - Catalyst Controller

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('ErrorMining::Controller::User is on Catalyst!');
#}

=back

=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
