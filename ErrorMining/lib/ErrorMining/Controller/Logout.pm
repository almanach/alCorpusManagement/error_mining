package ErrorMining::Controller::Logout;

use strict;
use warnings;
use base 'Catalyst::Controller';

 sub index : Private {
   my ($self, $c) = @_;
   
   # Clear the user's state
   $c->logout;
   
   # Send the user to the starting point
   $c->response->redirect($c->uri_for('/login'));
 }

=head1 NAME

ErrorMining::Controller::Logout - Catalyst Controller

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('ErrorMining::Controller::Logout is on Catalyst!');
#}

=back


=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
