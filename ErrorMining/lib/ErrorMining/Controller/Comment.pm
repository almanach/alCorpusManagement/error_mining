package ErrorMining::Controller::Comment;

use strict;
use warnings;
use base 'Catalyst::Controller';


sub make_widget {
  my ( $self, $c ) = @_;
    
  # Create an HTML::Widget to build the form
  my $w = $c->widget('comments')->method('post');

  # Create the form fields
  $w->element('Textfield', 'key')->label('Key')->size(20);
  $w->element('Textfield', 'comment')->label('comment')->size(30);
  $w->element('Submit',    'Chercher' )->value('submit');
  return $w;
}

sub list : Local {
  my ($self,$c) = @_;

  my $key = $c->req->params->{key};
  my $comment = $c->req->params->{comment};

  my $w = $self->make_widget($c);
  $w->action('list');

  my $result = $w->process($c->req);
  $c->stash->{widget_result} = $result;

  my $query = {};
  if ($key) {
    my $k = $key;
    $k = "$k/$k" unless ($k =~ m{/});
    $query->{key} = ($k =~ /%/) ? { '-like' => $k } : $k;
  }
  $query->{comment} = { '-like' => '%'.$comment.'%'} if ($comment);

  $c->stash->{query} = $query;
  $c->stash->{comments} = [$c->model('CommentSchema::Comment')->search($query)];
  $c->stash->{template} = 'comments/list.tt2';

}

sub edit_form : Local {
  my ( $self, $c ) = @_;
  my $key = $c->session->{current}{key};
  my $comment = $c->model('Schema::Comment')->find($key);
  if ($comment) {
    if ($c->check_user_roles('user')) {
      $c->stash->{comment} = $comment;
      $c->stash->{template} = "comments/form.tt2";
      $c->forward('ErrorMining::View::Simple');
    } else {
      $c->res->output("Insufficient rights");
      $c->res->status(404);
    }
  } else {
    $c->res->output("Missing comment");
    $c->res->status(404);
  }
}

sub new_form : Local {
  my ( $self, $c ) = @_;
  if ($c->check_user_roles( 'user' )) {
    my $key = $c->session->{current}{key};
    $c->stash->{comment}{key} = $key;
    $c->stash->{template} = "comments/form.tt2";
    $c->forward('ErrorMining::View::Simple');
  } else {
    $c->res->output("Insufficient rights");
    $c->res->status(404);
  }
}

sub submit : Local {
  my ( $self, $c ) = @_;
  my $author_id = $c->user->obj->id;
  my $key = $c->session->{current}{key};
  if ($c->check_user_roles( 'user' )) {
    my $query = { author_id => $author_id,
		  key => $key
		};
    $query->{comment} = norm_text($c->req->params->{comment} || "");
    my $comment = $c->model('Schema::Comment')->update_or_create($query);
    $comment->date(\"datetime('now','localtime')");
    $comment->update;
    $comment = $c->model('Schema::Comment')->find($key);
    $c->stash->{current} = $comment;
    $c->stash->{template} = 'comments/comment.tt2';
    $c->forward('ErrorMining::View::Simple');
  } else {
    $c->res->output("Insufficient rights");
    $c->res->status(404);
  }
}

sub norm_text {
  my $s = shift;
  $s =~ s/^\s+//og;
  $s =~ s/\s+$//og;
  return $s;
}

sub xdelete : Local {
  my ( $self, $c ) = @_;
  my $key = $c->session->{current}{key};
  my $comment = $c->model('Schema::Comment')->find($key);
  if ($c->check_user_roles('user')) {
    $comment->delete;
    $c->res->body("Deleting comment");
  } 
}

sub end : ActionClass('RenderView') {
  my ( $self, $c ) = @_;
}


=head1 NAME

ErrorMining::Controller::Comment - Catalyst Controller

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('ErrorMining::Controller::Comment is on Catalyst!');
#}

=back


=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
