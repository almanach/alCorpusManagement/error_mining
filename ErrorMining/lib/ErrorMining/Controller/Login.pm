package ErrorMining::Controller::Login;

use strict;
use warnings;
use base 'Catalyst::Controller';

sub index : Private {
  my ($self, $c) = @_;

##  $c->require_ssl;

  # Get the username and password from form
  my $username = $c->request->params->{username} || "";
  my $password = $c->request->params->{password} || "";
  
  # If the username and password values were found in form
  if ($username && $password) {
    # Attempt to log the user in
    if ($c->authenticate({ username => $username, password => $password})) {
      # If successful, then let them use the application
      $c->response->redirect($c->uri_for('/list'));
      return;
    } else {
      # Set an error message
      $c->stash->{error_msg} = "Bad username or password.";
    }
  }
  
  # If either of above don't work out, send to the login page
  $c->stash->{template} = 'login.tt2';
}


=head1 NAME

ErrorMining::Controller::Login - Catalyst Controller

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('ErrorMining::Controller::Login is on Catalyst!');
#}

=back


=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
