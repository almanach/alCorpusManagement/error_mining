package ErrorMining::Controller::Mine;

use strict;
use warnings;
use base 'Catalyst::Controller';


sub make_widget {
  my ($self,$c) = @_;

  my $w = $c->widget('mine')->method('post');

  # Create the form fields
  $w->element('Textfield', 'rank')->label('Enter rank (or start:end:key)')->size(20);
  return $w;
}

sub list : Global {
  my ($self,$c) = @_;
  my $rank = $c->request->params->{rank};
  my $w=$self->make_widget($c);
  $c->action('/list');

  my $result = $w->process($c->req);
  $c->stash->{widget_result} = $result;

  my $query = {};
  my $options = {};

  my $xrank=$rank;

  $xrank ||= 1;
  $xrank =~ s/^\s+//og;
  if ($xrank !~ /^\d+$/ && $xrank !~ /:/) {
    $xrank = "::$xrank";
  }
  if ($xrank =~ /:/) {
    $xrank=$self->find_rank($c,$xrank);
  }
  my $delta = (($xrank > 40) ? 20 : (($xrank > 20) ? $xrank/2 : 0)) * 23.3;
  $c->stash->{delta} = $delta;
  $self->get_entries($c,$xrank);
  $c->stash->{template} = "entries.tt2";
}

sub get_entries {
  my ($self,$c,$rank) = @_;
  my $min = $rank - 40;
  $min = 1 if ($min < 1);
  my $max = $min + 200;
  my $info = $c->stash->{entries} = [];
  my $k = $min-1;
  my $ers = $c->model('Schema::Rank')
    ->search( {
	       rank => [ -and => { '>=' , $min }, {'<=' , $max } ]
	      },
	      { 
	       order_by => 'rank'
	      }
	    );
  while (my $entry = $ers->next) {
    my $lrank = $entry->rank;
    my $mine = $entry->mine;
    my $key = $mine->key;
    my $occ = $mine->occ;
    my $focc = $mine->focc;
    my $weight = $mine->weight;
    my $id = $entry->id;
    my ($truelex,$lex) = get_lex($key);
    $k++;
    ##    print STDERR "rank=$lrank id=$id key=$key\n";
    my $tmp =  { key => $key,
		 truelex => $truelex,
		 lex => $lex,
		 rate => sprintf("%5.2f%%", 100*$weight), 
		 frate => sprintf("%5.2f%%", 100*($focc/($occ||1))),
		 orate =>  sprintf("%6.2f", $entry->weight),
		 occ => $occ,
		 focc => $focc,
		 sentences => [],
		 id => $id,
		 rank => $lrank,
		 history => { iter => [], weight => []},
		 related => [],
		 lefff => ''
	       };

    next if ($lex =~ /_(DATE|NUM|TEL|HEURE)/og);
    next if ($truelex =~ /^------/);

    if (my $x = $c->model('CommentSchema::Comment')->find($key) ) {
      $tmp->{comment} = $x->comment;
    }
    push(@$info,$tmp);
    if ($lrank == $rank) {
      $tmp->{is_current} = 1;
      $c->stash->{current} = $tmp;
      $c->session->{current} = { key => $key,
				 rank => $rank
			       };
      my $hrs = $entry->history;
      my $iters = $tmp->{history}{iter};
      my $weights = $tmp->{history}{weight};
      while (my $h = $hrs->next) {
	my $v = $h->weight;
	my $liter = $h->iter;
	$v = sprintf("%5.2f%%",100*$v);
	push(@$iters,$liter);
	push(@$weights,$v);
      }
      my $srs = $entry->sentence;
      my $sentences = $tmp->{sentences};
      my $xlex = $truelex;
      $xlex =~ s/_/ /g;
      $xlex=~ s/^\s+//o;
      $xlex=~ s/\s+$//o;
      while (my $s = $srs->next) {
	my $xs = $s->sentence;
	$xs =~ s{(\Q$xlex\E)}{<span class="key">$1</span>}ig;
	push(@$sentences,
	     { corpus => $s->corpus, 
	       id => $s->sid, 
	       sentence => $s->sentence,
	       xs => $xs
	     }
	    );
      }
      ## Getting related forms
      my $related = $tmp->{related};
      unless ($tmp->{lex} =~ /^_/) {
	# fill related lemma: TBD
	my $related = $tmp->{related};
	my $l = $entry->lemma;
	while (my $ll = $l->next) {
	  my $u = $c->model('Schema::Lemma')
	    ->search({lemma => $ll->lemma});
	  while (my $r = $u->next) {
	    my $k = $r->rank;
	    next if ($k->rank == $rank);
	    my $m = $k->mine;
	    my ($truelex,$lex) = get_lex($m->key);
	    my $comment = $c->model('CommentSchema::Comment')->find($m->key);
	    my $x = {
		     key => $truelex,
		     rank => $k->rank
		    };
	    $x->{comment} = $comment->comment if ($comment);
	    push(@$related,$x);
	  }
	}
      }
      ## Getting Lefff info
      $tmp->{lefff} = $c->model('Lefff')->entry($lex);
    }
  }
}

sub get_lex {
  my $key = shift;
  my @lex  = ($key =~ m{^(.+)/(.+?)$}o);
  return @lex;
}

sub check_key {
  my ($key,$pat) = @_;
  my ($truelex,$lex) = get_lex($key);
  return (($key eq $pat) || ($truelex eq $pat) || ($lex eq $pat));
}

sub find_rank {
  my ($self,$c,$rank) = @_;
  my ($start,$end,$key) = split(/:/,$rank);
  $start ||= 1;
  $end ||= 20000;
  my @rs = $c->model('Schema::Mine')
    ->search_like('key' => "$key\%");
  return @rs ? $rs[0]->rank->rank : $start;
}

sub end : ActionClass('RenderView') {
  my ( $self, $c ) = @_;
}

=head1 NAME

ErrorMining::Controller::Mine - Catalyst Controller

=head1 SYNOPSIS

See L<ErrorMining>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('ErrorMining::Controller::Mine is on Catalyst!');
#}

=back


=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
