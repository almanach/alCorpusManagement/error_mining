package ErrorMining::Schema::History;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

## __PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('history');
__PACKAGE__->add_columns(
			 id => {
				data_type => 'integer',
			       },
			 iter => {
				  data_type => 'integer'
				 },
			 weight => {
				    data_type => 'float'
				   },
			);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id iter/);
__PACKAGE__->meta->make_immutable;

1;
