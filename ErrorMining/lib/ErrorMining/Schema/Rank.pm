package ErrorMining::Schema::Rank;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

## __PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('rank');
__PACKAGE__->add_columns(
			 id => {
				data_type => 'integer',
				is_nullable => 0,
			       },
			 rank => {
				  data_type => 'integer',
				  is_nullable => 0,
				 },
			 weight => {
				    data_type => 'float'
				   },
			);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);

##__PACKAGE__->has_one('mine' => 'ErrorMining::Schema::Mine');
__PACKAGE__->has_one('mine' => 'ErrorMining::Schema::Mine',
		     { 'foreign.id' => 'self.id' });
__PACKAGE__->has_many('history' => 'ErrorMining::Schema::History', 'id');
__PACKAGE__->has_many('sentence' => 'ErrorMining::Schema::Sentence', 'id');
__PACKAGE__->has_many('lemma' => 'ErrorMining::Schema::Lemma', 
		      { 'foreign.id' => 'self.id' });

__PACKAGE__->meta->make_immutable;

1;
