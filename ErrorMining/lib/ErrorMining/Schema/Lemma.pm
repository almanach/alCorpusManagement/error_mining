package ErrorMining::Schema::Lemma;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

##__PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('lemma');
__PACKAGE__->add_columns(
			 id => {
				data_type => 'integer',
			       },
			 lemma => {
				   is_nullable => 0
				  },
			);

# Set the primary key for the table
#__PACKAGE__->set_primary_key(qw/id/);

__PACKAGE__->belongs_to('rank' => 
			'ErrorMining::Schema::Rank',
			{ 'foreign.id' => 'self.id' }
		       );


__PACKAGE__->meta->make_immutable;

1;
