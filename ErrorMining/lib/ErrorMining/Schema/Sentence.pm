package ErrorMining::Schema::Sentence;
    
use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

##__PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('sentence');
__PACKAGE__->add_columns(
			 id => {
				data_type => 'integer',
			       },
			 srank => {
				  data_type => 'integer'
				 },
			 corpus => {
				    is_nullable => 0
				   },

			 sid => {
				    is_nullable => 0
				},
			 sentence => {
				      is_nullable => 0
				     },

			);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id srank/);

__PACKAGE__->meta->make_immutable;

1;
