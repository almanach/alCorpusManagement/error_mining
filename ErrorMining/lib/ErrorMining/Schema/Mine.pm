package ErrorMining::Schema::Mine;

    
use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

##__PACKAGE__->load_components(qw/PK::Auto Core/);

__PACKAGE__->table('mine');
__PACKAGE__->add_columns(
			 id => {
				data_type => 'integer',
				is_nullable => 0,
                                is_auto_increment => 1
			       },
			 key => {
				 data_type => 'string'
				},
			 weight => {
				    data_type => 'float'
				   },
			 occ => {
				 data_type => 'integer'
				},
			 focc => {
				  data_type => 'integer'
				 },
			);

# Set the primary key for the table
__PACKAGE__->set_primary_key(qw/id/);

__PACKAGE__->has_one('rank' => 'ErrorMining::Schema::Rank', 
		     { 'foreign.id' => 'self.id'} );
##__PACKAGE__->belongs_to('rank' => 'ErrorMining::Schema::Rank', 'id');

__PACKAGE__->meta->make_immutable;

1;
