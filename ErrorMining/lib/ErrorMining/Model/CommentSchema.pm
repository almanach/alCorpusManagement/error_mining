package ErrorMining::Model::CommentSchema;

use strict;
use base 'Catalyst::Model::DBIC::Schema';

__PACKAGE__->config(
    schema_class => 'ErrorMining::CommentSchema'
);

1;
