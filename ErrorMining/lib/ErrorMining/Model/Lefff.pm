package ErrorMining::Model::Lefff;

use strict;
use warnings;
use Class::C3;
use base qw/Catalyst::Model/;
## use NEXT;
use Carp qw/croak/;
use IPC::Run qw(start finish run pump);

use Catalyst::Exception;

our $VERSION= '0.01';

sub new {
  my ($class,$c,$arguments) = @_;

  ##  my $self = $class->NEXT::new(@_);
  my $self = $class->next::method(@_);

  my $lexed = $c->config->{lexed};
  my $dicodir = $c->config->{dicodir};
  my $diconame = $c->config->{diconame};

  my @lexed = 
    ($lexed,
     '-d',$dicodir,
     '-p',$diconame,
     'consult');
  
  $self->{lefff} = { 
		    in => '',
		    out => '',
		    err => ''
		   };

  $self->{lefff}{h} = start \@lexed,
    '<',\$self->{lefff}{in},
      '>pty>',\$self->{lefff}{out},
	'2>',\$self->{lefff}{err};
  
  return $self;
}

sub entry {
  my ($self,$form) = @_;
  my $tmplex=$form;
  $tmplex =~ s/([^_-])_([^_-])/$1 $2/og;
  $self->{lefff}{in} .= "$tmplex\n";
  pump $self->{lefff}{h} until $self->{lefff}{out};
  my $lexinfo = $self->{lefff}{out};
  $self->{lefff}{out} = '';
  $lexinfo =~ s/^\d+//g;
  $lexinfo =~ s/\|\d*(\s+)/\n|$1/g;
  $lexinfo =~ s/</&lt;/og;
  $lexinfo =~ s/>/&gt;/og;
  chomp $lexinfo;
  return "$lexinfo";
}

1;
