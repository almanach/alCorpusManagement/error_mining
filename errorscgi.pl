#!/usr/bin/perl -w

use CGI::Pretty qw/-compile -nosticky :standard *pre *table *div map :html3 -private_tempfiles -oldstyle_urls/;
##use CGI qw/-compile -nosticky :standard *pre *table *div map :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);
use strict;

use IPC::Run qw(start finish run pump);
use IO::Socket;

use DBI qw/:sql_types/;

##use File::Temp qw/tempfile tempdir/ ;
##use File::Basename;

##$ENV{PATH} = '/bin:/usr/bin/:/usr/local/bin';

local our $query = new CGI;

######################################################################
## Configuration

our $PACKAGE = 'results14.biotim';
our $htmldir = '/var/www/perl/$PACKAGE';

our $basedir = "/home/bearn/clerger/ParserRuns/$PACKAGE";
our $db = "$basedir/dbdir/mine.dat";
our $dbcomment = "$basedir/dbdir/comments.dat";


our $parser = "/perl/parser.pl";
## our $sxpipe = "http://atoll.inria.fr/mafdemo";
our $sxpipe = "/perl/maf/mafdemo.pl";

##our $lineinfo = "$basedir/lines.dat";
our $iter=200;

our $current;

our $initialized;

init();

######################################################################
## Reading parameters

our ($rank) = $query->param(-name=>'rank');

$rank ||=1;

if ($rank !~ /^\d+$/ && $rank !~ /:/) {
  $rank = "::$rank";
}

if ($rank =~ /:/) {
  $rank=find_rank($rank);
}

## Fetch info
our $info = get_entries($rank);

our $delta = (($rank > 40) ? 20 : (($rank > 20) ? $rank/2 : 0)) * 23.3;

######################################################################
## Emitting forms

our $STYLE=<<END;
<!--
    BODY {background-color: linen;}
-->
END

our $JSCRIPT=<<END;

var key = false;

function scroll_handler (obj) {
//    alert("Just scrolled");
//    document.form1.min.value = Math.floor(obj.scrollTop / 14);
}

function mailthis () {
   mail = "mailto:Eric.De_La_Clergerie\@inria.fr?subject=[Lefff] error on "+escape(key);
   mail +=  "&body="+escape(top.location.href);
   var comment = document.getElementById('comment');
   if (comment) {
      comment = comment.innerHTML.split(/\\n+/mg);
      for (var i=0; i < comment.length; i++) {
          mail += "&body="+escape(comment[i]);
      }
   }
   location.href = mail;
}

function editorOpen () {
   var Editor = document.createElement("textarea");
   Editor.id="editor";
   Editor.value="type your comment for "+key;
   var comment = document.getElementById("comment");
   if (comment) {
        Editor.value= comment.innerHTML;
        comment.parentNode.removeChild(comment);
   }
   Editor.setAttribute("rows",10);
   Editor.setAttribute("cols",150);
   document.getElementById("comments").appendChild(Editor);
   button = document.getElementById("editorButton");
   button.setAttribute("title","Editor Button");
   button.innerHTML="close comment";
   button.onclick = function () { editorClose(); };
   Editor.focus();
}

function editorClose () {
   var button = document.getElementById("editorButton");
   button.innerHTML="edit comment";
   button.onclick = function () { editorOpen(); };
   var editor = document.getElementById("editor");
   var parent = editor.parentNode;
   var info = editor.value;
   parent.removeChild(editor);
   makeRequest('handlecomment.db.pl',"?key="+URLencode(key)+"&comment="+URLencode(info));
}

var http_request = false;
function makeRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      http_request.onreadystatechange = alertContents;
      http_request.open('GET', url + parameters, true);
      http_request.send(null);
   }

function alertContents() {
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {

            var comment = http_request.responseText;
//            alert(comment);
            if (comment) {
                   var textNode = document.createTextNode(comment);
                   commentNode = document.createElement('pre');
                   commentNode.id = 'comment';
                   commentNode.appendChild(textNode);
                   document.getElementById("comments").appendChild(commentNode);
            }
         } else {
            alert('There was a problem with the request.');
         }
      }
   }

function getStoredComments (key) {
//  alert("looking for comment on '"+key+"'");
      makeRequest('handlecomment.db.pl',"?key="+escape(key));
}

function toggle_related_comments() {
     var elt = document.getElementById("relatedcomments");
     if (elt.style.display == "block" ) {
       elt.style.display="none";
     } else {
       elt.style.display="block";
     }
}

function URLencode(sStr) {
    return escape(sStr).
             replace(/\\+/g, '%2B').
                replace(/\\"/g,'%22').
                   replace(/\\'/g, '%27').
                     replace(/\\//g,'%2F');
  }

window.onload = function () { 
     key = document.getElementById("key").innerHTML.replace(/\\s+/mg,'');
     getStoredComments(key); 
     scroll = document.getElementById("scroll");
     scroll.scrollTop=$delta;
};

document.addEventListener("keydown", function(event) {					
	var selectedText = window.getSelection();
	
	if (selectedText != '' && !event.altKey && !event.ctrlKey && !event.shiftKey){
 switch (1==1){
    /* p */  case event.keyCode==80: window.open('$parser?grammar=biomgtel&Action=Submit&sentence='+URLencode(selectedText),'Parse','menubar=0,location=0,scrollbar=1'); break;
    /* m */  case event.keyCode==77: window.open('$sxpipe?Action=Submit&opt_compact=on&opt_reduced=on&format=xml&sentence='+URLencode(selectedText),"MAF",'width=700,height=800,scrollbars=1,menubar=0,location=0'); break;
    /* l */  case event.keyCode==76: window.open('lefff.pl?Action=Submit&search='+URLencode(selectedText),"Lefff",'width=600,height=200,menubar=0,location=0'); break;
    default: break;
 }
 }
},false)

END

print
  header(-expires => 'now'),
  start_html( -title => 'Browsing errors',
	      -script=>$JSCRIPT,
              -style => { -code => $STYLE },
              -style => { -src => 'style.css' },
	      -meta => { -robots => 'noindex,nofollow'}
            ),
  h1('Browsing errors for '.$PACKAGE, " [iter=$iter] "),
  div({class => "scroll",
       id => "scroll",
       -onScroll => "scroll_handler(this)"},
      table(map {call($_)} @$info)),
  start_div({class=>"body"}),
  start_form(-name=>'form1'),
  p('Enter rank (or start:end:key)',
    textfield( -name=> 'rank', -size=>40, -maxlength=>100 ),
##    hidden( -name=> 'min' ),
    a({href=>'javascript:mailthis()'},
      "Mail this page"),
   ),
  end_form,
  hr;

process_entry();

print   
  hr,
  end_div,
  end_html;

######################################################################

sub call {
  my $info = shift;
  my $lrank = $info->{rank};
  my $attr={};
  my $key = $info->{key};
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  my $label = $truelex;
  $label .= span({class=>"lex"},"/$lex") unless ($lex eq $label);
  $attr->{class} = "withcomment" if (exists $info->{comment});
  $attr->{class} = "current" if ($rank == $lrank);
  return Tr(td($attr,a({href=>"errorscgi.pl?rank=$lrank"},"$lrank $label")));
}
 
sub process_entry {
  $rank =~ s/^\s+//og;
  return unless ($rank);
  my $info = $current;
  return unless (defined $info);
  my $key = $info->{key};
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  print div({ id => "comments", 
	    ##  -onLoad => "getStoredComments($key)"
	    },
	    div({id => "editorbar" }, 
		a({-onClick=> "editorOpen()",href=>"#",class=>"button",id=>"editorButton"},"edit comment")));
  print hr,h1("Statistical info on  ",span({class=>"key",id=>"key"},$key));
  print table({class=>"stats",
	       border => "1",
	       cellpadding => "3",
	       cellspacing => "0",
	       width=>"80%",
	       align => "center"
	      },
	      Tr( 
		 [ th(["rank","\#occ.","\#failed","%failed weight","%failed sentences","orate"]),
		   td({align => "center"},[$rank,
					   $info->{occ},
					   $info->{focc},
					   $info->{rate},
					   $info->{frate},
					   $info->{orate}
					  ])
		 ]));
  print p({class=>"history"},"history: $info->{history}");
  my @lexed = 
    ('/usr/bin/lexed',
     '-d','/usr/local/lib/lefff-frmg',
     '-p','dico.xlfg',
     'consult');

  my $h = start \@lexed,
    '<pipe',\*IN,
      '>pipe',\*OUT;
  my $tmplex=$lex;
  $tmplex =~ s/([^_-])_([^_-])/$1 $2/og;
  print IN "$tmplex\n";
  close(IN);
  my $lexinfo = <OUT>;
  $lexinfo =~ s/^\d+//g;
  $lexinfo =~ s/\|\d*(\s+)/\n|$1/g;
  print hr, h1("Lefff info for ",span({class=>"key"},$lex)),
    pre(escapeHTML($lexinfo));
  close(OUT);
  finish $h;
  if (@{$info->{related}}) {
    print hr,h1("Related words");
    print p(a({class=>"button",
	       href=>"javascript:void(toggle_related_comments())",
	       id=>'relcommenttoggle'},"[comments]"),
	    map { call_related($_,1) } @{$info->{related}});
    my @comments = ();
    foreach my $tmp (@{$info->{related}}) {
      next unless (exists $tmp->{comment});
      my $comment = escapeHTML($tmp->{comment});
      $comment =~ s/(::\S+)/a({href=>"errorscgi.pl?rank=$1"},$1)/oge;
      push(@comments,li(call_related($tmp),':',$comment));
    }
    print div({id => 'relatedcomments'}, ul(@comments));
  }
  if (@{$info->{sentences}}) {
    my @content = ();
    foreach my $k (@{$info->{sentences}}) {
      my $s = $k->{sentence};
      my $s2 = URLencode($k->{sentence});
      $truelex =~ s/_/ /g;
      $s =~ s{\Q$truelex\E}{<span class="key">$truelex</span>}ig;
      push(@content,li(span({class=>"sref"},
			    "[$k->{corpus}#$k->{id}]"),
		       $s,
		       a({ href=>"$parser?grammar=biomgtel&Action=Submit&sentence=$s2",target => "new",name=>"Parsing"},"[P]"),
		       a({ href=>"$sxpipe?Action=Submit&opt_compact=true&opt_reduced=true&format=xml&sentence=$s2",target => "new",name=>"Morpho-Syntax"},"[M]")
		      )
	  );
    }
    print 
      hr,
	h1("Failed sentences with ",span({class=>"key"},$key),br," as most probable cause for failure"),
	  ul(@content);
  }
}

sub URLencode {
  my $s = shift;
  $s =~ s/\+/%2B/og;
  $s =~ s/\"/%22/og;
  $s =~ s/\'/%27/og;
  $s =~ s/\//%2F/og;
  $s = escapeHTML($s);
  return $s;
}

sub call_related {
  my $info = shift;
  my $flag = shift || 0;
  my $key = $info->{key};
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  my $label = $truelex;
  my $rank = $info->{rank};
  my $attr={href=>"errorscgi.pl?rank=$info->{rank}"};
  $attr->{class} = "withcomment" if ($flag && exists $info->{comment});
  ##  $label .= span({class=>"lex"},"/$lex") unless ($lex eq $label);
  return a($attr,"$label ($rank/$info->{occ}/$info->{weight}%)");
}

sub new_weight {
  my ($weight,$occ) = @_;
  return $weight * log($occ);
}

sub get_entries {
  my $rank = shift;
  my $min = $rank - 40;
  $min = 1 if ($min < 1);
  my $max = $min + 200;
  my $dbh = DBI->connect_cached("dbi:SQLite:$db", "", "",
				{RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
  $dbh->func( "new_weight", 2, \&new_weight, "create_function" );
  my $comment_dbh = DBI->connect_cached("dbi:SQLite:$dbcomment", "", "",
					{RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
  my $comment_fetch = $comment_dbh->prepare_cached(<<EOF);
SELECT comment FROM comment WHERE key=?
EOF
  my $mine_fetch = $dbh->prepare_cached(<<EOF);
SELECT mysort.rank,mine.id,mine.key,mine.weight,mine.occ,mine.focc FROM mysort,mine
WHERE mysort.rank >= ? and mysort.rank <= ? and mysort.id = mine.id
ORDER BY mysort.rank
EOF
  $mine_fetch->bind_param(1,$min,SQL_INTEGER);
  $mine_fetch->bind_param(2,$max,SQL_INTEGER);
  $mine_fetch->execute();
  my $info = [];
  my $k = $min-1;
  while (my ($lrank,$id,$key,$weight,$occ,$focc) = $mine_fetch->fetchrow_array()) {
    my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
    $k++;
    ##    print STDERR "rank=$lrank id=$id key=$key\n";
    my $tmp =  { key => $key,
		 truelex => $truelex,
		 lex => $lex,
		 rate => sprintf("%5.2f%%", 100*$weight), 
		 frate => sprintf("%5.2f%%", 100*($focc/$occ)),
		 orate =>  sprintf("%6.2f", new_weight($weight,$occ)),
		 occ => $occ,
		 focc => $focc,
		 sentences => [],
		 id => $id,
		 ## rank => $id,
		 rank => $lrank,
		 history => ""
	       };
    $comment_fetch->execute($key);
    while (my ($comment) = $comment_fetch->fetchrow_array()) {
      $tmp->{comment} = $comment;
    }
    push(@$info,$tmp);
    if ($lrank == $rank) {
      $current = $tmp;
      my $history_fetch = $dbh->prepare_cached(<<EOF);
SELECT iter, weight FROM history 
WHERE id=?
ORDER BY iter DESC
EOF
      my $sentence_fetch = $dbh->prepare_cached(<<EOF);
SELECT srank,corpus,sid,sentence FROM sentence WHERE id=? ORDER BY srank
EOF
      $history_fetch->execute($id);
      my %history= ( iter => ['#iteration'], weight => ['weight']);
      while (my ($liter,$v) = $history_fetch->fetchrow_array()) {
	$v = sprintf("%5.2f%%",100*$v);
	push(@{$history{iter}},$liter);
	push(@{$history{weight}},$v);
      }
      $current->{history} = 
	table({ class => "history", border => 1, 
		cellpadding => "3",
		cellspacing => "0",
		width=>"60%",
		align => "center"
	      },
	      Tr([td($history{iter}),td($history{weight})]));
      $sentence_fetch->execute($id);
      while (my ($srank,$corpus,$sid,$sentence) = $sentence_fetch->fetchrow_array()) {
	push(@{$current->{sentences}},{ corpus => $corpus, id => $sid, sentence => $sentence});
      }
      ## Getting related forms
      $current->{related} = [];
      unless ($current->{lex} =~ /^_/) {
      my $lemma_fetch = $dbh->prepare_cached(<<EOF);
SELECT mine.id,mine.key,mine.occ,mine.weight,mysort.rank FROM mine,mysort 
WHERE mine.id IN (SELECT id FROM lemma 
             WHERE lemma IN (SELECT lemma FROM lemma WHERE id=?))
AND mine.id = mysort.id
ORDER BY mysort.rank
LIMIT 30
EOF
      $lemma_fetch->execute($id);
      while (my ($lid,$lkey,$locc,$lweight,$lrank) = $lemma_fetch->fetchrow_array()) {
	next if ($lid == $id);
	my $tmp = { key => $lkey, 
		    id => $lid, 
		    rank => $lrank, 
		    weight => sprintf("%5.2f",100*$lweight),
		    occ => $locc
		  };
	$comment_fetch->execute($lkey);
	while (my ($comment) = $comment_fetch->fetchrow_array()) {
	  $tmp->{comment} = $comment;
	}
	push(@{$current->{related}},$tmp);
      }
    }
    }
  }
  return $info;
}


sub check_key {
  my ($key,$pat) = @_;
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  return (($key eq $pat) || ($truelex eq $pat) || ($lex eq $pat));
}

sub find_rank {
  my $rank=shift;
  my ($start,$end,$key) = split(/:/,$rank);
  $start ||= 1;
  $end ||= 20000;
  my $dbh = DBI->connect_cached("dbi:SQLite:$db", "", "",
				{RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
  $dbh->func( "check_key", 2, \&check_key, "create_function" );
  my $mine_fetch = $dbh->prepare_cached(<<EOF);
SELECT mysort.rank FROM mine,mysort 
WHERE check_key(mine.key,?) 
AND mine.id=mysort.id AND mysort.rank >= ? AND mysort.rank <= ?
LIMIT 1
EOF
  $mine_fetch->bind_param(1,$key,SQL_VARCHAR);
  $mine_fetch->bind_param(2,$start,SQL_INTEGER);
  $mine_fetch->bind_param(3,$end,SQL_INTEGER);
  $mine_fetch->execute();
##  $mine_fetch->execute($key,$start,$end); 
  while (my ($lrank) = $mine_fetch->fetchrow_array()) {
    $mine_fetch->finish();
    return $lrank;
  }
  return 1;
}


sub init {
  return if (defined $initialized);
  $initialized = 1;
  my $dbh = DBI->connect_cached("dbi:SQLite:$db", "", "",
				{RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
  $dbh->func( "new_weight", 2, \&new_weight, "create_function" );
  my %form = ();
  my $sth1 = $dbh->prepare_cached("SELECT id FROM mine ORDER BY 1.0*new_weight(weight,occ) DESC");
  $sth1->execute();
  my $i = 1;
  while (my ($id) = $sth1->fetchrow_array()) {
    $form{$id} = $i++;
  }
  $dbh->func( "rank", 1, sub { my $id=shift; return $form{$id}; }, "create_function" );
  my $sth = $dbh->do(<<EOF);
CREATE TEMP TABLE mysort AS 
SELECT rank(id) as rank, id FROM mine
EOF
  ##   print STDERR "Done sorting !";
}
