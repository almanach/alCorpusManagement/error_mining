#!/usr/bin/perl

use strict;
use Carp;
use DBI;
use IPC::Run  qw(start finish run pump);

use AppConfig qw/:argcount :expand/;
my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
			    "mine=f",
                            "results=f" => {DEFAULT => 'results'},
			    "db=f" => {DEFAULT => "dbdir/mine.dat" },
			    "ngm=i" => {DEFAULT => 1},
			    "algo=s" => {DEFAULT => ''},
			    "l=i" => {DEFAULT => 0},
			    "iter=i" => {DEFAULT => 200}
                           );

$config->args();

my $iter = $config->iter();
my $results=$config->results;
my $algo = $config->algo;
my $handle_lemmas=$config->l;
my $lsuffix="";
if ($handle_lemmas) {$lsuffix="l"}
my $ngm = $config->ngm;
my $mine = $config->mine() || "$results/mine$iter-$ngm$lsuffix$algo.csv";
my $db=$config->db()  || "$results/../dbdir/mine$iter-$ngm$lsuffix$algo.dat";

my $out;
my $in;

my @lexed = 
  ('/usr/bin/lexed',
   '-d','/usr/local/lib/maf',
   '-p','dico.xlfg',
   'consult');

my $h = start \@lexed,
  \$in,\$out;

unlink $db if (-r $db);

create_table();

fill_table();

## create table
sub create_table {
  my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
                         {RaiseError => 1, AutoCommit => 0});
  $dbh->do(<<EOF );
CREATE TABLE mine (id INTEGER,key,weight FLOAT,occ INTEGER, focc INTEGER,
                  PRIMARY KEY(id),
                  UNIQUE(id)
);
EOF
  
  $dbh->do(<<EOF );
CREATE TABLE history (id INTEGER,iter INTEGER,weight FLOAT,
                  PRIMARY KEY(id,iter)
);
EOF
  
  $dbh->do(<<EOF );
CREATE TABLE sentence (id INTEGER, srank INTEGER,corpus,sid,sentence,
                  PRIMARY KEY(id,srank)
);
EOF

  $dbh->do(<<EOF);
CREATE TABLE lemma (id INTEGER, lemma);
EOF
  
  $dbh->commit;
  $dbh->disconnect;
}

## populate table
sub fill_table {
  my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
                         {RaiseError => 1, AutoCommit => 0});
  # insert initial values
  my $mine_sth=$dbh->prepare('INSERT INTO mine(id,key,weight,occ,focc) VALUES (?,?,?,?,?)');
  my $history_sth=$dbh->prepare('INSERT INTO history(id,iter,weight) VALUES (?,?,?)');
  my $sentence_sth=$dbh->prepare('REPLACE INTO sentence(id,srank,corpus,sid,sentence) VALUES (?,?,?,?,?)');
  my $lemma_sth = $dbh->prepare('REPLACE INTO lemma(id,lemma) VALUES (?,?)');

  open(ERROR,"<$mine") || die "can't open $mine: $!";
  my $rank=0;
  my %corpus = ();
  my %sentences = ();
  my $srank = 0;
  my $key;
  while (<ERROR>) {
    chomp;
    next if (/^#/);
    if (/^\S+/) {
      $dbh->commit unless ($rank % 1000) ;
      my @k = split(/\s+/,$_);
      $rank++;
      $srank = 0;
      $key = $k[0];
      $mine_sth->execute($rank,$k[0],$k[1],$k[2],$k[3]);
      my %lemma = get_lemma($key);
      foreach my $lemma (keys %lemma) {
	print STDERR "Register $rank $key $lemma\n" if ($config->verbose);
	$lemma_sth->execute($rank,$lemma);
      }
      next;
    }
    if (/^\s+history\s*(.*)/) {
      my $history = $1;
      chomp $history;
      my $i = 1;
      foreach my $h (reverse(split(/\s+/,$history))) {
	my ($liter,$r,$v) = split(/:/,$h);
	next unless ($liter % 10 || $liter ge ($iter - 1));
	last if ($i++ == 15);
	$history_sth->execute($rank,$liter,$v);
      }
      next;
    }
    if (/^\s+/) {
      s/^\s+//og;
      my @k = split(/\s+/,$_);
      $srank++;
      my ($corpus,$id) = split(/\#/,$k[0]);
      $corpus{$corpus} = 1;
      $sentences{$corpus}{$id}{$rank}{$srank} = 1;
      next;
    }
  }
  close(ERROR);

  $dbh->commit;

  print STDERR "Loading relevant sentences in database\n";
  foreach my $corpus (keys %corpus) {
    my $file = "$results/$corpus.log";
    if (-r $file) {
      open(LOG,"<$file") || die "can't open file:$!";
    } elsif (-r "$file.gz") {
      open(LOG,"-|","gunzip -c $file.gz") || die "can't open file:$!";
    } else {
      next;
    }
    while (<LOG>) {
      if (/^fail\s+(\d+)\s+(.+)/ && (exists $sentences{$corpus}{$1})) {
	chomp;
	my $sid = $1;
	my $s = $2;
	foreach my $info ($sentences{$corpus}{$sid}) {
	  foreach my $rank (keys %$info) {
	    foreach my $srank (keys %{$info->{$rank}}) {
	      ##	      print "$corpus $sid $srank $rank\n";
	      $sentence_sth->execute($rank,$srank,$corpus,$sid,$s);
	    }
	  }
	}
      }
    }
    close(LOG);
    $dbh->commit;
  }

  $dbh->disconnect;
}


sub get_lemma {
  my $key = shift;
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  my $tmplex=$lex;
  $tmplex =~ s/([^_-])_([^_-])/$1 $2/og;
  $in .= "$tmplex\n";
  ##  close(IN);
  $h->pump until $out;
  ## close(OUT);
  my @lemma = ($out =~ /pred='([^_<>']+).*?'/og);
  $out = '';
  my %lemma = ();
  $lemma{$_} = 1 foreach (@lemma);
  return %lemma;
}
