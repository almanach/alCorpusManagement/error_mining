#!/usr/bin/perl

## Loading a log file inside a database to compute more easily statistics

use strict;
use Carp;

use DBI;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "corpus=f@" => {DEFAULT => []},
                            "slice=i" => {DEFAULT => 20},
                            "run!" => {DEFAULT => 1},
                            "lock=f" => {DEFAULT => 'lock'},
                            "results=f" => {DEFAULT => 'results'},
                            "testparser=f" => {DEFAULT => '/usr/bin/testparser'},
                            "parse_options=s" => {DEFAULT => '-collsave -time -stats -xmldep -robust'},
                            "key=s" => {DEFAULT => ''},
                            "host=s@" => {DEFAULT => []},
                            "hostallow!" => {DEFAULT => 1},
                            "share!" => {DEFAULT => 1},
			    "dag=f" => {DEFAULT => 'dag'},
			    "mafdocument!" => {DEFAULT => 0},
			    "dagext=s" => {DEFAULT => "udag"},
			    "maxiter=i" => {DEFAULT => 40},
			    "dump!" => {DEFAULT => 0},
			    "beta=i" => {DEFAULT => 0.1},
			    "ngm=i" => {DEFAULT => 1},
			    "l=i" => {DEFAULT => 0},
			    'distrib=f'
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my %status = ( unparsed => -1,
	       ok => 1,
	       fail => 0,
	       robust => 0
	     );

my $results = $config->results;
my $verbose = $config->verbose;
my @corpus = @{$config->corpus};
my $dagdir = $config->dag;
my $dagext = $config->dagext;

unless (@corpus) {
  my $info = "$dagdir/info.txt";
  $info = "info.txt" unless (-r $info);
  if (!-r $info) {
    # on prend comme corpus tous ceux qui ont un fichier log dans results
    @corpus = `ls $results/*.log`;
    for my $i (0..$#corpus) {
      chomp($corpus[$i]);
      $corpus[$i]=~s/^.*\///;
      $corpus[$i]=~s/\.log$//;
    }
  } else {
    open(INFO,"$dagdir/info.txt") or die "can't open $dagdir/info.txt: $!";    
    while(<INFO>){
      chomp;
      my ($corpus,$size) = split(/\s+/,$_);
      push(@corpus,$corpus);
    }
    close(INFO);
  }
}

my $rate=0;

my %words;
my %sentences;

my @words;
my @sentences;

my $wcount=0;
my $scount=0;
my $socc=0;
my $success=0;
my $avg=0;
my $var=0;

my $maxiter=$config->maxiter;
my $ngm=$config->ngm;
my $beta = $config->beta;
my $handle_lemmas=$config->l;
my $lsuffix="";
if ($handle_lemmas) {$lsuffix="l"}
my %form_to_lemmas;

my $lefffdir="/usr/local/share/lefff";


loader();
for (my $iter=1; $iter <= $maxiter; $iter++) {
  process($iter);
}

sub loader {

  if ($handle_lemmas) {
    print STDERR "Loading lexicon ...";
    for my $lexfile (`ls $lefffdir/*.lex`) {
      chomp($lexfile);
      my $lexentriesnum=0;
      print STDERR "\n   $lexfile       \r";
      open (LEXFILE,"<$lexfile") || die "Could not open lexfile $lexfile: $!\n";
      while (<LEXFILE>) {
	chomp;
	next if (/^\#/ || /^$/);
	if (/^([^\t]+)\t[^\t]*\t[^\t]*\t[^\']*\'(.*[^_])__/) {
	  if ($lexentriesnum % 1000 == 0) {
	    print STDERR "   $lexfile : $1 -> L:$2                           \r";
	  }
	  $lexentriesnum++;
	  $form_to_lemmas{$1}{"L:".$2}=1;
	}
      }
      close(LEXFILE);
      print STDERR "   $lexfile                                                  \r";
    }
  }

  print STDERR "Loading error database ...\n";

  open(LINE,">$results/lines.dat") || die "can't open lines.dat";

  foreach my $corpus (@corpus) {
    my $log = "$results/$corpus.log";
    if (-r $log) {
      open(LOG,"<$log") or die "can't open log file: $!";
    } elsif (-r "$log.gz") {
      open(LOG,"-|","gunzip -dc $log.gz") or die "can't open log file: $!";
    } else {
      next;
    }



    print STDERR "\tprocessing log $corpus\n";
    my $id;
    my $status;
    my $time;
    my $ambiguity;
    my $host;
    my $timeout=0;
    $emptycorpus{$corpus}=1;
    while (<LOG>) {
      if (/^(fail|ok)\s+(\d+)/) {
	$id = $2;
	next unless ($id);
	$status = $status{$1};
	$time = undef;
	$ambiguity = undef;
	$host = undef;
	$timeout=0;
	my $sid = $sentences{$corpus}{$id} = $scount++;
	my $sentry = { status => $status, 
		       corpus => $corpus, 
		       id => $id,
		       occ => [],
		       n => 0,
		       line => $.,
		       sid => $sid
		     };
	push(@sentences,$sentry);
	print LINE "$corpus $id $.\n";
	$success++ if ($status);
	$emptycorpus{$corpus}=0;
	next;
      }
    }
    close(LOG);
  }

  close(LINE);
  print STDERR "\tLoaded $scount sentences including $success successes\n";

  foreach my $corpus (@corpus) {
    next if ($emptycorpus{$corpus});
    my $file = "$dagdir/$corpus.$dagext";
    if (-r "$file") {
      open(DAG,"<$file") or die "can't open file: $!";
    } elsif (-r "$file.gz") {
      open(DAG,"-|","gunzip -dc $file.gz") or die "can't open file: $!";
    } else {
      print STDERR "missing DAG file: $corpus\n";
      next;
    }
    print STDERR "\tprocessing dag $corpus\n";
    my $sid;
    my $n=0;
    my $edges = 0;
    my $lastsid;
    my $sentry;
    my (@curr,@prev);
    while (my $line = <DAG>) {
      if ($sid % 1000 == 0) {print STDERR "\t$sid\r";}
      if ($line =~ /^##\s*DAG\s+END/io){
	if ((defined $sentry) && !@{$sentry->{occ}}) {
	  print STDERR "*** VOID SENTENCE $corpus $sid\n";
	  exit 1;
	}
	next;
      }
      if ($line =~ /^##\s*DAG\s+BEGIN/io) {
	$line = <DAG>;
	$line =~ /id="E(\d+)F\d+"/;
	$lastsid = $sid;
	$sid = $1;
	if (defined $lastsid && $sid == $lastsid) {
	  print STDERR "Warning: duplicate $corpus $sid\n";
	  $sid++;
	}
	$n = 0;
	$edges = 0;
	undef $sentry;
	my $key = $sentences{$corpus}{$sid};
	$sentry = $sentences[$key] if (defined $key);
	$prev[1]="<BEGIN>";
	$prev[2]="";
      }
      if ($line =~ /^(\d+)\s+\{(.+)\}\s+(.+?)\s+(\d+)/o) {
	## print "DAG $line";
	next unless (defined $sentry);
	my ($left,$content,$lex,$right) = ($1,$2,$3,$4);
	my @content = ($content =~ m{<F\s+.*?>\s*(.*?)\s*</F>}g);
# ANCIENNEMENT:
# 	my $v = join("_",@content).'/'.$lex;
# 	# occurence to process
# 	my $wid = $words{$v} ||= $wcount++;
# 	## wentry: 0=rate 1=tmprate 2=occ 3=$lex 4=confidence 5=focc 6=fsentences 7=spring 8=history
# 	## 9=barycentre
# 	my $wentry = $words[$wid] ||= [1,0,0,$v,0,0,[],0,"",1];
# 	push(@{$sentry->{occ}},$wentry);
# 	$sentry->{n}++;
# 	$wentry->[2]++;
# 	$wentry->[5]++ unless ($sentry->{status});
# 	$socc++;
# FIN ANCIENNEMENT
# NOUVEAU
	$curr[1]=join("_",@content).'/'.$lex;
	if ($ngm>=2) {
	    $curr[2]=$prev[1].'__'.$curr[1];
	    if ($ngm>=3) {
		$curr[3]="";
		$curr[3]=$prev[2].'__'.$curr[1] unless ($prev[2] eq "");
		$prev[2]=$curr[2];
	    }
	    $prev[1]=$curr[1];
	}

	## wentry: 0=rate 1=tmprate 2=occ 3=$lex 4=confidence 5=focc 6=fsentences 7=spring

	for my $i (1..$ngm) {
	    my $occ=$curr[$i];
	    if ($occ ne "") {
		my $wid = $words{$occ} ||= $wcount++;
		my $wentry = $words[$wid] ||= [1,0,0,$occ,0,0,[],0];
		push(@{$sentry->{occ}},$wentry);
		$sentry->{n}++;
		$wentry->[2]++;
		$wentry->[5]++ unless ($sentry->{status});
		$socc++;
	    }
	}
	if ($handle_lemmas && defined($form_to_lemmas{$lex})) {
	  for my $occ (keys %{$form_to_lemmas{$lex}}) {
	    my $wid = $words{$occ} ||= $wcount++;
	    my $wentry = $words[$wid] ||= [1,0,0,$occ,0,0,[],0];
	    push(@{$sentry->{occ}},$wentry);
	    $wentry->[2]++;
	    $wentry->[5]++ unless ($sentry->{status});
	    $sentry->{n}++;
	    $socc++;
	  }
	}
# FIN NOUVEAU
      }
      my ($tmp) = ($line =~ /id="E\d+F(\d+)"/o);
      $edges++;
      $n = $tmp if ($tmp > $n);
    }
    close(DAG);
  }

  print STDERR "\tLoaded $wcount words and $socc occurrences\n";

  %words = {};
  %sentences = {};

  $avg = ($scount-$success) / $socc;

  foreach my $wentry (@words) {
    if ($beta<100) {
      my $lambda = $wentry->[7] = (1-exp(-$beta*$wentry->[2]));
      $wentry->[9] = $avg + $lambda * (1-$avg);
    } else {  # on considère que beta = +\infty
      $wentry->[7] = $wentry->[9] = 1;
    }
  }

  if ($config->distrib()) {
    my $db = "$results/".$config->distrib();
    unlink $db if (-r $db);
    my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
			   {RaiseError => 1, AutoCommit => 0});
    $dbh->do(<<EOF);
CREATE TABLE form (key,occ,focc);
EOF
    my $sth = $dbh->prepare('INSERT INTO form(key,occ,focc) VALUES (?,?,?)');
    foreach my $wentry (@words) {
      $sth->execute($wentry->[3],$wentry->[2],$wentry->[5]);
    }
    $dbh->commit;
    $dbh->disconnect;
  }

}

sub process {
  my $iter = shift;

  print STDERR "Processing round $iter...                                                           \r";

  foreach my $wentry (@words) {
    $wentry->[6] = [];
  }

  print STDERR "Processing round $iter...\tUpdating occurrences in sentences\r";
  foreach my $sentry (@sentences) {
    next if ($sentry->{status});
    my $n = 0;
    my $max=0;
    my $maxw=0;
##    $n += $avg+$_->[7]*($_->[0]-$avg) foreach (@{$sentry->{occ}});
    $n += $_->[9] foreach (@{$sentry->{occ}});
    foreach my $wentry (@{$sentry->{occ}}) {
##      my $delta = (($avg+$wentry->[7]*($wentry->[0]-$avg)) / $n);
      my $delta = $wentry->[9] / $n;
      $wentry->[1] += $delta;
      if ($delta > $max) {
	$max=$delta;
	$maxw=$wentry;
      }
    }
    $sentry->{max} = $max;
    if ($iter == $maxiter ) {
      if ($max == 0) {
	print STDERR "\n*** max=0 $sentry->{corpus} $sentry->{id}\n";
      } else {
	my $threshold=0.6*$max;
	foreach my $wentry (@{$sentry->{occ}}) {
	  my $delta;
	  if ($beta<100) {
	    $delta = (($avg+$wentry->[7]*($wentry->[0]-$avg)) / $n);
	  } else { # on considère que beta = +\infty, et donc on a $wentry->[7] = 1, donc:
	    $delta = $wentry->[0] / $n;
	  }
	  next unless ($delta > $threshold);
	  my @l = ([$delta,$sentry],@{$wentry->[6]});
	  @l = sort {$b->[0] <=> $a->[0]} @l;
	  @l = @l[0..20] if (@l > 20);
	  $wentry->[6] = [@l];
	}
      }
    }
  }

  print STDERR "Processing round $iter...\tUpdating words                                \r";

  foreach my $wentry (@words) {
    my $tmp = $wentry->[0] = $wentry->[1] / $wentry->[2];
    $wentry->[1] = 0;
    $wentry->[9] = $avg + $wentry->[7]*($tmp - $avg);
    ##    $wentry->[4] = $wentry->[9] * $wentry->[2];
  }
  
  ## sorting @words
  unless (($iter % 5) && ($iter < ($maxiter -1))) {
    @words = sort {$b->[9] <=> $a->[9]} @words;
    my $rank=0;
    # handling history on words
    foreach my $wentry (@words) {
      next unless ($wentry->[2] > 5);
      last unless ($wentry->[9] > 1.5*$avg);
      $rank++;
      $wentry->[8] = "$wentry->[8] $iter:$rank:$wentry->[9]";
    }
  }

  if (0) {
    foreach my $wentry (@words) {
      my $n = $wentry->[2];
      next unless ($n > 10);
      my $nu = $n * $avg;
      my $s = 2*$nu * (1-$avg);
      $wentry->[4] = exp(-($wentry->[0]-$nu)**2/$s)/sqrt($s*3.14);
    }
  }

  if ($iter == $maxiter) {
    &mydump($iter);
    &myshortdump($iter);
    &maxent_dump($iter);
    &vannoord_dump($iter);
  }

}

sub mydump {
  my $iter = shift;
  ##  return if ($iter % 5);
  print STDERR "Dumping round $iter ...\n";
  print STDERR "\tsorting\n";
  my @words = sort {$b->[9] <=> $a->[9]} @words;
  print STDERR "\twriting\n";
  open(DUMP,">$results/mine$iter-$ngm$lsuffix".".csv") || die "can't dump iter $iter";
  print "#AVERAGE FAILURE RATE=$avg\n";
  print DUMP "#AVERAGE FAILURE RATE=$avg\n";
  my $rank=0;
  foreach my $wentry (@words) {
    next unless ($wentry->[2] > 5);
    last unless ($wentry->[9] > 1.5*$avg);
 ##   next unless ($iter != $maxiter || @{$wentry->[6]});
    $rank++;
    print DUMP "$wentry->[3]\t$wentry->[9]\t$wentry->[2]\t$wentry->[5]\t$wentry->[4]\t$wentry->[0]\n";
    print DUMP "\thistory $wentry->[8]\n";
    foreach my $info (@{$wentry->[6]}) {
      my $sentry = $info->[1];
      print DUMP "\t$sentry->{corpus}#$sentry->{id}\t$sentry->{line}\n";
    }
  }
  close(DUMP);
}


sub myshortdump {
  my $iter = shift;
  ##  return if ($iter % 5);
  print STDERR "Dumping round $iter ...\n";
  print STDERR "\tsorting\n";
  ## my @words = sort {$b->[9] <=> $a->[9]} @words;
  print STDERR "\twriting\n";
  open(DUMP,">$results/shortmine$iter-$ngm$lsuffix".".csv") || die "can't shortdump iter $iter";
  my $rank=0;
  foreach my $wentry (@words) {
    $rank++;
    print DUMP "$wentry->[3]\t$wentry->[9]\t$wentry->[2]\t$wentry->[5]\t$wentry->[4]\t$wentry->[0]\n";
  }
  close(DUMP);
}

sub maxent_dump {
  my $iter = shift;
  open(DUMP,">$results/me$iter-$ngm$lsuffix.txt") || die "can't open file:$!";
  print STDERR "Dumping data for megam (max ent classifier) ...\n";
  foreach my $sentry (@sentences) {
    my $status = $sentry->{status};
    next if ($status == -1);
    $status = 1-$status;
    my %f = ();
    foreach my $occ (@{$sentry->{occ}}) {
      $f{$occ->[3]}++;
    }
    print DUMP "$status";
    foreach my $lex (sort keys %f) {
      my $v = $f{$lex} / $sentry->{n};
      print DUMP "\t$lex $v";
    }
    print DUMP "\n";
  }
  close(DUMP);
}

sub vannoord_dump {
  my $iter = shift;
  open(DUMP,">$results/vn$iter-$ngm$lsuffix.txt") || die "can't open file:$!";
  print STDERR "Dumping van-Noord-like results ...\n";
  my (%ftot,%ffailed);
  foreach my $sentry (@sentences) {
    my $status = $sentry->{status};
    next if ($status == -1);
    my %seen=();
    foreach my $occ (@{$sentry->{occ}}) {
      if (!defined($seen{$occ->[3]})) {
	$ftot{$occ->[3]}++;
	$ffailed{$occ->[3]}+=$status;
	$seen{$occ->[3]}=1;
      }
    }
  }
  foreach my $lex (sort keys %ffailed) {
    $ffailed{$lex}/=$ftot{$lex};
  }
  my @f = sort {$ffailed{$b} <=> $ffailed{$a}} (keys %ffailed);
  foreach my $i (0..$#f) {
    print DUMP "$i\t".$f[$i]."\t".$ffailed{$f[$i]}."\n";
  }
  close(DUMP);
}

1;

__END__
